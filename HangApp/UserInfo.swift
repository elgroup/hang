//
//  UserInfo.swift
//  BluLint
//
//  Created by hament miglani on 21/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class UserInfo: NSObject
{
    var userName:NSString!
    var emailId:NSString!
    var mobileNumber:NSString!
    var name:NSString!
    var password:NSString!
    var imageUrl:NSString!
    var userId:Int!
    var fullName:NSString!
    var location:NSString!
    var bio:NSString!
     var dob:NSString!
    var userType:NSString!
    


}
