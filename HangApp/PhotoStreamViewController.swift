////
//  PhotoStreamViewController.swift
//  RWDevCon
//
//  Created by Mic Pringle on 26/02/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import AVFoundation


class PhotoStreamViewController: UIViewController,UINavigationControllerDelegate,
UIPopoverPresentationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,AppServiceDelegate
{
     var momentSelected = MomentsInfo()
    var isComingFrom = String()
     let ser:AppService = AppService()
     @IBOutlet var collectionView: UICollectionView!
  var userMomentsArray:NSMutableArray = []
    var momentsImageArray = NSMutableArray ()
      var selectedIndexPath = IndexPath()
 var photos = Photo.allPhotos()
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return UIStatusBarStyle.lightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
      view.backgroundColor = UIColor.white
   
   collectionView!.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
  }
    override func viewWillAppear(_ animated: Bool)
    {
        if isComingFrom == "Discover"
        {
            getMomentsById(momentId: AppDelegate.getAppDelegate().categoryID!) 
        }
    }
    
    func getMomentsById(momentId : Int)
    {
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        let strId = String(format:"%d", momentId )
        self.ser.getMomentDataById(id: strId)
        
        
    }
    func callMomentDetailService(momentId : Int)
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.getMomentDetail(id: momentId)
    }

    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        Loader.sharedLoader.hideLoader()
        
        
        if response.statusCode == true
        {
            
            if(apiName == "getMomentDataById")
            {
                
                let profiledata = response as! UserContactInfo
                userMomentsArray =   profiledata.momentsArray
                 //collectionView.collectionViewLayout.invalidateLayout()
                collectionView.reloadData()
               
                
            }
            if(apiName == "getMomentDetail")
            {
                 let momentData = response as! MomentsInfo
                goToMomentDetailPage(index: selectedIndexPath , momentInfo: momentData)
                
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
       MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    func reloadGrid()
    {
        
        collectionView.reloadData()
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        
        return 1
    }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return userMomentsArray.count;
    }
    
   
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnotatedPhotoCell", for: indexPath) as! AnnotatedPhotoCell
        
        let moment = userMomentsArray[indexPath.row] as! MomentsInfo
        if moment.fileType == ".mov"
        {
            cell.imgPlay.isHidden = false
            if let url  = NSURL(string: moment.momentsVideoThumbUrl)
                
            {
                let image = UIImage(named: "heng")
                cell.imageView.sd_setImage(with: url as URL, placeholderImage: image)
            }
        }
        else
        {
            cell.imgPlay.isHidden = true

        if let url  = NSURL(string: moment.momentsUrl)
            
        {
            let image = UIImage(named: "heng")
            cell.imageView.sd_setImage(with: url as URL, placeholderImage: image)
        }
        }
        cell.lblUerName.text  =  moment.momentPostedByUser
        
        //cell.lblCommentCount.text = String(format:"%d",moment.commentsArray.count )
       // cell.lblNoOfView.text = String(format:"%d",moment.viewsCount )

        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width:UIScreen.main.bounds.size.width/2-15, height:150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 1.5;
    }
    
    
    
    func selectItemAtIndexPath(indexPath: NSIndexPath?, animated: Bool, scrollPosition: UICollectionViewScrollPosition)
    {
        
    }
    
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//         momentSelected = userMomentsArray[indexPath.row] as! MomentsInfo
//        selectedIndexPath  = indexPath
//       callMomentDetailService(momentId: momentSelected.momentId)
    }
    func goToMomentDetailPage(index : IndexPath , momentInfo : MomentsInfo)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let momentVC = mainStoryboard.instantiateViewController(withIdentifier: "Moments") as! MomentsViewCtrl
        let cell = collectionView.cellForItem(at: index) as! AnnotatedPhotoCell
        cell.lblNoOfView.text = String(momentInfo.viewsCount)
        momentVC.momentImage =  cell.imageView.image
        momentVC.moment = momentInfo
         momentVC.momentSelected = momentSelected
         momentVC.isComingFrom = isComingFrom
        momentVC.momentIndex = index
        momentVC.modalPresentationStyle = .overCurrentContext
        momentVC.modalTransitionStyle = .crossDissolve
        momentVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        //momentVC.delegate = self
        let pVC = momentVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = collectionView
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        present(momentVC, animated: true, completion: nil)
    }
    
   
}



