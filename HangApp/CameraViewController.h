/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
View controller for camera interface.
*/

@import UIKit;
@import GPUImage;
@protocol CameraViewControllerDelegate
-(void)setCheckInImage:(UIImage *)image;
@end
@interface CameraViewController : UIViewController
@property (nonatomic, strong) NSString*comingfrom;
@property (nonatomic,retain) id delegate;
@property (nonatomic, strong) IBOutlet UIView *progressViewCir;
-  (void)handleLongPress:(UILongPressGestureRecognizer*)sender;
@end
