//
//  InviteHeaderCell.swift
//  HangApp
//
//  Created by Evan Luthra on 04/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class InviteHeaderCell: UITableViewCell {
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var txtSearch: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
