//
//  FeedController.swift
//  HangApp
//
//  Created by Evan Luthra on 12/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

protocol FeedControllerDelegate {
    func getCategoryId(categoryId: Int)
    
}


class FeedController: UIViewController,CAPSPageMenuDelegate {
    var pageMenu : CAPSPageMenuNew?
     var feedVC : PhotoStreamViewController?
     var selectedPageIndex : Int = 0
    
    @IBOutlet weak var bottomView: UIView!
     //weak var delegate:FeedControllerDelegate?
 var categoryNameArray : NSMutableArray = NSMutableArray()
   
 
    override func viewDidLoad() {
        layOutBottomVie()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func layOutBottomVie()
    {
        var controllerArray : [UIViewController] = []
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        for var i in (0..<categoryNameArray.count)
        {
           
                let bannerImg  = categoryNameArray[i] as! BannerImages
                
                feedVC  = mainStoryboard.instantiateViewController(withIdentifier: "photoStream") as? PhotoStreamViewController
                
                feedVC?.isComingFrom = "FeedController"
                feedVC?.title = bannerImg.title
                controllerArray.append(feedVC!)
   
        
       }
       
        
        
       // Customize menu (Optional)
        
        let navColor : UIColor =  UIColor(patternImage: UIImage(named: "navBg.png")!)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(navColor),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
            .menuHeight(40.0),
            
            .menuItemWidth(100.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenuNew(viewControllers: controllerArray, frame: CGRect(x: 0.0, y:20.0, width: UIScreen.main.bounds.width-60, height: self.view.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        self.addChildViewController(pageMenu!)
        self.bottomView.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        pageMenu?.moveToPage(selectedPageIndex)
        
    }

     func willMoveToPage(_ controller: UIViewController, index: Int)
     {
        if index <  categoryNameArray.count
        {
            let bannerImg  = categoryNameArray[index] as! BannerImages
            AppDelegate.getAppDelegate().categoryID = bannerImg.bannerId
        }
       // else if index == 8
        //{
          // pageMenu?.moveToPage(0)
        //}
       
        print(index)

     }
     func didMoveToPage(_ controller: UIViewController, index: Int)
     {
       
        
        
        
    }
    @IBAction func backPressed(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
   

}
protocol FeedControllerdelegate
{
    func myVCDidFinish(controller:FeedController,text:String)
}
