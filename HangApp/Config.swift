//
//  Config.swift
//  BluLint
//
//  Created by Akhilesh on 3/3/16.
//  Copyright © 2016 IRESLAB. All rights reserved.
import UIKit

let KDomain                     = "http://159.203.125.247"
let KAPI_Content_Type            = "Content-Type"
let KAPI_Content_Value          = "application/json"
let KAuth_Header_Type           = "Authorization"


let KobjAppDelegate              = UIApplication.shared.delegate as! AppDelegate
let KView_BgColor                = UIColor(red: 0/255, green: 57/255, blue: 99/255, alpha: 1)
let KButton_BgColor              = UIColor(red: 30/255, green: 177/255, blue: 210/255, alpha: 1)
let KButton_TextColor           = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)



let KAlert_Title                = "Spin Event Info!"

let KHardCode_Username          = "spinuser"
let KHardCode_Pass              = "y^Z##0OAVajRuPeiUGLmE1km"
let KUsername_Blank             = "Please enter the user name"
let KPassword_Blank             = "Please enter the password"
let KValidusername              = "Please enter valid username"
let KValidPassword              = "Please enter valid password"
let KSelect_Event_First         = "Please select event first"
let KSelect_Event_Station_First = "Please select event and station first"
let KNO_Network                 = "The network connection was lost."
let KNo_DataFound               = "No data found try again"
let KSave                       = "Record save successfully"
let KEnter_Valid_Email          = "Please enter the valid email address"

class Config: NSObject {

}
