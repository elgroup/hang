//
//  SelectedSuggestionTableViewCell.swift
//  BluLint
//
//  Created by iRESLAB19 on 28/06/16.
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgStatus: UIImageView!
    @IBOutlet var txtDesc: UITextView!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var btnStatus: UIButton!
    @IBOutlet var lblName: UILabel!
   

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
