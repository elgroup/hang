//
//  UserFollowers.swift
//  HangApp
//
//  Created by Evan Luthra on 20/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class UserFollowers: NSObject
{
    var fullName = String()
    var emailId = String()
    var imageUrl = String()
    var userName = String()
    var authToken = String()
    var id = 0
    var user_id = 0
    var followed_id = 0
   var  is_followed = false

}
