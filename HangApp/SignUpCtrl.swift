//
//  SignUpCtrl.swift
//  HangApp
//
//  Created by Sujeet Singh on 22/02/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class SignUpCtrl: UITableViewController, AppServiceDelegate,UITextFieldDelegate {
    @IBOutlet var bgSinUpView: UIView!
    @IBOutlet var bgViewSin: UIView!
    @IBOutlet var selectionViewleft: UIView!
    @IBOutlet var selectionViewRight: UIView!
    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var lblError: UILabel!
    var activeTextField = UITextField()
    var tempEmailAddress:String!
     var tempUserName:String!
    var isEmailEditing : Bool!
    var isUserNameEditing : Bool!
     var strUserType:String?
    let ser:AppService = AppService()
    override func viewDidLoad() {
        super.viewDidLoad()
       // bgViewSin.layer.cornerRadius = 6;
       // bgViewSin.layer.masksToBounds = true
        //lblError.isHidden = true
         strUserType = "personal"
        let tableBackgroundView :UIImageView!
        tableBackgroundView = UIImageView()
        tableBackgroundView.image = UIImage.init(named: "SignUpBg")
        self.tableView.backgroundView  = tableBackgroundView
         self.tableView.setContentOffset(tableView.contentOffset, animated: false)
        // Do any additional setup after loading the view.
              selectionViewRight.isHidden = true
        self.tableView.backgroundColor = UIColor.clear
        txtUserName.delegate = self;
        txtEmail.delegate = self;
        isEmailEditing = false
        isUserNameEditing = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func personalBtnClicked(_ sender: AnyObject)
    {
         strUserType = "personal"
        selectionViewleft.isHidden = false
        selectionViewRight.isHidden = true
    }
    @IBAction func businessBtnClicked(_ sender: AnyObject)
    {
          strUserType = "business"
        selectionViewleft.isHidden = true
        selectionViewRight.isHidden = false
    }
    @IBAction func signInBtnClicked(_ sender: AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func signUpBtnClicked(_ sender: AnyObject)
    {
        if validateAndProceed(email: txtEmail.text!, password: txtPassword.text!,confirmPassword: txtConfirmPassword.text!)
        {
            let info:UserInfo = UserInfo()
            info.userName = txtUserName.text! as NSString!
            info.emailId = txtEmail.text! as NSString!
            info.mobileNumber = txtMobileNo.text! as NSString
            info.password = txtPassword.text as NSString!
            info.userType = strUserType as NSString!
             Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.registerUserWithInfo(userInfo: info)
        }
    }
  
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            if(apiName == "sign_up")
            {
                
                
                let message  = "Thank You! Sign-Up process would be complete after the verification of ".appending("\(txtEmail.text!) mail account, by clicking the 'Confirm my account' link in sent mail to this account. Please verify your mail account first to continue!")
                
                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                
                let continueAction = UIAlertAction(title: "Continue", style: .default, handler: { (void) in
                    Loader.sharedLoader.showLoaderOnScreen(vc: self)
                    let dict = GlobalInfo.sharedInstance.getContacts()
                    AppService.sharedInstance.delegate=self
                    AppService.sharedInstance.getSuggestionList(emails: dict as [String])
                      })
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (void) in
                     self.dismiss(animated: true, completion: nil)
                
                })
               
                alert.addAction(cancelAction)
                alert.addAction(continueAction)
                self.present(alert, animated: true, completion: nil)
               
                
            }
            if(apiName == "validateEmail")
            {
                isEmailEditing = false
                txtPassword.becomeFirstResponder()
               //MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: response.statusDescription!, viewcontroller: self)
                
            }
            if(apiName == "validateUseName")
            {
                isUserNameEditing = false
                 txtEmail.becomeFirstResponder()
              //  MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: response.statusDescription!, viewcontroller: self)
              
            }
            
            if(apiName == "getSuggestionList")
            {
                UserDefaults.standard.set(true, forKey: kIsAppAlreadyLogin)
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let suggestionVC = mainStoryboard.instantiateViewController(withIdentifier: "Suggestion") as! SuggestionVc
                suggestionVC.suggestionUsersArray = (response as! HangUsersParserInfo).hangUsers
                self.navigationController?.pushViewController(suggestionVC, animated: true)
                Loader.sharedLoader.hideLoader()
                
            }
        }
    }
  

    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
      
        if  response.statusDescription! == "You have to confirm your email address before continuing."
        {
            let message  = "Thank You! Sign-Up process would be complete after the verification of ".appending("\(txtEmail.text!) mail account, by clicking the 'Confirm my account' link in sent mail to this account. Please verify your mail account first to continue!")
            
            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
            
            let continueAction = UIAlertAction(title: "Contiue", style: .default, handler: { (void) in
                Loader.sharedLoader.showLoaderOnScreen(vc: self)
                let dict = GlobalInfo.sharedInstance.getContacts()
                AppService.sharedInstance.delegate=self
                AppService.sharedInstance.getSuggestionList(emails: dict as [String])
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (void) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alert.addAction(cancelAction)
            alert.addAction(continueAction)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            if(isEmailEditing==true){
                
                txtEmail.becomeFirstResponder();
            }
            else if(isUserNameEditing==true){
               
                 txtUserName.becomeFirstResponder();
            }
           
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
            
            
            
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y>0)
        {
            scrollView.setContentOffset(CGPoint(x:0, y: 0), animated: true)
        }
    }
    
    func validateAndProceed(email:String,password:String,confirmPassword:String)->Bool{
        
        if email.characters.count <= 0
        { //  lblError.isHidden = false
            //lblError.text = "Please enter email address"
            MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Please enter email address" , viewcontroller:self)
            
            return false
        }
        else if !(Validator.sharedValidator.isValidEmail(testStr: email)){
           // lblError.isHidden = false
            //lblError.text = "Please enter valid email address"
            MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Please enter valid email address" , viewcontroller:self)
            return false
        }else if password.characters.count == 0{
            //lblError.isHidden = false
            //lblError.text = "Please enter password"
            MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Please enter password" , viewcontroller:self)
            return false
        }
        else if password.characters.count < 6
        {
          //  lblError.isHidden = false
           // lblError.text = "Password length should be greater than 6 characters "
            MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Password length should be greater than 6 characters" , viewcontroller:self)
            
            return false
        }
        else if password != confirmPassword
        {
           // lblError.isHidden = false
           // lblError.text = "These passwords don't match. Try again?"
            MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Password and confiem password not matched.Try again? " , viewcontroller:self)
            
            return false
        }
      //  lblError.isHidden = true
        return true
    }
    
    
    //MARK: Textfield delegates
    
    func textFieldShouldReturn(_ userText: UITextField) -> Bool
    {
        switch userText
        {
        case txtEmail: break
            
           
        case txtUserName: break
            
            
        case txtPassword:
            txtConfirmPassword.becomeFirstResponder()
        default:
            break
        }
        userText.resignFirstResponder()
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
       
//        //         lblError.isHidden = true
//        
//        
//        switch textField
//        {
//            
//        case txtUserName:
//            if DeviceType.IS_IPHONE_4_OR_LESS
//            {
//                self.view.frame.origin.y = -100
//            }
//            else if DeviceType.IS_IPHONE_5
//            {
//                self.view.frame.origin.y = -40
//            }
//            else if DeviceType.IS_IPHONE_6
//            {
//                self.view.frame.origin.y = -20
//            }
//            
//        case txtPassword:
//            if DeviceType.IS_IPHONE_4_OR_LESS
//            {
//                self.view.frame.origin.y = -100
//            }
//            else if DeviceType.IS_IPHONE_5
//            {
//                self.view.frame.origin.y = -80
//            }
//            else if DeviceType.IS_IPHONE_6
//            {
//                self.view.frame.origin.y = -40
//            }
//            
//        case txtConfirmPassword:
//            if DeviceType.IS_IPHONE_4_OR_LESS
//            {
//                self.view.frame.origin.y = -100
//            }
//            else if DeviceType.IS_IPHONE_5
//            {
//                self.view.frame.origin.y = -120
//            }
//            else if DeviceType.IS_IPHONE_6
//            {
//                self.view.frame.origin.y = -60
//            }
//        default:
//            print("")
//        }
    }
    
    func isEmpty(text:String) -> Bool{
        if text.isEmpty == true{
            return true
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
       // self.view.frame.origin.y = 0
        switch textField
        {
            
        case txtEmail:
            if isEmpty(text: txtEmail.text!) == false
                
            {
                
                tempEmailAddress = txtEmail.text!.trimmingCharacters(in: .whitespaces)
                if !(Validator.sharedValidator.isValidEmail(testStr: tempEmailAddress!))
                {
                   // lblError.isHidden = false
                  //  lblError.text = "Please enter valid email address"
                    MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Please enter valid email address" , viewcontroller:self)
                }
                else{
                    if(isEmailEditing==false){
                        isUserNameEditing = false
                     Loader.sharedLoader.showLoaderOnScreen(vc: self)
                    ser.delegate=self
                    ser.validateEmail(email: tempEmailAddress!)
                   // lblError.isHidden = true
                   
                        isEmailEditing = true;
                    }
                    else{
                      isEmailEditing = false
                    }
                    
                    
                   
                }
            }
            
            break
            
            
        case txtUserName:
            print(isUserNameEditing!)
            if (isEmpty(text: txtUserName.text!) == false
                )
                
            {
                
                if(isUserNameEditing == false){
                isEmailEditing = false
                tempUserName = txtUserName.text!.trimmingCharacters(in: .whitespaces)
                Loader.sharedLoader.showLoaderOnScreen(vc: self)
                ser.delegate=self
                ser.validateUseName(usename: tempUserName!)
              
                    isUserNameEditing = true
              
                
                
                
            }
            else{
                isUserNameEditing = false;
            }
            }
            
            break

            
        case txtPassword:
            
            isEmailEditing = false;
            isUserNameEditing = false

            if isEmpty(text: txtPassword.text!) == false
                
            {
                if (txtPassword.text?.characters.count)! < 6
                {
                   // lblError.isHidden = false
                  //  lblError.text = "Password length should be greater than 6 characters"
                     MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Password length should be greater than 6 characters" , viewcontroller:self)
                }
                else{
                    
                   // lblError.isHidden = true
                }
            }
            
            break
        case txtConfirmPassword:
            
            isEmailEditing = false;
            isUserNameEditing = false

            if isEmpty(text: txtConfirmPassword.text!) == false
                
            {
                if txtConfirmPassword.text != txtPassword.text
                {
                   // lblError.isHidden = false
                  //  lblError.text = "These passwords don't match. Try again?"
                    MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString:"Password and confiem password not matched.Try again? " , viewcontroller:self)
                }
                else{
                    
                    //lblError.isHidden = true
                }
            }
            
            break
            
        default:
            
            isEmailEditing = false;
            isUserNameEditing = false

            break
        }
    }
    
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
         self.view.endEditing(true)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
