//
//  FriendsViewCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 01/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
class FriendsViewCtrl: UIViewController ,UITableViewDataSource,UITableViewDelegate ,AppServiceDelegate {
   let ser:AppService = AppService()
     var hangUser = UserContactInfo()
     @IBOutlet weak var friendsTable: UITableView!
    var userFriendsArray:NSMutableArray = []
    override func viewDidLoad() {
        //callFriendsService()
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            
            if(apiName == "getFriendsList")
            {
                
                userFriendsArray = (response as! HangUsersParserInfo).hangUsers
                friendsTable.reloadData()
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userFriendsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendCell", for: indexPath as IndexPath) as! FriendCell
        
              hangUser  = userFriendsArray[indexPath.row] as! UserContactInfo
                 if hangUser.fullName != "" {
                cell.lblName.text = hangUser.fullName
                   }
        if hangUser.location != "" {
            cell.lblLocation.text = hangUser.location
        }
                cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
                cell.imgUser.clipsToBounds = true;
        //let image = UIImage(named: "defaultUserImage")
       // cell.imgUser.sd_setImage(with: NSURL(string: hangUser.imageUrl) as! URL, placeholderImage: image)
        
        Alamofire.request(hangUser.imageUrl, method: .get).responseImage { response in
            guard let image = response.result.value else {
                                return
            }
            cell.imgUser.image = image
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        return 55.0
    }
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 55))
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "accountCell") as! CustomTableViewCell
       
        headerCell.frame = headerView.frame
        
        headerView.addSubview(headerCell)
        headerCell.btnCheckin.addTarget(self, action: #selector(FriendsViewCtrl.openSearchView), for: .touchUpInside)
        
        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
          hangUser  = userFriendsArray[indexPath.row] as! UserContactInfo
        if hangUser.userType == "business"
        {
            
            let controller : OtherBusinessProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "otherBusinessAccount") as! OtherBusinessProfileVC
            controller.user_id = hangUser.userId
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            let controller : OthersAccountViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "otherAccount") as! OthersAccountViewCtrl
            controller.user_id = hangUser.userId
           self.navigationController?.pushViewController(controller, animated: true)            
        }
        
        
        
        
    }
    
    
    func openSearchView()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let searchVC = mainStoryboard.instantiateViewController(withIdentifier: "searchCtrl") as! SearchUserController
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
   // func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       // return 80
    //}
    

}
