//
//  MomentsViewCtrl.swift
//  
//
//  Created by Evan Luthra on 01/03/17.
//
//

import UIKit
import AVFoundation
import AVKit

//protocol momentControllerDelegate
//{
   // func reloadProfileView(index : IndexPath ,noOfViews : Int )
    
//}
class MomentsViewCtrl: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate,AppServiceDelegate
{
    @IBOutlet  weak var momentImage: UIImage!
      let ser:AppService = AppService()
     var likeCount = Int()
     var isComingFrom = String()
     var  momentIndex : IndexPath = []
   // var delegate:momentControllerDelegate?
     var moment = MomentsInfo()
     var momentSelected = MomentsInfo()
    
     @IBOutlet weak var momentDetailTable: UITableView!
    override func viewDidLoad() {
        likeCount = moment.likeCount
        UIApplication.shared.statusBarStyle = .lightContent
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        
        self.dismiss(animated: true, completion: nil)
        //delegate?.reloadProfileView(index: momentIndex, noOfViews: moment.viewsCount)
            
    }
    
    
    @IBAction func messageBtnClicked(_ sender: UIButton)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "CommentListVC") as! CommentListVC
        popupVC.apiname = "/comments"
        popupVC.moment_id = moment.momentId
        popupVC.userCommentArray = moment.commentsArray
        popupVC.modalPresentationStyle = .overCurrentContext
        popupVC.modalTransitionStyle = .crossDissolve
        popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        let pVC = popupVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = sender
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        present(popupVC, animated: true, completion: nil)
        
   }
    func callLikesService()
    {
       
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.sendLikeOnMoment(momentId: moment.momentId)
    }
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        Loader.sharedLoader.hideLoader()
        
        if response.statusCode == true
        {
            
            if(apiName == "sendLikeOnMoment")
            {
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: response.statusDescription!, viewcontroller: self)
                likeCount = response.momentLikeCount!
                
            momentDetailTable.reloadData()
        }
           
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }

    @IBAction func likeBtnClicked(_ sender: Any)
    {
       callLikesService()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.view.frame.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "momentCell", for: indexPath as IndexPath) as! FeedsCell
        cell.imgPlaces.image  = momentImage
        if moment.fileType == ".mov" {
            cell.btnPlay.isHidden = false
             cell.btnPlay.addTarget(self, action: #selector(MomentsViewCtrl.playButtonClicked(_:)), for: .touchUpInside)

        }
        else
        {
        cell.btnPlay.isHidden = true
        }
        
        
       // let userName =  UserDefaults.standard.object(forKey: "userFullName")
        let strUserId = String(format:"%@",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
        
         let distance   = Float(moment.momentDistance)
        cell.checkInDescr.tag = indexPath.row
        if(moment.tag_Users.count > 0)
        {
        if strUserId != String((moment.tag_Users[0] as! Tags).tagByUser_id )
        {
            if moment.tag_Users.count>1
            {
                let results = [(moment.tag_Users[0] as! Tags).tagByUser_Name ,"other","You",String((moment.tag_Users[0] as! Tags).tagByUser_id)]
                let tagsString = String(format:"%@  Invite You & other", (moment.tag_Users[0] as! Tags).tagByUser_Name )
                cell.checkInDescr.attributedText =  createClickableLink(userNames:results as! [String] , checkInDescr: cell.checkInDescr , main_string:tagsString  )
            }
            else
            {
                
                let results = [(moment.tag_Users[0] as! Tags).tagByUser_Name ,"You",String((moment.tag_Users[0] as! Tags).tagByUser_id)]
                let tagsString = String(format:"%@  Invite You", (moment.tag_Users[0] as! Tags).tagByUser_Name )
                cell.checkInDescr.attributedText =  createClickableLink(userNames:results as! [String] , checkInDescr: cell.checkInDescr , main_string:tagsString  )
            
            }
            
        }
        else
        {
            if moment.tag_Users.count>1
            {
            let results = [(moment.tag_Users[0] as! Tags).tagUser_Name ,"other","You",String((moment.tag_Users[0] as! Tags).tagUser_id)]
            let tagsString = String(format:"You Invite %@ & other", (moment.tag_Users[0] as! Tags).tagUser_Name )
             cell.checkInDescr.attributedText =  createClickableLink(userNames:results as! [String] , checkInDescr: cell.checkInDescr , main_string:tagsString  )
            }
            else
            {
                let results = [(moment.tag_Users[0] as! Tags).tagUser_Name ,"You",String((moment.tag_Users[0] as! Tags).tagUser_id)]
                let tagsString = String(format:"You Invite %@", (moment.tag_Users[0] as! Tags).tagUser_Name )
                cell.checkInDescr.attributedText =  createClickableLink(userNames:results as! [String] , checkInDescr: cell.checkInDescr , main_string:tagsString  )
            
            }
        
        }
          cell.lblLocation.text =  String(format:"%@\n%@",moment.caption,moment.address )
            
            
            
                    if let url  = NSURL(string: (self.moment.tag_Users[0] as! Tags).tagByUserImageUrl),
                        let data = NSData(contentsOf: url as URL)
                    {
                        cell.imgUser.image = UIImage(data: data as Data)
                    }
                    
            
        }
        else
        {
            
            if isComingFrom == "FeedController"
            {
   let results = [momentSelected.momentPostedByUser ,String(momentSelected.momentPostedByUserId)]
            
        let tagsString = String(format:"%@", momentSelected.momentPostedByUser )
            
        cell.checkInDescr.attributedText =  createClickableLink(userNames:results as! [String] , checkInDescr: cell.checkInDescr , main_string:tagsString  )
            
            
            if let url  = NSURL(string: momentSelected.momentPostedByUserImage as String),
                let data = NSData(contentsOf: url as URL)
            {
                 cell.imgUser.image = UIImage(data: data as Data)
            }
            }
            else
             {
            
                if let url  = NSURL(string: UserDefaults.standard.object(forKey: "userImage") as! String),
                    let data = NSData(contentsOf: url as URL)
                {
                    cell.imgUser.image = UIImage(data: data as Data)
                }
                cell.checkInDescr.text = UserDefaults.standard.object(forKey: "userFullName") as! String
            
            }
            
    
           if moment.caption != ""
            {
                
     cell.lblLocation.text =  String(format:"%@\n%@",moment.caption,moment.address)

            }
            else
            {
              cell.lblLocation.text =  String(format:"%@%@",moment.caption,moment.address )
            }
              
        }
        
       cell.lblDuration.text = MiscUtils.sharedMiscUtils.getDurationFromCurrentDate(dateString: moment.momentDate)
       cell.lblCommentCount.text = String(format:"%d Comments",moment.commentsArray.count )
         cell.lblLikesCount.text = String(format:"%d Likes",likeCount )
         cell.lblViewsCount.text = String(format:"%d Views",moment.viewsCount)
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        cell.lblDistance.text  = String(format:"%0.1f km",distance )
     
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    @IBAction func playButtonClicked(_ sender: Any)
    {
        playVideo(videoUrl: URL(string: moment.momentsVideoUrl)!)
    }
    public  func playVideo( videoUrl : URL) {
        
        let player = AVPlayer(url: videoUrl)
        
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
        self.dismiss(animated: true, completion: nil)
    }
    func createClickableLink(userNames : [String] , checkInDescr:UITextView , main_string:String ) -> NSMutableAttributedString
    {
        
       
        let attribute = NSMutableAttributedString.init(string: main_string)
        for var i in (0..<userNames.count).reversed()
        {
            
            
         
            let range = (main_string as NSString).range(of: userNames[i] as String)
            
            //let replaced = String(userNames[i].characters.filter {$0 != " "})
            
       let userId  =  userNames.last
            let fullNameArr = userNames[i].components(separatedBy: " ")
        let replaced = fullNameArr.joined(separator:"@")
           
         let resultStr = replaced+"#"+userId!
            
            attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 18) as Any , range: range)
            attribute.addAttribute(NSLinkAttributeName, value:resultStr, range: range)
            checkInDescr.linkTextAttributes = [ NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.86, green: 0.00, blue: 0.43, alpha: 1.00)]
            
            
           
        }
        let range1 = (main_string as NSString).range(of: "Invite")
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.white , range: range1)
         attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 18) as Any , range: range1)
        let range2 = (main_string as NSString).range(of: "&")
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.white , range: range2)
         attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 18) as Any , range: range2)
        return attribute
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "popUp") as! PopUpViewCtrl
        
        
      var array = String(describing: URL).components(separatedBy: "#")
        if array[0] != "other" &&  array[0] != "You"
        {
            popupVC.messageReceiver_id = array.last!
            popupVC.messageReceiver_name = array[0]
            
            popupVC.modalPresentationStyle = .overCurrentContext
            popupVC.modalTransitionStyle = .crossDissolve
            popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
            let pVC = popupVC.popoverPresentationController
            pVC?.permittedArrowDirections = .any
            pVC?.delegate = self
            pVC?.sourceView = self.view
            pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
            
            self.present(popupVC, animated: true, completion: nil)
        }
        else if array[0] == "other"
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle: nil)
            let tagsController = mainStoryboard.instantiateViewController(withIdentifier: "tagUsers") as! TagUsersVC
            tagsController.modalPresentationStyle = .overCurrentContext
            tagsController.modalTransitionStyle = .crossDissolve
            tagsController.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
            let pVC = popupVC.popoverPresentationController
            pVC?.permittedArrowDirections = .any
            pVC?.delegate = self
            pVC?.sourceView = self.view
            pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
            tagsController.tagUsersArray = moment.tag_Users
            
            
            self.present(tagsController, animated: true, completion: nil)
        }
        
        
        return true;
    }
    
    

}
