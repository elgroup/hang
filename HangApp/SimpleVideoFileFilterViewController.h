#import <UIKit/UIKit.h>
#import "YLProgressBar.h"
@import GPUImage;

@interface SimpleVideoFileFilterViewController : UIViewController
{
    GPUImageMovie *movieFile;
    //GPUImageOutput<GPUImageInput> *filter;
    GPUImageMovieWriter *movieWriter;
    NSTimer * timer;
    UIImageView *filterImageview;
    UIScrollView* scrollView;
    NSURL *movieURL;
    int selectedFilterIndex;
    }
@property (nonatomic, strong) NSString*videoUrl;
@property (nonatomic, strong)  NSMutableArray *filtersImageArray;

@property (retain, nonatomic) IBOutlet UILabel *progressLabel;
@property (retain, nonatomic) IBOutlet UIImageView *preview;
@property (nonatomic, strong) IBOutlet YLProgressBar *progressBarFlatRainbow;
- (IBAction)updatePixelWidth:(id)sender;

@end
