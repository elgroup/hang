
//
//  AppDelegate.swift
//  HangApp
//
//  Created by Sujeet Singh on 22/02/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import CoreData
import NotificationCenter
import UserNotifications
import CoreLocation
import GoogleMaps
import Alamofire
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate,CLLocationManagerDelegate{
    let ser:AppService = AppService()
    var window: UIWindow?
     var pageMenu : CAPSPageMenu?
      var didFindLocation:Bool?=nil
     var getUpdatedLongLat: (() -> Void)?
    let locationManager = CLLocationManager()
    var bannerImagesArray = NSMutableArray()
    var yourstoriesArray = NSMutableArray()
    var coordinates:CLLocationCoordinate2D?
     var categoryID:Int?
    var profileCategoryID : Int = 0
    
    

     var videoURL:NSURL?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        startUpdatinLocation()
        UIApplication.shared.applicationIconBadgeNumber = 0
                //Fabric.with([Crashlytics.self])
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(grant, error)  in
                if error == nil {
                    if grant {
                        application.registerForRemoteNotifications()
                    } else {
                        UserDefaults.standard.set("123456", forKey: "devieToken")
                        UserDefaults.standard.synchronize()
                        //User didn't grant permission
                    }
                } else {
                    print("error: ",error!)
                }
            })
        } else {
            // Fallback on earlier versions
            let notificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
        }
      
        checkForLogin()
      
        fetchLocation()

        return true
    }
    
  
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        print("DEVICE TOKEN = \(deviceToken)")
        var deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        deviceTokenString = deviceTokenString.replacingOccurrences(of: ">", with: "")
        deviceTokenString = deviceTokenString.replacingOccurrences(of: " ", with: "")
        deviceTokenString = deviceTokenString.replacingOccurrences(of: "<", with: "")
        
        UserDefaults.standard.set(deviceTokenString, forKey: "devieToken")
        UserDefaults.standard.synchronize()
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> ()) {
        
      print(userInfo)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserDefaults.standard.set("123456", forKey: "devieToken")
        UserDefaults.standard.synchronize()
        print("Couldn't register: \(error)")
        print(error)
    }
    func checkForLogin()
    {
        
       
        if(UserDefaults.standard.bool(forKey: kIsAppAlreadyLogin))
        {
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let dashboardVC = mainStoryboard.instantiateViewController(withIdentifier: "WazzaTabBarController") as! WazzaTabBarController
            //let navigationCtrl = UINavigationController.init(rootViewController: dashboardVC)
           
            self.window?.rootViewController = dashboardVC

           
         }
      
    }
    
    
    // MARK: Location Updates Methods
    func fetchLocation()
    {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    func startUpdatinLocation()
    {
        didFindLocation = false
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation()
    {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if didFindLocation == false
        {
            didFindLocation = true
            self.stopUpdatingLocation()
            coordinates = locations.last?.coordinate
            if let _ = self.getUpdatedLongLat
            {
                getUpdatedLongLat!()
            }
            print("locations = \(coordinates!.latitude) \(coordinates!.longitude)")
        }
    }
    

       func goToHome()
    {
        var controllerArray : [UIViewController] = []
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 : NotificationViewController = mainStoryboard.instantiateViewController(withIdentifier: "notify") as! NotificationViewController
        
        controller1.title = ""
        controllerArray.append(controller1)
        
        
        let controller2 : HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "home") as! HomeViewController
        
        
        controller2.title = ""
        controllerArray.append(controller2)
        let controller3 : AccountViewController = mainStoryboard.instantiateViewController(withIdentifier: "account") as! AccountViewController
        
        controller3.title = ""
        controllerArray.append(controller3)
        
        // Customize menu (Optional)
        
        let navColor : UIColor =  UIColor(patternImage: UIImage(named: "navBg.png")!)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(navColor),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.white),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
            .menuMargin(5),
            .menuHeight(60.0),
            .menuItemWidth(70),
            .centerMenuItems(false)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.window!.frame.width, height: (self.window?.frame.height)!), pageMenuOptions: parameters)
        
        self.window?.rootViewController?.addChildViewController(pageMenu!)
        self.window?.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController:self.window?.rootViewController)
    }
    
    
    
    
  
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
//    // MARK: - Container View Controller
//    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
//        return true
//    }
//    
//    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
//        return true
//    }
//    
    

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    
    
    {
//        let urlScheme = url.scheme //[URL_scheme]
//        let host = url.host //red
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    class func getAppDelegate() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "HangApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

