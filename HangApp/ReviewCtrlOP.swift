//
//  ReviewCtrlOP.swift
//  HangApp
//
//  Created by Evan Luthra on 22/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class ReviewCtrlOP:UIViewController ,UITableViewDataSource,UITableViewDelegate ,AppServiceDelegate, FloatRatingViewDelegate ,UITextViewDelegate{
    var user_id = Int()
    let ser:AppService = AppService()
    var review = Review()
    var reviewComment:String!
    var ratingCount:String!
    @IBOutlet weak var reviewTable: UITableView!
    var userReviewsArray:NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        reviewComment = ""
        ratingCount = "3"
        
    }
    
    func callCreateReviewService()
    {
        let reviews:Review = Review()
        reviews.reviewDesc = reviewComment
        reviews.user_id = user_id
        reviews.ratingCount = ratingCount
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.createReview(review: reviews)
    }
    
    
    func getReviewService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.getReviewsById(id: String(user_id))
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        Loader.sharedLoader.hideLoader()
        
        if response.statusCode == true
        {
            
            if(apiName == "createReview")
            {
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: response.statusDescription!, viewcontroller: self)
                reviewTable.isHidden = false
                getReviewService()
                
                reviewTable.reloadData()
            }
            if(apiName == "getReviewsById")
            {
                
                let profiledata = response as! UserContactInfo
                userReviewsArray = profiledata.reviewArray
                
                reviewTable.reloadData()
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userReviewsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath as IndexPath) as! ReviewCell
        
        review  = userReviewsArray[indexPath.row] as! Review
        if review.name != "" {
            cell.lblName.text = review.name
        }
        let tap = UITapGestureRecognizer()
        
        tap.addTarget(self, action: #selector(ReviewsViewController.tap(sender:)))
        tap.numberOfTapsRequired = 1
        cell.addGestureRecognizer(tap)
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        let image = UIImage(named: "defaultUserImage")
        cell.imgUser.sd_setImage(with: NSURL(string: review.imageUrl) as! URL, placeholderImage: image)
        cell.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        cell.floatRatingView.fullImage = UIImage(named: "StarFull")
        cell.floatRatingView.delegate = self
        cell.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        cell.floatRatingView.maxRating = 5
        cell.floatRatingView.minRating = 1
        cell.floatRatingView.rating = Float(review.ratingCount)!
        cell.txtReview.text = review.reviewDesc
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.lblDuration.text = MiscUtils.sharedMiscUtils.getDurationFromCurrentDate(dateString: review.reviewDate)
        
        return cell
    }
    
    
    func tableView( _ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let currentUserId = String(format:"%d",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
        let userId = String(format:"%d",user_id)
        if userId != currentUserId
        {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 125))
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "createreviewCell") as! CreateReviewCell
            let tap = UITapGestureRecognizer()
            
            tap.addTarget(self, action: #selector(ReviewsViewController.tap(sender:)))
            tap.numberOfTapsRequired = 1
            // headerCell.addGestureRecognizer(tap)
            headerCell.frame = footerView.frame
            headerCell.txtPostReview.delegate = self
            headerCell.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
            headerCell.floatRatingView.fullImage = UIImage(named: "StarFull")
            
            headerCell.floatRatingView.delegate = self
            headerCell.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
            headerCell.floatRatingView.maxRating = 5
            headerCell.floatRatingView.minRating = 1
            headerCell.floatRatingView.rating = 3
            headerCell.btnPost.accessibilityLabel =  headerCell.txtPostReview.text
            headerCell.btnPost.addTarget(self, action: #selector(ReviewsViewController.postReview(sender:)), for: .touchUpInside)
            footerView.addSubview(headerCell)
            return footerView
        }
        
        
        return nil
        
    }
    
    func tableView( _ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 125.0
    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Post review") {
            textView.text  = ""
            textView.textColor = UIColor.darkGray
            
            
        }
        
        
        if DeviceType.IS_IPHONE_5
        {
            reviewTable.frame.origin.y = -260
        }
        else if DeviceType.IS_IPHONE_6
        {
            reviewTable.frame.origin.y = -260
        }
        else
        {
            reviewTable.frame.origin.y = -260
        }
        
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Post review"
            textView.textColor = UIColor.lightGray
        }
        reviewTable.frame.origin.y = 0
    }
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        reviewComment = textView.text
        
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func postReview(sender: UIBarButtonItem )
    {
        self.view.endEditing(true)
        
        callCreateReviewService()
    }
    func tap(sender: UITapGestureRecognizer )
    {
        self.view.endEditing(true)
        
    }
    
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float)
    {
        ratingCount =  String(rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
    {
        
    }
    
    
    
    
}
