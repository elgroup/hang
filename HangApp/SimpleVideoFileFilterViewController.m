#import "SimpleVideoFileFilterViewController.h"
#import <Photos/Photos.h>
#import "HangApp-Swift.h"
@implementation SimpleVideoFileFilterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
    GPUImageFilter *filter = [[GPUImageFilter alloc] init];
    [self  filterthevideo:filter];
        [_progressBarFlatRainbow setProgress:0.0 animated:YES];
         scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-100, [[UIScreen mainScreen] bounds].size.width, 100)];
        scrollView.backgroundColor = [UIColor whiteColor];
        NSArray *filtersNameArray = [[NSArray alloc] initWithObjects: @"None", @"Grayscale", @"Sepia", @"Toon",@"Pinch",@"Sketch", @"ColorInvert" ,@"Pixellate" , nil];
    
        for (int i=0; i<_filtersImageArray.count; i++)
        {
            filterImageview = [[UIImageView alloc] initWithFrame:CGRectMake(75*i+5, 20, 75, 80)];
            filterImageview.image = [_filtersImageArray objectAtIndex:i];
            filterImageview.tag = i;
           UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapfilters:)];
            tapRecognizer.numberOfTouchesRequired = 1;
            tapRecognizer.numberOfTapsRequired = 1;
            filterImageview.userInteractionEnabled = YES;
            [filterImageview addGestureRecognizer:tapRecognizer];
        UILabel *lblFilterName = [[UILabel alloc] initWithFrame:CGRectMake(75*i+5, 0, 75, 20)];
            lblFilterName.font = [UIFont systemFontOfSize:13.0];
            lblFilterName.textColor = [UIColor darkGrayColor];
            lblFilterName.userInteractionEnabled = NO;
            lblFilterName.textAlignment = NSTextAlignmentCenter;
            lblFilterName.text = [filtersNameArray objectAtIndex:i];
            [scrollView addSubview:lblFilterName];
            [scrollView addSubview:filterImageview];
            
        }
        
        [self.view addSubview:scrollView];
        [scrollView setContentSize:CGSizeMake((80) * [_filtersImageArray count], scrollView.frame.size.height)];
    
   
}
- (void)filterthevideo :(GPUImageOutput<GPUImageInput>*)filter
{
    
    double delayInSeconds = 1.0;
   
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
         [self initFlatRainbowProgressBar];
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                 target:self
                                               selector:@selector(retrievingProgress)
                                               userInfo:nil
                                                repeats:YES];
        movieFile = [[GPUImageMovie alloc] initWithURL:[AppDelegate getAppDelegate].videoURL];
        movieFile.runBenchmark = YES;
        movieFile.playAtActualSpeed = NO;
       
        [movieFile addTarget:filter];
        
        // Only rotate the video for display, leave orientation the same for recording
        GPUImageView *filterView = (GPUImageView *)self.view;
        [filter addTarget:filterView];
        if ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationPortrait)
        {
            [filter setInputRotation:kGPUImageRotateRight atIndex:0];
        }
        // In addition to displaying to the screen, write out a processed version of the movie to disk
        NSString *pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
        unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
        movieURL = [NSURL fileURLWithPath:pathToMovie];
        
        movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:CGSizeMake(360.0, 667.0)];
        //movieWriter.transform =  CGAffineTransformMakeRotation(180);
        [filter addTarget:movieWriter];
        
        // Configure this for video from the movie file, where we want to preserve all video frames and audio samples
        movieWriter.shouldPassthroughAudio = YES;
        movieFile.audioEncodingTarget = movieWriter;
        [movieFile enableSynchronizedEncodingUsingMovieWriter:movieWriter];
        
        [movieWriter startRecording];
        [movieFile startProcessing];
        
      
        
        [movieWriter setCompletionBlock:^{
            [filter removeTarget:movieWriter];
            [movieWriter finishRecording];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [timer invalidate];
                
                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _progressBarFlatRainbow.progress = 0.0;
                    scrollView.userInteractionEnabled = YES;
                });
                NSData *movieData = [NSData dataWithContentsOfURL:movieURL];
                
            });
        }];
    });

}

- (void)tapfilters:(UITapGestureRecognizer*)sender {
    
   scrollView.userInteractionEnabled = NO;
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag);
    selectedFilterIndex = (int)view.tag;
    [_progressBarFlatRainbow setProgress:0.0 animated:YES];
    switch ((int)view.tag)
    {
        case 0:
        {
           GPUImageFilter *noneFilter = [[GPUImageFilter alloc] init];
             [self  filterthevideo:noneFilter];
        }
            break;
        case 1:
        {
            GPUImageGrayscaleFilter *grayFilter = [[GPUImageGrayscaleFilter alloc] init];
            [self  filterthevideo:grayFilter];
        }
            break;
        case 2:
        {
            GPUImageSepiaFilter *sepiaFilter = [[GPUImageSepiaFilter alloc] init];
            [self  filterthevideo:sepiaFilter];
        }
            break;
        case 3:
        {
            GPUImageToonFilter *toonFilter = [[GPUImageToonFilter alloc] init];
            [self  filterthevideo:toonFilter];
        }
            break;
        case 4:
        {
            GPUImagePinchDistortionFilter *pinchFilter = [[GPUImagePinchDistortionFilter alloc] init];
            [self  filterthevideo:pinchFilter];
        }
            break;
        case 5:
        {
           GPUImageSketchFilter *sketchFilter = [[GPUImageSketchFilter alloc] init];
            [self  filterthevideo:sketchFilter];
        }
            break;
        case 6:
        {
             GPUImageColorInvertFilter *colorFilter = [[GPUImageColorInvertFilter alloc] init];
            

            [self  filterthevideo:colorFilter];
        }
            
            
            break;
        case 7:
        {
            GPUImagePixellateFilter *pixelet = [[GPUImagePixellateFilter alloc] init];
            [pixelet setFractionalWidthOfAPixel:0.01];
            [self  filterthevideo:pixelet];
        }
            break;
        default:
        {
            GPUImageFilter *noneFilter = [[GPUImageFilter alloc] init];
            [self  filterthevideo:noneFilter];
        }
            break;
    }
   
    
    
    
   
}

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextButtonPressed:(id)sender
{
     UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
     InviteFriendsCtrl *inviteFriendVC = [aStoryboard instantiateViewControllerWithIdentifier:@"InviteFriends"];
    inviteFriendVC.isComingFrom = @"editPhoto";
    inviteFriendVC.fileType = @".mov";
    inviteFriendVC.videoURL = movieURL;
    inviteFriendVC.imgThumnail = [_filtersImageArray objectAtIndex:selectedFilterIndex];
    [self.navigationController pushViewController:inviteFriendVC animated:YES];
}
- (void)retrievingProgress
{
    
    [_progressBarFlatRainbow setProgress:(float)(movieFile.progress) animated:YES];
    
}

- (void)viewDidUnload
{
    [self setProgressLabel:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)updatePixelWidth:(id)sender
{
    //   [(GPUImageUnsharpMaskFilter *)filter setIntensity:[(UISlider *)sender value]];
    // [(GPUImagePixellateFilter *)filter setFractionalWidthOfAPixel:[(UISlider *)sender value]];
   
}


- (void)initFlatRainbowProgressBar
{
    NSArray *tintColors = @[[UIColor colorWithRed:33/255.0f green: 180/255.0f  blue: 162/255.0f  alpha: 1.0f],[UIColor colorWithRed:33/255.0f green: 180/255.0f  blue: 162/255.0f alpha:1.0f],
    [UIColor colorWithRed:91/255.0f green:63/255.0f blue:150/255.0f alpha:1.0f],
    [UIColor colorWithRed:87/255.0f green:26/255.0f blue:70/255.0f alpha:1.0f],
    [UIColor colorWithRed:126/255.0f green:26/255.0f blue:36/255.0f alpha:1.0f],
    [UIColor colorWithRed:149/255.0f green:37/255.0f blue:36/255.0f alpha:1.0f],
    [UIColor colorWithRed:228/255.0f green:69/255.0f blue:39/255.0f alpha:1.0f],
    [UIColor colorWithRed:245/255.0f green:166/255.0f blue:35/255.0f alpha:1.0f],
    [UIColor colorWithRed:165/255.0f green:202/255.0f blue:60/255.0f alpha:1.0f],
    [UIColor colorWithRed:202/255.0f green:217/255.0f blue:54/255.0f alpha:1.0f],
    [UIColor colorWithRed:111/255.0f green:188/255.0f blue:84/255.0f alpha:1.0f]];
    
    _progressBarFlatRainbow.type               = YLProgressBarTypeFlat;
    _progressBarFlatRainbow.progressTintColors = tintColors;
    _progressBarFlatRainbow.hideStripes        = YES;
    _progressBarFlatRainbow.hideTrack          = YES;
    _progressBarFlatRainbow.behavior           = YLProgressBarBehaviorDefault;
    _progressBarFlatRainbow.cornerRadius = 2.0;
}

@end
