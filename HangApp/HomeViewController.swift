//
//  HomeViewController.swift
//  SwiftSlideShow
//
//  Created by Evan Luthra on 24/02/17.
//  Copyright © 2017 Medigarage Studios LTD. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import CoreLocation
class HomeViewController: UIViewController,UIScrollViewDelegate , UICollectionViewDelegate,UICollectionViewDataSource,AppServiceDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate
 {
    let ser:AppService = AppService()
    var hangUser = UserContactInfo()
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var collectionView:UICollectionView!
     @IBOutlet var startButton: UIButton!
     var bannersImagesArray:NSMutableArray = []
    var arraySuggestionFriends:NSMutableArray = NSMutableArray()
    var bannerImg = BannerImages()
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
           UIApplication.shared.statusBarStyle = .lightContent
           self.navigationController?.setNavigationBarHidden(true, animated: true)
      
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        getbanners()
    }
    
    
    @objc(alertView:clickedButtonAtIndex:) func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0 {
            if let url = URL(string: "App-Prefs:root=LOCATION_SERVICES") {
                UIApplication.shared.open(url, completionHandler: .none)
            }
        }
        else if buttonIndex == 1 {
           
        }
        
    }
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            if(apiName == "getBannerImages")
            {
                if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied)
                {
                    AppDelegate.getAppDelegate().startUpdatinLocation()
                }
                else{
                    let alertView = UIAlertView(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", delegate: self, cancelButtonTitle: "Settings", otherButtonTitles: "Cancel")
                    alertView.delegate = self
                    alertView.show()
                    return
                }
                
                bannersImagesArray  = (response as! HangUsersParserInfo).hangUsers
                AppDelegate.getAppDelegate().bannerImagesArray = bannersImagesArray
                 self.addBanner()
                collectionView.reloadData()
                                
            }
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    func getbanners()
    {
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.getBannerImages()
        
        
    }
     func addBanner()
     {
        
        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
        let scrollViewWidth: CGFloat = self.scrollView.frame.width
        let scrollViewHeight: CGFloat = self.scrollView.frame.height
        pageControl.numberOfPages = bannersImagesArray.count
        textView.textAlignment = .center
        textView.isEditable = false
        textView.textColor = .white
          DispatchQueue.main.async {
        for var i in (0..<self.bannersImagesArray.count)
        {
            
            let imgBanner = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(i), y: 0, width: scrollViewWidth, height: scrollViewHeight))
            imgBanner.contentMode = UIViewContentMode.scaleToFill
            self.bannerImg  = self.bannersImagesArray[i] as! BannerImages
            let image = UIImage(named: "heng")
            imgBanner.sd_setImage(with: NSURL(string: self.bannerImg.imageUrl)! as URL, placeholderImage: image)
            
            
            self.scrollView.addSubview(imgBanner)
            
        }
        }
        if bannersImagesArray.count>0 {
            textView.text = (bannersImagesArray[0] as!BannerImages).title
            
        }
        
        
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(bannersImagesArray.count) , height: self.scrollView.frame.height-50)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        
        if scrollView == self.scrollView
            {
                let pageWidth: CGFloat = scrollView.frame.width
                let currentPage:CGFloat = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
                
                self.pageControl.currentPage = Int(currentPage)
                bannerImg  = bannersImagesArray[Int(currentPage)] as! BannerImages
                textView.text = bannerImg.title
        }
       
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func moveToNextPage (){
        
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
    }
    
    
    
    //MARK:collection view delegate
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return bannersImagesArray.count;
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        
         bannerImg  = bannersImagesArray[indexPath.row] as! BannerImages
        let image = UIImage(named: "heng")
       
        cell.categoryImageView.sd_setImage(with: NSURL(string: self.bannerImg.imageUrl)! as URL, placeholderImage: image)
        cell.categoryLabel.text = bannerImg.title
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width:UIScreen.main.bounds.size.width, height:120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 1.5;
    }
    
    
    
    func selectItemAtIndexPath(indexPath: NSIndexPath?, animated: Bool, scrollPosition: UICollectionViewScrollPosition)
    {
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let feedsVC = mainStoryboard.instantiateViewController(withIdentifier: "feeds") as! FeedController
        feedsVC.selectedPageIndex = indexPath.row
          bannerImg  = bannersImagesArray[indexPath.row] as! BannerImages
        AppDelegate.getAppDelegate().categoryID = bannerImg.bannerId
        feedsVC.categoryNameArray = bannersImagesArray
        self.navigationController?.pushViewController(feedsVC, animated: true)
        
    }
    
    


}
