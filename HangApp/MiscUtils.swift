//
//  MiscUtils.swift
//  BluLint
//
//  Created by Akhilesh on 3/7/16.
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import Foundation
import AddressBookUI
import Contacts
import CoreLocation
import EventKit
import AVFoundation
import AVKit
class MiscUtils{
    
    static let sharedMiscUtils = MiscUtils()

    
    func getStringResource(key : String) -> String
    {
        return NSLocalizedString(key, comment: "")
    }
    
    func getCurrentTimeInFormatyyyymmdd() -> String
    {
        let currentDateTime = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let stringDate = dateFormatter .string(from: currentDateTime as Date)
        var a:String = stringDate .components(separatedBy: " ")[0]
        
        
        a = a.replacingOccurrences(of: "/", with: "-")
        return a
    }
    
    func getDayFromToday() -> String
    {
        let stringDate = getCurrentTimeInFormatyyyymmdd()
         let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy/MM/dd"
        let date = dateFormater.date(from: stringDate)
        dateFormater.dateFormat = "EEEEEEE"
        let day = dateFormater.string(from: date!)
        print(day)
        return day
    }
    
    
    
    func getDurationFromCurrentDate(dateString:String) -> String
    {
         var duration:String!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: dateString)
        let currentTime = Date()
        
        let elapsed = currentTime.timeIntervalSince(date!)
        let durationSecond = Int(elapsed)
        let (d , h, m, s) = secondsToHoursMinutesSeconds (seconds: durationSecond)
        //print ("\(d) Days, \(h) Hours, \(m) Minutes, \(s) Seconds")
        
        if d != 0
        {
          duration = String(format:"%d days ago",d)
            return duration
        }
        else if h != 0
        {
        
            duration = String(format:"%d hours ago",h)
            return duration
        }
        else if m != 0
        {
            
            duration = String(format:"%d minutes ago",m)
            return duration
        }
        else
        {
            duration = String(format:"%d seconds ago",s)
            return duration
        
        }

       
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int,Int) {
        return (seconds / (3600 * 24),seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func getDayFromDate(dateString:String) -> String
    {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let date = dateFormater.date(from: dateString)
        dateFormater.dateFormat = "EEEEEEE"
        let day = dateFormater.string(from: date!)
        print(day)
        return day
    }
    
    func getDateDateAndMonth (dateStr:String) -> String
    {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy/MM/dd"
        let dateInDate = dateFormater.date(from: dateStr)
        dateFormater.dateFormat = "d MMM"
         let strdate = dateFormater.string(from: dateInDate!)
        print(strdate)
        return strdate
    }
    
    func getCurrentDateandTimeForDatePicker () -> (String)
    {
        
        let currentDateTime = NSDate()
        let dateFormatter = DateFormatter()
        //dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let stringDate = dateFormatter .string(from: currentDateTime as Date)
        //dateFormatter.timeStyle = .full
        //dateFormatter.dateFormat = "hh:mm a"
        //let stringTime = dateFormatter .string(from: currentDateTime as Date)

       // print(stringDat)
       
        return stringDate
    }
    
    func getCurrentTime() -> String
    {
        let currentDateTime = NSDate()
        let datePlus60Minutes:NSDate = currentDateTime.addingTimeInterval(60*60)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        dateFormatter.dateFormat = "yyyy/MM/dd hh:mm a"
        let stringDate = dateFormatter .string(from: currentDateTime as Date)
        let time:String = stringDate .components(separatedBy: " ")[1]
        let amPm:String = stringDate .components(separatedBy: " ")[2]
       
        return String(format: "%@ %@", time,amPm)
     }
    
    func getCurrentTimebyAddingOnehour() -> String{
        let calender = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))
         let dateComponents = NSDateComponents()
        dateComponents.hour = 1
        var currentDateTime = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        dateFormatter.dateFormat = "yyyy/MM/dd hh:mm a"
        
        currentDateTime = (calender?.date(byAdding: dateComponents as DateComponents, to: currentDateTime as Date, options: .matchFirst))! as NSDate
        let stringDate = dateFormatter .string(from: currentDateTime as Date)
        let date:String = stringDate .components(separatedBy:" ")[0]
        let time:String = stringDate .components(separatedBy:" ")[1]
        let amPm:String = stringDate .components(separatedBy:" ")[2]
        
        /*
         NSDate *currentDate = [NSDate date];
         NSDate *datePlus60Minute = [currentDate dateByAddingTimeInterval:60];
         */
        return stringDate//String(format: "%@ %@ %@" ,date,time,amPm)
        /*
         NSDate *today=[NSDate date];
         NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
         NSDateComponents *components=[[NSDateComponents alloc] init];
         components.day=1;
         NSDate *targetDate =[calendar dateByAddingComponents:components toDate:today options: 0];
         */
    }
    
    func isTimeSmallerThenCurrentTime(timeStr : String,currentTimeStr:String) -> Bool{
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateFormat = "hh:mm a"
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone=timeZone as TimeZone!
        let outputA = dateFormatter.date(from: timeStr)
        let outputB = dateFormatter.date(from: currentTimeStr)
        if outputA?.compare(outputB!) == ComparisonResult.orderedAscending {
            return true
        }else{
            return false
        }
    }
    
    
    func getCurrentTimeInMS() -> TimeInterval
    {
        let currentDateTime = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy/MM/dd hh:mm a"
        let stringDate = dateFormatter .string(from: currentDateTime as Date)
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        let date = dateFormatter .date(from: stringDate)
        return (NSDate().timeIntervalSince1970)    }
    
    
    func checkContactsStatus() -> Bool
    {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        let status:Bool!
        switch authorizationStatus {
        case .authorized:
            status = true
        case .denied, .notDetermined:
            status = false
        default:
            status = false
        }
        return status
    }
    
    func checkLocationAuthorizationStatus() -> Bool
    {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied)
        {
          return false
        }
        else
        {
            return true
        }
        
    }
    
    func showWarningAlertWithMessage(messageString:String,  viewcontroller:UIViewController)
    {
        let alert = UIAlertController(title: "", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        viewcontroller.present(alert, animated: true, completion: nil)
    }
    
    func showWarningAlertWithTitleAndMessage(title:String, messageString:String, viewcontroller:UIViewController)
    {
        let alert = UIAlertController(title: title, message: messageString, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        viewcontroller.present(alert, animated: true, completion: nil)
    }
    
    func failToReceiveResponce(messagestring:String, viewcontroller:UIViewController)
    {
        Loader.sharedLoader.hideLoader()
        let alert = UIAlertController(title: "Error", message: messagestring, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        viewcontroller.present(alert, animated: true, completion: nil)
    }
    
    
    
    static func addEventToCalendar(title: String, description: String?, startDate: NSDate, endDate: NSDate, eventId:String,completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        eventStore.eventStoreIdentifier
//        eventStore.eventWithIdentifier(eventId)
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title =  title//"Vadé"
                event.startDate = startDate as Date
                event.endDate = endDate as Date
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
//                let oneHourAlarm = EKAlarm.alarmWithRelativeOffset(-3600)
                
                
                let halfHourAlarm:EKAlarm = EKAlarm(relativeOffset: -1800)
                let sevenDayAlarm:EKAlarm = EKAlarm(relativeOffset: -604800)
                let oneDayAlarm:EKAlarm = EKAlarm(relativeOffset: -86400)
//                EKAlarm(relativeOffset:)
                event.alarms = [halfHourAlarm, oneDayAlarm, sevenDayAlarm]

                event
                do {
                    try eventStore.save(event, span: .thisEvent)
                    UserDefaults.standard.set(event.eventIdentifier, forKey: eventId)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    static func removeEventFromCalendar( eventId:String,completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
//        eventStore.eventWithIdentifier(eventId)
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                if let eventIdStr = UserDefaults.standard.object(forKey: eventId){
                    let event = eventStore.event(withIdentifier: eventIdStr as! String)
                    event
                    do {
                        try eventStore.remove(event!, span: .thisEvent)
                        //                    try eventStore.saveEvent(event!, span: .ThisEvent)
                    } catch let e as NSError {
                        completion?(false, e)
                        return
                    }
                    completion?(true, nil)
                } else {
                    completion?(false, error as NSError?)
                }
            }
   
        })
    }
    
    static func removeArrayOfEvents(eventIds:Array<String>){
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                for eventId in eventIds{
                        let event = eventStore.event(withIdentifier: eventId )
                    if event != nil{
                        do {
                            try eventStore.remove(event!, span: .thisEvent)
                            print("Success")
                            //                    try eventStore.saveEvent(event!, span: .ThisEvent)
                        } catch let e as NSError {
                            print("error:%@",e)
                            return
                        }
                    }
                    
                }
            }
            
        })

    }
    
    func convertDatenTimetoTimestamp(strDate:String) -> Double{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd hh:mm a"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        return (dateFormatter.date(from: strDate)?.timeIntervalSince1970)! * 1000
    }
    
    func convertTimeStringtoLocalTimeString(timeUTCStr:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        let date = dateFormatter.date(from: timeUTCStr)
        print(date)
        dateFormatter.timeZone = NSTimeZone.local//NSTimeZone(forSecondsFromGMT: 0)
        return dateFormatter.string(from: date!)
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func openAppUrl(urlString: String, iTuneUrlString: String)
    {
         let appURL = NSURL(string: urlString)!
        
        if UIApplication.shared.canOpenURL(appURL as URL)
        {
            if #available(iOS 10, *) {
                UIApplication.shared.open(appURL as URL, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(appURL): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(appURL as URL)
                print("Open \(appURL): \(success)")
            }
            
 
        }
        else {
            
            UIApplication.shared.openURL(NSURL( string: iTuneUrlString)! as URL)
        }
        
        }
   
     func thumbnailForVideoAtURL(urlVideo: URL) -> UIImage? {
        
       //et url = NSURL(string: urlStr)
        let asset = AVAsset(url: urlVideo )
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
}
