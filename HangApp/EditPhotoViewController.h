//
//  EditPhotoViewController.h
//  CLImageEditorDemo
//
//  Created by sho yakushiji on 2013/11/14.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol EditPhotoViewControllerDelegate
-(void)setImage:(UIImage *)image;
@end
@interface EditPhotoViewController : UIViewController
<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITabBarDelegate, UIActionSheetDelegate, UIScrollViewDelegate>
{
   
     IBOutlet __weak UIScrollView *_scrollView;
     IBOutlet __weak UIImageView *_imageView;
     IBOutlet __weak UIButton *btnInvite;
     IBOutlet __weak UIButton *btnSave;
     IBOutlet __weak UIButton *btnShare;
    IBOutlet __weak UIButton *btnDone;
}
 @property (nonatomic,retain) id delegate;
@property (nonatomic, strong) NSString*comingfrom;
     @property(strong,nonatomic) NSData *data;
@end
