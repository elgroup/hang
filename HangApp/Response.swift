//
//  Response.swift
//  BluLint
//
//  Created by hament miglani on 21/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class Response: NSObject
{
    var statusCode:Bool?
    var statusDescription:String?
    var groupCreationTime:Double = 0.0
    var pendingInvitationsCount:Int? = 0
    var momentLikeCount:Int? = 0


}
