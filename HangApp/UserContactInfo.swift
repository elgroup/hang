//
//  ReefUserContactInfo.swift
//  BluLint
//
//  Created by hament miglani on 25/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class UserContactInfo: Response
{
    var followed_id = Int()
     var isFriend = Bool()
     var followed_users_status = Bool()
    var fullName = String()
    var emailId = String()
    var imageUrl = String()
    var userId = Int()
    var userName = String()
     var authToken = String()
    var bioGraphy = String()
    var location = String()
     var dob = String()
    var badgeNumber:Int!
     var momentsArray = NSMutableArray()
    var reviewArray = NSMutableArray()
    var checkInArray = NSMutableArray()
    var messagesArray:NSMutableArray = []
    var commentsArray:NSMutableArray = []
     var friendsArray:NSMutableArray = []

    var accountId = 0
    var userType:String!
    
    
    
}
