//
//  TutorialViewCtrl.swift
//  HangApp
//
//  Created by Sunil on 14/08/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class TutorialViewCtrl: UIViewController ,UIScrollViewDelegate{
    

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    var  imageList :NSArray!
    var  messageList :NSArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.alwaysBounceHorizontal = true
        
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate =  self
        scrollView.backgroundColor = UIColor.clear
        imageList = ["SignUpBg","BackgroundImage"]
        messageList = ["Night Life at your finger tips","Discover local events"]
        btnSignIn.layer.borderColor = UIColor.white.cgColor
        btnSignIn.layer.borderWidth = 2.0
        self.view.backgroundColor = UIColor.white
        var index: Int
        index = 0
      
     
        for  imageName  in imageList {
            
            print(imageName)
            
            var frame = CGRect()
            frame.origin.x = self.view.frame.size.width * CGFloat(index)
            frame.origin.y =  0
            frame.size = self.view.frame.size
            
            var imageView: UIImageView!
            imageView = UIImageView()
            imageView.frame = frame
            imageView.contentMode = UIViewContentMode.scaleAspectFit;
            imageView.clipsToBounds = false
            imageView.image = UIImage (named: imageName as! String)
            scrollView.addSubview(imageView)
            index += 1
        }
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width * CGFloat( imageList.count), height: self.view.frame.size.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
   
    }

    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width;
        let page: Int  = Int(floor((scrollView.contentOffset.x - pageWidth / CGFloat(2)) / pageWidth) + CGFloat(1));
        let index :Int = page == 1 ? 0 : 1
        lblMsg.text = messageList.object(at: index) as! String
        
        
    }
    
}

