//
//  Comment.swift
//  HangApp
//
//  Created by Evan Luthra on 23/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class Comment: NSObject {
    var imageUrl:String!
    var commentDate:String!
    var name:String!
    var commentDesc:String!
    var commentId = Int()
    var moment_id = Int()
     var user_id = Int()

}
