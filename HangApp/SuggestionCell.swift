//
//  SuggestionCell.swift
//  HangApp
//
//  Created by Evan Luthra on 14/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell {
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var btnStatus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    @IBAction func statusBtnClicked(_ sender: Any)
//    {
//        if btnStatus.isSelected
//        {
//          btnStatus.isSelected = false
//        }
//        else
//        {
//         btnStatus.isSelected = true
//            btnStatus.setTitle("", for: .selected)
//        
//        }
//        
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
