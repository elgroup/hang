//
//  MomentsInfo.swift
//  HangApp
//
//  Created by Evan Luthra on 06/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class MomentsInfo: Response
{
    var videodata:Data!
    var imagedata:Data!
    var loc_lat:Double!
    var loc_long:Double!
    var caption:String!
    var momentDate:String!
    var momentPostedByUser:String!
    var momentPostedByUserImage:String!
    var momentPostedByUserId = Int()
    var momentDistance:Double!
    var address:String!
    var isLike:Bool!

    var categoryId:Int!
    var viewsCount:Int!
    var tag_Users:NSMutableArray!
     var commentsArray:NSMutableArray!
    var fileType:String!
     var momentsUrl:String!
     var momentsVideoUrl:String!
    var momentsVideoThumbUrl:String!
    var momentId = Int()
     var likeCount = Int()
     var commentCount = Int()

}
