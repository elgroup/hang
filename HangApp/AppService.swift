
//  AppService.swift

import UIKit
import Alamofire

import CoreLocation


let deviceType:Int = 121

extension String {
   
    func toBase64()->String{
        
        let data = self.data(using: String.Encoding.utf8)
       return (data?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!

    }
}

@objc protocol AppServiceDelegate
{
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    func didFailToReceiveResponse(response:Response)
    @objc optional func didFailToReceiveResponseandAPIName(response:Response, apiName:String)
    
}

class AppService: NSObject
{
    static let sharedInstance = AppService()
    var delegate:AppServiceDelegate?
    let configuration = URLSessionConfiguration.default

    func getLocalUrl() -> String
    {

      return kBaseURL
    }
   
    func getSessionConfigration() -> URLSessionConfiguration
    {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 1
        return configuration
    }

    
func loginUserWithInfo(userInfo : UserInfo)
    {
        
        let devieToken  = UserDefaults.standard.value(forKey: "devieToken") as AnyObject as? String != nil ? UserDefaults.standard.value(forKey: "devieToken") as AnyObject as! String : "123456"
        
        
        let parameters:[String: AnyObject] = [
            "login": userInfo.emailId,
            "password":userInfo.password,
            "devise_token": devieToken as AnyObject,
            "devise_type": "ios" as AnyObject
            
            ]
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/users/sign_in.json" )
            
            Alamofire.request(str, method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: nil)
                .responseJSON{ (response:DataResponse<Any>)  in
                    
                    
                    
                                        if response.result.error == nil
                                        {
                                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                                            print(s)
                                            let appParser:AppParser = AppParser()
                                            let userParsedInfo:UserParsedInfo = appParser.perseLoginResponse(s: s) as! UserParsedInfo
                                            if userParsedInfo.statusCode == true
                                            {
                                        self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "sign_in")                                             }
                                            else
                                            {
                                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                                            }
                    
                                        }
                                        }
                                        else
                                        {
                                            self.serviceFail(error: response.result.error! as NSError,response: response.response)
                                        }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    
    func resetPasswordWithInfo(parameters : [String: AnyObject])
    {
       
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/users/update_password" )
            
            Alamofire.request(str, method: .put, parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON{ (response:DataResponse<Any>)  in
                    
                    
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserParsedInfo = appParser.perseLoginResponse(s: s) as! UserParsedInfo
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "resetPassword")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    func searchUsers(searchString : String)
    {
        
        let parameters:[String: AnyObject] = [
            "query":searchString as AnyObject
            ]
        print(parameters)
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/search" )
            
            Alamofire.request(str, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON{ (response:DataResponse<Any>)  in
                    
                    
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:HangUsersParserInfo = appParser.parseSearchContactInfo(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "searchUsers")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    
    func getFriendsList()
    {
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            let str = String(format:"%@%@", getLocalUrl(), "/follows" )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON{ (response:DataResponse<Any>)  in
                    
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:HangUsersParserInfo = appParser.parseContactInfo(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getFriendsList")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    func getNotifications()
    {
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            let str = String(format:"%@%@", getLocalUrl(), "/notifications" )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON{ (response:DataResponse<Any>)  in
                    
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let notification:Notifications = appParser.parseNotifications(responseString: s)
                            if notification.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: notification, apiName: "getNotifications")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: notification)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    
    
    func registerUserWithInfo(userInfo : UserInfo)
    {
        
        let devieToken  = UserDefaults.standard.value(forKey: "devieToken") as AnyObject as? String != nil ? UserDefaults.standard.value(forKey: "devieToken") as AnyObject as! String : "123456"
        
        let parameters:[String: AnyObject] = [
            "email": userInfo.emailId,
            "username": userInfo.userName,
            "phone": userInfo.mobileNumber,
            "password":userInfo.password,
            "devise_token":devieToken as AnyObject,
            "user_type": userInfo.userType,
            "devise_type": "ios" as AnyObject
            ]
        
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/users" )
            
            Alamofire.request(str, method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: nil)
                .validate()
                .responseData{response in
                    
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserParsedInfo = appParser.perseLoginResponse(s: s) as! UserParsedInfo
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "sign_up")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    

    
    
    func validateEmail(email:String){
        let parameters:[String: AnyObject] = [
            "email": email as AnyObject,
                ]
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        
        if(Reachability.isConnectedToNetwork())
        {
            Alamofire.request(String(format: "%@%@",getLocalUrl(),"/users/email_check"), method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON
            { response in
                
                
                if response.result.error == nil
                {
                    if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                        print(s)
                        let appParser:AppParser = AppParser()
                        let userParsedInfo:UserParsedInfo = appParser.perseResponse(s: s) as! UserParsedInfo
                        if userParsedInfo.statusCode == true
                        {
                            self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "validateEmail")                                             }
                        else
                        {
                            self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                        }
                        
                    }
                }
                else
                {
                    self.serviceFail(error: response.result.error! as NSError,response: response.response)
                }
        }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func validateUseName(usename:String){
        let parameters:[String: AnyObject] = [
            "username": usename as AnyObject,
            ]
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        
        if(Reachability.isConnectedToNetwork())
        {
            Alamofire.request(String(format: "%@%@",getLocalUrl(),"/users/name_check"), method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: nil)

                .responseJSON
                { response in
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserParsedInfo = appParser.perseResponse(s: s) as! UserParsedInfo
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "validateUseName")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    
    func getSuggestionList(emails:[String])
    {
        let parameters:[String: AnyObject] = [
            "email": emails as AnyObject,
            ]
        
       
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
//    let headers = [ "X-User-Email": UserDefaults.standard.value(forKey: kemailIdKey),
//                        "X-User-Token":UserDefaults.standard.value(forKey: kauthKey)
//        ]
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/users/all_user" )
            
            Alamofire.request(str, method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                   
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:HangUsersParserInfo = appParser.parseContactInfo(responseString: s) 
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getSuggestionList")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    
    
    func getProfileData(id:String)
    {
        
        
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
      
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@%@", getLocalUrl(), "/users/",id )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parseProfileInfo(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getProfileData")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    func getMomentDetail(id:Int)
    {
        
        
                let parameters:[String: AnyObject] = [
                    "lat2": (AppDelegate.getAppDelegate().coordinates?.latitude)! as Double as AnyObject,
                     "lon2": (AppDelegate.getAppDelegate().coordinates?.longitude)! as Double as AnyObject,
                      "id": String(id) as AnyObject
                    ]
                let dictRequest :[String: AnyObject]  = ["moment" : parameters as AnyObject ]
              
                
                let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
                
                
                let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
                
                if(Reachability.isConnectedToNetwork())
                {
                    
                    
                    let str = String(format:"%@%@", self.getLocalUrl(), "/moments/nearproperty")
                    
                    Alamofire.request(str, method: .post,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                        .responseJSON { response in
                            
                            if response.result.error == nil
                            {
                                if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                                    print(s)
                                    let appParser:AppParser = AppParser()
                                    let userParsedInfo:MomentsInfo = appParser.parseMomentDetail(responseString: s)
                                    if userParsedInfo.statusCode == true
                                    {
                                        self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getMomentDetail")                                             }
                                    else
                                    {
                                        self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                                    }
                                    
                                }
                            }
                            else
                            {
                                self.serviceFail(error: response.result.error! as NSError,response: response.response)
                            }
                    }
                }
                else
                {
                    GlobalInfo.netWorkNotAvailable()
                }
                
                
                
    
        
        
       
        
    }
    

    
    func getMomentDataById(id:String)
    {
        let parameters:[String: AnyObject] = [
            "banner_id": id as AnyObject,
            "latitude": (AppDelegate.getAppDelegate().coordinates?.latitude)! as Double as AnyObject,
            "longitude": (AppDelegate.getAppDelegate().coordinates?.longitude)! as Double as AnyObject
            ]
         let dictRequest :[String: AnyObject]  = ["moment" : parameters as AnyObject ]
        print(dictRequest)
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@%@%", getLocalUrl(), "/moments/all_moments")
            
            Alamofire.request(str, method: .post,  parameters: dictRequest,encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parseMomentData(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getMomentDataById")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    
    func getCheckInDataById(id:String)
    {
        let parameters:[String: AnyObject] = [
            "user_id": id as AnyObject,
            "latitude": (AppDelegate.getAppDelegate().coordinates?.latitude)! as Double as AnyObject,
            "longitude": (AppDelegate.getAppDelegate().coordinates?.longitude)! as Double as AnyObject
        ]
        let dictRequest :[String: AnyObject]  = ["place" : parameters as AnyObject ]
        print(dictRequest)
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@%@%", getLocalUrl(), "/places/nearplaces")
            
            Alamofire.request(str, method: .post,  parameters: dictRequest,encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parseCheckInData(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getCheckInDataById")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    
    
    
    func updateProfileData(userInfo : UserInfo)
    {
        
        let parameters:[String: AnyObject] = [
            "image": userInfo.imageUrl,
            "name": userInfo.fullName,
            "location": userInfo.location,
            "biography": userInfo.bio,
            "dob": userInfo.dob,
            ]
        
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@%d", getLocalUrl(), "/users/",userInfo.userId )
            
            Alamofire.request(str, method: .put,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "updateProfileData")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func createReview(review : Review)
    {
        
        let parameters:[String: AnyObject] = [
            "rating": review.ratingCount as AnyObject,
            "review_user_id": review.user_id as AnyObject,
            "description": review.reviewDesc as AnyObject
            ]
        
        let dictRequest :[String: AnyObject]  = ["review" : parameters as AnyObject ]
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/reviews" )
            
            Alamofire.request(str, method: .post,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "createReview")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    
    
    func sendLikeOnMoment(momentId : Int)
    {
        
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@/moments/%d%@", getLocalUrl(),momentId, "/like" )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseLikeResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "sendLikeOnMoment")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    
    func postCommentMoment(comment : Comment)
    {
        
        let parameters:[String: AnyObject] = [
            
            "moment_id": comment.moment_id as AnyObject,
            "body": comment.commentDesc as AnyObject
        ]
        
        let dictRequest :[String: AnyObject]  = ["comment" : parameters as AnyObject ]
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/comments" )
            
            Alamofire.request(str, method: .post,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "postComment")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }

    
    
    func postCommentPlace(comment : Comment , apiName : String)
    {
        
         

        
        let parameters:[String: AnyObject] = [
           
            "place_id": comment.moment_id as AnyObject,
             "place_user_id": comment.user_id as AnyObject,
            "body": comment.commentDesc as AnyObject
        ]
        
        let dictRequest :[String: AnyObject]  = ["comment_place" : parameters as AnyObject ]
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), apiName )
            
            Alamofire.request(str, method: .post,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "postComment")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func sendMessage(message : Messages)
    {
        
        let parameters:[String: AnyObject] = [
            "body": message.body as AnyObject,
            "sender_id": message.user_id as AnyObject
        ]
        
        let dictRequest :[String: AnyObject]  = ["message" : parameters as AnyObject ]
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/messages" )
            
            Alamofire.request(str, method: .post,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "sendMessage")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    

    func getReviewsById(id:String)
    {
        
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@/users/%@%@", getLocalUrl(), id,"/all_review")
            
            Alamofire.request(str, method: .get,encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parseReview(responseString:s )
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getReviewsById")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func getCommentsById(id:String)
    {
        
      
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@/moments/%@%@", getLocalUrl(), id,"/all_comments")
            
            Alamofire.request(str, method: .get,encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parseComments(responseString:s )
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getCommentsById")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func logOut()
    {
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@%@", getLocalUrl(), "/users/sign_out")
            
            Alamofire.request(str, method: .delete,encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "logOut")
                            }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    
    func getMessageById(id:String)
    {
      
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@%@", getLocalUrl(), "/messages")
            
            Alamofire.request(str, method: .get,encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parseMessage(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getMessageById")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    func postYourStories(storyImage : UIImage)
    {
        
        let parameters:[String: AnyObject] = [
            "image": String(format:"%@%@","data:image/png;base64,",GlobalInfo.convertImageToBase64(image: storyImage) ) as AnyObject,
            
            ]
        
        let dictRequest :[String: AnyObject]  = ["yourstory" : parameters as AnyObject ]
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/yourstories" )
            
            Alamofire.request(str, method: .post,parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:Response = appParser.perseResponse(s: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "postYourStories")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    func getYourStories()
    {
        
       
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/yourstories" )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserContactInfo = appParser.parsYourStories(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getYourStories")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func postCheckinData(checkInfo : CheckinInfo)
    {
        
        let parameters:[String: AnyObject] = [
            
            "place[place_type]" : checkInfo.fileType as AnyObject,
            "place[latitude]" : checkInfo.loc_lat as AnyObject,
            "place[longitude]" : checkInfo.loc_long as AnyObject,
            "place[title]" : checkInfo.address as AnyObject
            
            
        ]
        
        let parametersNew:[String: AnyObject] = [
            "place[place_OtherKew]" : parameters as NSDictionary,
            "place[tag_places_attributes][][palce_user_id]" : checkInfo.tag_Users as AnyObject
            
        ]
        
        
        let strPhotoName = String(format:"Photo%@.png",MiscUtils.sharedMiscUtils.getCurrentTime())
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
            let str = String(format:"%@%@", getLocalUrl(), "/places" )
            let url = NSURL(string: str)
            var r   = URLRequest(url:url as! URL)
            r.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            r.addValue(strAuth, forHTTPHeaderField: "X-User-Token")
            r.addValue(strEmail, forHTTPHeaderField: "X-User-Email")
            
            let imgData = checkInfo.imagedata ?? Data()
            
            
            r.httpBody = createPlacesBody(parameters: parametersNew ,
                                    boundary: boundary,
                                    data: imgData,
                                    mimeType: "image/png",
                                    filename: strPhotoName)
            
            
            
            let session = URLSession.shared
            
            print(r)
            let task = session.dataTask(with: r) {
                (
                data, response, error) in
                
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                
                if let s = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    
                    
                {
                    print(s)
                    let appParser:AppParser = AppParser()
                    let response:Response = appParser.perseResponse(s: s)
                    if response.statusCode == true
                    {
                        self.delegate?.didReceiveResponseWithResponseandAPIName(response: response, apiName:"postCheckinData")                                             }
                    else
                    {
                        self.delegate?.didFailToReceiveResponse(response: response)
                    }
                    
                }
                
            }
            
            task.resume()
            
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    
    func postMomentData(momentInfo : MomentsInfo)
    {
        
        let parameters:[String: AnyObject] = [
           
            "moment[moment_type]" : momentInfo.fileType as AnyObject,
            "moment[caption]" : momentInfo.caption as AnyObject,
            "moment[banner_id]" : momentInfo.categoryId as AnyObject,
            "moment[latitude]" : momentInfo.loc_lat as AnyObject,
            "moment[longitude]" : momentInfo.loc_long as AnyObject
           

            ]
        
        let parametersNew:[String: AnyObject] = [
                       "moment[moment_OtherKew]" : parameters as NSDictionary,
                      "moment[tag_moments_attributes][][moment_user_id]" : momentInfo.tag_Users as AnyObject
    
        ]
        
       
   
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        if(Reachability.isConnectedToNetwork())
        {
             let str = String(format:"%@%@", getLocalUrl(), "/moments" )
             let url = NSURL(string: str)
             var r   = URLRequest(url:url as! URL)
             r.httpMethod = "POST"
             let boundary = "Boundary-\(UUID().uuidString)"
             r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
             r.addValue(strAuth, forHTTPHeaderField: "X-User-Token")
             r.addValue(strEmail, forHTTPHeaderField: "X-User-Email")
            
            var data = Data()
            var mimetype = ""
           var strFileName = ""
            if momentInfo.fileType == ".mov" {
                data = momentInfo.videodata
                mimetype = "video/mov"
            strFileName = String(format:"Video%@.mov",GlobalInfo.sharedInstance.getCurrentTime())
            }
            else
            {
              data = momentInfo.imagedata
              mimetype = "image/png"
            strFileName = String(format:"Photo%@.png",GlobalInfo.sharedInstance.getCurrentTime())
            }
            
        
             r.httpBody = createBody(parameters: parametersNew ,
                                    boundary: boundary,
                                    data: data,
                                    mimeType: mimetype,
                                    filename: strFileName)
            
            
            
            let session = URLSession.shared
            
            print(r)
            let task = session.dataTask(with: r) {
                (
                data, response, error) in
                
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                
                if let s = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
              
                
                {
                    print(s)
                    let appParser:AppParser = AppParser()
                    let response:Response = appParser.perseResponse(s: s)
                    if response.statusCode == true
                    {
                        self.delegate?.didReceiveResponseWithResponseandAPIName(response: response, apiName: "postMoment")                                             }
                    else
                    {
                        self.delegate?.didFailToReceiveResponse(response: response)
                    }
                    
                }
                
            }
            
            task.resume()
     
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
 
    

    func createBody(parameters: [String: AnyObject],
                    boundary: String,
                    data: Data,
                    mimeType: String,
                    filename: String) -> Data
    {
        
        let body = NSMutableData()
        let parameters1 = parameters["moment[moment_OtherKew]"] as! [String: AnyObject]
        
        
        for (key, value) in parameters1 {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        let key = "moment[tag_moments_attributes][][moment_user_id]"
        
        let parameters2  = parameters ["moment[tag_moments_attributes][][moment_user_id]"] as! NSArray
         for value in parameters2
         {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        if (filename.contains(".png"))
        {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"moment[image]\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(data)
            body.appendString("\r\n")
            
        }
        else
        {
        
             body.appendString("--\(boundary)\r\n")
             body.appendString("Content-Disposition: form-data; name=\"moment[video]\"; filename=\"\(filename)\"\r\n")
             body.appendString("Content-Type: \(mimeType)\r\n\r\n")
             body.append(data)
             body.appendString("\r\n")
                
        
            
        }
        
       
        body.appendString("--\(boundary)--\r\n")
        
        
        //print(body)
        return body as Data
    }
    
    
    
    func createPlacesBody(parameters: [String: AnyObject],
                    boundary: String,
                    data: Data?,
                    mimeType: String,
                    filename: String) -> Data
    {
        
        let body = NSMutableData()
        let parameters1 = parameters["place[place_OtherKew]"] as! [String: AnyObject]
        
        
        for (key, value) in parameters1 {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        let key = "place[tag_places_attributes][][palce_user_id]"
        
        let parameters2  = parameters ["place[tag_places_attributes][][palce_user_id]"] as! NSArray
        for value in parameters2
        {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        if ((data?.count)! > 0)
        {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"place[image]\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(data!)
            body.appendString("\r\n")
            
        }
        
       
        body.appendString("--\(boundary)--\r\n")

        
        //print(body)
        return body as Data
    }

    func getBannerImages()
    {
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
      
      
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/banner_uploads" )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:HangUsersParserInfo = appParser.parseBannerImages(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getBannerImages")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            
            GlobalInfo.netWorkNotAvailable()
            
        }
        
    }
    
    
    func followUserWithUserId(id: String)
    {
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        let parameters:[String: AnyObject] = [
            "followed_id": id as AnyObject,
            ]
        
        let dictRequest :[String: AnyObject]  = ["followed_user" : parameters as AnyObject ]
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/followed_users" )
            
            Alamofire.request(str, method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserParsedInfo = appParser.perseResponse(s: s) as! UserParsedInfo
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "followUser")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    
    
    func acceptFollowRequestUserId(id: String)
    {
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
        let parameters:[String: AnyObject] = [
            "id": id as AnyObject,
            ]
        
        let dictRequest :[String: AnyObject]  = ["followed_user" : parameters as AnyObject ]
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/followed_users/add_following" )
            
            Alamofire.request(str, method: .put, parameters: dictRequest, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserParsedInfo = appParser.perseResponse(s: s) as! UserParsedInfo
                            if userParsedInfo.statusCode == true
                            {
                        self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "acceptFollowRequestUserId")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
    }
    func getAllFollowerList()
    {
        
        
        let strEmail = String(format:"%@",UserDefaults.standard.value(forKey: kemailIdKey) as! CVarArg )
        
        
        let strAuth =  String(format:"%@",UserDefaults.standard.value(forKey: kauthKey) as! CVarArg )
        
      
        if(Reachability.isConnectedToNetwork())
        {
            
            
            let str = String(format:"%@%@", getLocalUrl(), "/followed_users" )
            
            Alamofire.request(str, method: .get, encoding: JSONEncoding.default, headers: ["X-User-Email" : strEmail,"X-User-Token" : strAuth])
                .responseJSON { response in
                    
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                             let userParsedInfo:HangUsersParserInfo = appParser.parseFollowerList(responseString: s)
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "getAllFollowerList")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }


    func forgotPassword(email:String){
        let parameters:[String: AnyObject] = [
            "email": email as AnyObject,
            ]
        let dictRequest :[String: AnyObject]  = ["user" : parameters as AnyObject ]
        
        if(Reachability.isConnectedToNetwork())
        {
            Alamofire.request(String(format: "%@%@",getLocalUrl(),"users/password"), method: .post, parameters: dictRequest, encoding: JSONEncoding.default, headers: nil)
                .validate()
                .responseJSON
                { response in
                    if response.result.error == nil
                    {
                        if let s = String(data: response.data!, encoding: String.Encoding.utf8) {
                            print(s)
                            let appParser:AppParser = AppParser()
                            let userParsedInfo:UserParsedInfo = appParser.perseResponse(s: s) as! UserParsedInfo
                            if userParsedInfo.statusCode == true
                            {
                                self.delegate?.didReceiveResponseWithResponseandAPIName(response: userParsedInfo, apiName: "forgotPassword")                                             }
                            else
                            {
                                self.delegate?.didFailToReceiveResponse(response: userParsedInfo)
                            }
                            
                        }
                    }
                    else
                    {
                        self.serviceFail(error: response.result.error! as NSError,response: response.response)
                    }
            }
        }
        else
        {
            GlobalInfo.netWorkNotAvailable()
        }
        
    }
    

    
    

    func serviceFail(error:NSError,response:HTTPURLResponse?)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if(error.code == -1004) || (error.localizedDescription == MiscUtils.sharedMiscUtils.getStringResource(key: "Could not connect to the server"))
        {
            let alert = UIAlertController(title: MiscUtils.sharedMiscUtils.getStringResource(key: "server down"), message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: MiscUtils.sharedMiscUtils.getStringResource(key: "ok"), style: UIAlertActionStyle.cancel, handler: nil))
            Loader.sharedLoader.hideLoader()
            AppDelegate.getAppDelegate().window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        else if (error.code == -1009) || (error.localizedDescription == MiscUtils.sharedMiscUtils.getStringResource(key: "no internet connection"))
        {
            Loader.sharedLoader.hideLoader()
            GlobalInfo.netWorkNotAvailable()
        }
            
        else if (response != nil && response!.statusCode == 401) || error.localizedDescription == MiscUtils.sharedMiscUtils.getStringResource(key: "Operation could not becompleted")
        {
            
            MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: MiscUtils.sharedMiscUtils.getStringResource(key: "logged_in_another_device"), messageString: "", viewcontroller: (AppDelegate.getAppDelegate().window?.rootViewController)!)
            
        }
            
        else if ((response != nil && response!.statusCode == 400) || (error.localizedDescription == MiscUtils.sharedMiscUtils.getStringResource(key: "User not present"))){
            Loader.sharedLoader.hideLoader()
            MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: MiscUtils.sharedMiscUtils.getStringResource(key: "Unauthorise user"), messageString: "", viewcontroller: (AppDelegate.getAppDelegate().window?.rootViewController)!)
        }
            
        else
        {
            let alert = UIAlertView.init(title: error.localizedDescription, message: "", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
            
            alert.show()
            
            
            
          
        }
    }
    

    
    
   
    

}
extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
