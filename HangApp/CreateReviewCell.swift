//
//  CreateReviewCell.swift
//  HangApp
//
//  Created by Evan Luthra on 17/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class CreateReviewCell: UITableViewCell {
     @IBOutlet var txtPostReview: UITextView!
     @IBOutlet var floatRatingView: FloatRatingView!
     @IBOutlet var btnPost:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
