//
//  PopUpViewCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 03/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class PopUpViewCtrl: UIViewController {
     @IBOutlet weak var bgView: UIView!
     var messageReceiver_id = String()
     var messageReceiver_name = String()
     var user_id = Int()
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.clear
        view.isOpaque = false
        bgView.layer.cornerRadius = 6;
        bgView.layer.masksToBounds = true
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func addToContactsBtnClicked(_ sender: Any)
    {
    }
    @IBAction func sendMessageBtnClicked(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let messageVC = mainStoryboard.instantiateViewController(withIdentifier: "message") as! MessageViewCtrl
        messageVC.messageReceiver_id = messageReceiver_id
         messageVC.messageReceiver_name = messageReceiver_name
        self.present(messageVC, animated: true) {
            
        }
    }
    @IBAction func visitProfileBtnClicked(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller : OthersAccountViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "otherAccount") as! OthersAccountViewCtrl
        controller.user_id = Int(messageReceiver_id)!
        controller.comingFrom = "popup"
        self.present(controller, animated: true) {
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
