//
//  UserParsedInfo.swift
//  BluLint
//
//  Created by hament miglani on 21/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import Foundation

class UserParsedInfo:Response
{
    var deviceGcmId:Int!
    var emailId:String!
    var firstName:String!
    var imageUrl:String!
    var lastName:String!
    var mobileNumber:String!
    var password:String!
    var authToken:String!
    var createdDate:String!
    var updatedDate:String!
    var userName:String!
     var userType:String!
    var groupsInfo:NSMutableArray!
    var msgCount:Int!
     var userId:Int!
}
