//
//  EditProfileController.swift
//  HangApp
//
//  Created by Evan Luthra on 22/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class EditProfileController: UIViewController ,UIImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate,UINavigationControllerDelegate,AppServiceDelegate,UIPopoverPresentationControllerDelegate,UITableViewDataSource,UITableViewDelegate{
 var picker = UIImagePickerController()
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet var txtFullName: UITextField!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet weak var biographyView: UIView!
    @IBOutlet var txtBioGraphy: UITextView!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnDOB: UIButton!
    var pickerContainer = UIView()
    var pickerDate = UIDatePicker()
     let ser:AppService = AppService()
     @IBOutlet weak var tblMore: UITableView!
    override func viewDidLoad() {
        callProfileService()
        super.viewDidLoad()
    self.btnDOB.setTitle(MiscUtils.sharedMiscUtils.getCurrentDateandTimeForDatePicker(), for: UIControlState.normal)
        tblMore.layer.cornerRadius = 4;
        tblMore.clipsToBounds = true;
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
        imgProfile.clipsToBounds = true;

    }

    @IBAction func dobButtonClicked(_ sender: UIButton)
    {
        
        configurePicker()
      
    }
    func configurePicker()
    {
        pickerContainer.frame = CGRect(x:0.0,y: self.view.frame.height-250,width: self.view.frame.width,height:250.0)
        pickerContainer.backgroundColor = UIColor.white
        
        pickerDate.frame    = CGRect(x:0.0, y:20.0,width: self.view.frame.width,height: 250.0)
        pickerDate.setDate(NSDate() as Date, animated: true)
        pickerDate.maximumDate = NSDate() as Date
        pickerDate.datePickerMode = UIDatePickerMode.date
        pickerContainer.addSubview(pickerDate)
        
        let doneButton = UIButton()
        doneButton.setTitle("Done", for: UIControlState.normal)
     
        doneButton.setTitleColor(UIColor.blue, for: UIControlState.normal)
        doneButton.addTarget(self, action: #selector(EditProfileController.dismissPicker), for: UIControlEvents.touchUpInside)
        doneButton.frame    = CGRect(x:self.view.frame.width-90, y:5.0, width:70.0, height: 37.0)
        
        pickerContainer.addSubview(doneButton)
        
        self.view.addSubview(pickerContainer)
    }
    @IBAction func changeBtnClicked(_ sender: UIButton)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "resetPassword") as! ResetPasswordVC
        popupVC.modalPresentationStyle = .overCurrentContext
        popupVC.modalTransitionStyle = .crossDissolve
        popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        let pVC = popupVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = sender
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        present(popupVC, animated: true, completion: nil)
        
      
        
    }
    
    
    func dismissPicker ()
    {
        UIView.animate(withDuration: 0.4, animations: {
            
            self.pickerContainer.frame = CGRect(x:0.0, y:self.view.frame.height, width: 0.0,height: 0.0)
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            self.btnDOB.setTitle(dateFormatter.string(from: self.pickerDate.date), for: UIControlState.normal)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backPressed(_ sender: Any)
    {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func morePressed(_ sender: Any)
    {
        if tblMore.isHidden
         {
          tblMore.isHidden = false
         }
        else
        {
            tblMore.isHidden = true
        }
        tblMore.reloadData()
    }
    
    
    @IBAction func openCamera(_ sender: Any)
    {
        let alert:UIAlertController=UIAlertController(title: MiscUtils.sharedMiscUtils.getStringResource(key: "Choose Profile Pic"), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
           
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
           
        }
        
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
   
     func openCamera()
     {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
         
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.camera;
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }
      }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
           
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if picker.sourceType == UIImagePickerControllerSourceType.camera || picker.sourceType == UIImagePickerControllerSourceType.photoLibrary || picker.sourceType == UIImagePickerControllerSourceType.savedPhotosAlbum
        {
            let image = info["UIImagePickerControllerOriginalImage"]
            
            imgProfile.image = image as! UIImage?
            
            
        }
        else
        {
            
        }
        
        picker .dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
        
    }

    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            
            if(apiName == "getProfileData")
            {
                
                let profiledata = response as! UserContactInfo
                txtFullName.text = profiledata.fullName;
                if txtFullName.text == "N/A"
                {
                    txtFullName.text = ""
                }
                let data : NSData =  profiledata.bioGraphy.data(using: String.Encoding.utf8)! as NSData
                let biographyText = String(data: data as Data, encoding: String.Encoding.nonLossyASCII)
                txtBioGraphy.text = biographyText
                if (txtBioGraphy.text != "Your Bio")
                {
                    txtBioGraphy.textColor = UIColor.darkGray
                }
                else if txtBioGraphy.text == "N/A"
                {
                txtBioGraphy.text = ""
                
                }
                txtLocation.text  = profiledata.location
                
                if txtLocation.text == "N/A"
                {
                  txtLocation.text = ""
                }
                self.btnDOB.setTitle(profiledata.dob, for: UIControlState.normal)
               
                if let url  = NSURL(string: profiledata.imageUrl),
                let imageData:NSData = NSData(contentsOf: url as URL)
                    
                {
                    
                    
                    DispatchQueue.main.async
                        {
                            let image = UIImage(data: imageData as Data)
                            self.imgProfile.image = image
                            
                        }
                }


                
            }
            if(apiName == "updateProfileData")
            {
                let alert:UIAlertController=UIAlertController(title: response.statusDescription, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                   self.backPressed(self)
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            
            if(apiName == "getYourStories")
            {
                let alert:UIAlertController=UIAlertController(title: response.statusDescription, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.backPressed(self)
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            
            
            if(apiName == "logOut")
            {
                tblMore.isHidden = true
                if let bundle = Bundle.main.bundleIdentifier {
                    UserDefaults.standard.removePersistentDomain(forName: bundle)
                }
                let alert:UIAlertController=UIAlertController(title: response.statusDescription, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInCtrl = mainStoryboard.instantiateViewController(withIdentifier: "signInCtrl") as! ViewController
                    let navigationCtrl = UINavigationController.init(rootViewController: signInCtrl)
                    
                    AppDelegate.getAppDelegate().window?.rootViewController = navigationCtrl
                    
                    GlobalInfo.sharedInstance.getDeviceToken()
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            }
        }

   
    
    func textFieldShouldReturn(_ userText: UITextField) -> Bool
    {
        switch userText
        {
        case txtFullName:
            txtLocation.becomeFirstResponder()
            break
            
            
        case txtLocation:
            txtBioGraphy.becomeFirstResponder()
        default:
            break
        }
        userText.resignFirstResponder()
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        //         lblError.isHidden = true
        
        
       
    }
    
    func isEmpty(text:String) -> Bool{
        if text.isEmpty == true{
            return true
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
     
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
    
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (text == "Your Bio") {
            textView.text  = ""
            textView.textColor = UIColor.darkGray
            
            return false
        }
       return true
    }
     func textViewDidChange(_ textView: UITextView)
     {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame;
        biographyView.frame.size.height  = newSize.height
    }
    
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
        tblMore.isHidden = true
    }
    

    func callProfileService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        
        let strUserId = String(format:"%@",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
        
        self.ser.getProfileData(id: strUserId)
    }
   
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: "Server error", viewcontroller: self)
    }

    @IBAction func saveInformation(_ sender: Any)
    {
        let info:UserInfo = UserInfo()
        info.fullName = txtFullName.text! as NSString!
        info.location = txtLocation.text! as NSString!
        let strImageURL = String(format:"%@%@","data:image/png;base64,",GlobalInfo.convertImageToBase64(image: imgProfile.image!) )
        
        info.imageUrl = strImageURL as NSString!
        info.userId = UserDefaults.standard.value(forKey: "userId") as! Int!
        let strBio = txtBioGraphy.text as String
        
        
        let messageData = strBio.data(using: String.Encoding.nonLossyASCII)
        let finalMessage = String(data: messageData!, encoding: String.Encoding.utf8)
        info.bio  =  finalMessage as NSString!
        info.dob  = btnDOB.titleLabel?.text as NSString!

        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.updateProfileData(userInfo: info)

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath as IndexPath) as! NotificationCell
        
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        if indexPath.row == 0
        {
            
           cell.lblName.text = "Log Out"
            cell.imgUser.image = imgProfile.image
            
        }
       // if indexPath.row == 1
        //{
            
            //cell.lblName.text = "Log Out"
            //cell.imgUser.image = UIImage(named: "Check.png")
        //}
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
       
        if indexPath.row == 0
        {
            
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            
            self.ser.logOut()
        }
    }
    
}




extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
