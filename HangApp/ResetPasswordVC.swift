//
//  ResetPasswordVC.swift
//  HangApp
//
//  Created by Evan Luthra on 23/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController ,AppServiceDelegate{
    @IBOutlet var txtOldPassword: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet weak var bgView: UIView!
    let ser:AppService = AppService()
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.clear
        view.isOpaque = false
        bgView.layer.cornerRadius = 6;
        bgView.layer.masksToBounds = true
        super.viewDidLoad()

        // Do any additional setup after loading the view.
           }
    @IBAction func cancelButtonClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func saveButtonClicked(_ sender: Any)
   
    {
        let alert = UIAlertView()
        alert.title = ""
        alert.addButton(withTitle: "OK")
        if txtConfirmPassword.text != ""
        {
            
        
        if txtPassword.text != txtConfirmPassword.text
        {
           alert.message = "These passwords don't match. Try again?"
            alert.show()

                               }
        else if (txtConfirmPassword.text?.characters.count)!<6
        {
           
            alert.message = "Password length should be greater than 6 characters "
            alert.show()
        
        }
        else{
        let parameters:[String: AnyObject] = [
            "password":txtPassword.text as AnyObject,
            "current_password":txtOldPassword.text as AnyObject,
            
            ]
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.resetPasswordWithInfo(parameters: parameters)
        }
        }
        else
        {
            alert.message = "Password field can't be empty"
            alert.show()
        
        }
      
    }
  
   
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            
            
        if(apiName == "resetPassword")
            {
                let alert = UIAlertView()
                alert.title = ""
                alert.message = response.statusDescription
                alert.addButton(withTitle: "OK")
                alert.show()
                self.dismiss(animated: true, completion: nil)
            }
            
            
            
            
        }
    }
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func textFieldShouldReturn(_ userText: UITextField) -> Bool
    {
        switch userText
        {
        case txtOldPassword:
             txtPassword.becomeFirstResponder()
            
        case txtPassword:
            txtConfirmPassword.becomeFirstResponder()
        default:
            break
        }
        userText.resignFirstResponder()
        return true;
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
        
    }

}
