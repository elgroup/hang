//
//  Location.swift
//  HangApp
//
//  Created by Evan Luthra on 31/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class Location: Response
{
    var lat:Float!
    var long:Float!
    var placeName:String!
     var placeDetailedAddress:String!
}
