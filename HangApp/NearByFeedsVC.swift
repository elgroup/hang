//
//  NearByFeedsVC.swift
//  Kwick
//
//  Created by Evan Luthra on 19/07/17.
//  Copyright © 2017 Evan Luthra. All rights reserved.
//

import UIKit
import GUIPlayerView

class NearByFeedsVC: UIViewController,UITableViewDelegate,UITableViewDataSource ,AppServiceDelegate,GUIPlayerViewDelegate{
 var feedsArray: NSMutableArray = NSMutableArray()
    @IBOutlet var feedsTableView:UITableView!
     @IBOutlet var feedsvideoView:GUIPlayerView!
    var likeButton = UIButton()
    
    var index:Int!
    
    
    override func viewDidLoad() {
        index = -1
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func reloadTable()
    {
        
        feedsTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 338
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "nearFeedsCell", for: indexPath) as! FeedsCell
       
        
        
        cell.backgroundColor = UIColor.clear
        let moment = feedsArray[indexPath.row] as! MomentsInfo
        if moment.fileType == ".mov"
        {
            cell.imgPlay.isHidden = false
            if let url  = NSURL(string: moment.momentsVideoThumbUrl)
                
            {
                if index != -1
                {
                    
                    let indexpath = NSIndexPath(row: index, section: 0)
                    let currentCell = tableView.cellForRow(at: indexpath as IndexPath)! as! FeedsCell
                    let videoPlayer = currentCell.playerView as GUIPlayerView
                    videoPlayer.clean()
                }
               
               
                let image = UIImage(named: "heng")
                cell.imgMoments.sd_setImage(with: url as URL, placeholderImage: image)
                cell.playerView = GUIPlayerView()
                 cell.playerView.frame = CGRect(x: cell.imgMoments.frame.origin.x, y: cell.imgMoments.frame.origin.y+44, width: cell.contentView.frame.size.width, height: cell.imgMoments.frame.size.height-44)
                cell.contentView.addSubview(cell.playerView)
                cell.playerView.delegate = self
                
                let videourl  = NSURL(string: moment.momentsVideoUrl)
                 cell.playerView.videoURL = videourl! as URL
                 cell.playerView.prepareAndPlayAutomatically(true)
            }
            
        }
        else
        {
            cell.imgPlay.isHidden = true
            
            if let url  = NSURL(string: moment.momentsUrl)
                
            {
                let image = UIImage(named: "heng")
                cell.imgMoments.sd_setImage(with: url as URL, placeholderImage: image)
            }
        }
          cell.lblCommentCount.text = String(format:"%d",moment.commentCount )
       
       
        cell.lblNoOfView.text = String(format:"%d",moment.likeCount )
        cell.lblUseName.text = moment.momentPostedByUser
        if let url  = NSURL(string: moment.momentPostedByUserImage)
            
        {
            cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
            cell.imgUser.clipsToBounds = true;
            let image = UIImage(named: "defaultUserImage")
            cell.imgUser.sd_setImage(with: url as URL, placeholderImage: image)
        }
        cell.btnReport.tag  = indexPath.row
        cell.btnReport.addTarget(self, action: #selector(NearByFeedsVC.reportButtonHandler(_:)), for: .touchUpInside)
        cell.btnLike.tag  = indexPath.row
        cell.btnReport.addTarget(self, action: #selector(NearByFeedsVC.likeButtonHandler(_:)), for: .touchUpInside)
       
        return cell
    }
    func reportButtonHandler(_ sender: UIButton)
        
        
    {
        
//        let indexpath = NSIndexPath(row: sender.tag, section: 0)
//        let currentCell = feedsTableView.cellForRow(at: indexpath as IndexPath)! as! FeedsCell
//        currentCell.reportView.isHidden = false
//        currentCell.reportView.layer.cornerRadius = 5
        
        
    }
    func likeButtonHandler(_ sender: UIButton)
        
        
    {
        
        //        let indexpath = NSIndexPath(row: sender.tag, section: 0)
        //        let currentCell = feedsTableView.cellForRow(at: indexpath as IndexPath)! as! FeedsCell
        //        currentCell.reportView.isHidden = false
        //        currentCell.reportView.layer.cornerRadius = 5
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            if(apiName == "sendLikeOnFeed" )
            {
   
                
//                let feed = feedsArray[likeButton.tag] as! FeedsInfo
//                feed.likeCount =  response.feedLikeCount
//                if likeButton.isSelected
//                {
//                    feed.isLike = false
//                }
//                else
//                {
//                    
//                    feed.isLike = true
//                }
//
//                 feedsArray.replaceObject(at: likeButton.tag, with: feed)
//                 feedsTableView.reloadData()

            }
       
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
       
    }
    func likeBtnPressed(_ button: UIButton)
    {
        
       // likeButton = button
         //let feed = feedsArray[button.tag] as! MomentsInfo
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        AppService.sharedInstance.delegate = self;
        //AppService.sharedInstance.sendLikeOnFeed(feedId: feed.feedId, feeduserId: feed.userId)
        
        
        
        
    }
    
    
    
        

       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
