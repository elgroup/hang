//
//  CheckinInfo.swift
//  HangApp
//
//  Created by Evan Luthra on 06/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class CheckinInfo: NSObject {
    var videodata:Data!
    var imagedata:Data!
    var loc_lat:Double!
    var loc_long:Double!
    var caption:String!
    var checkinDate:String!
    var checkinDistance:Double!
    var address:String!
    var categoryId:Int!
    var tag_Users:NSMutableArray!
    var commentsArray:NSMutableArray!
    var fileType:String!
    var placesUrl:String!
    var checkinId = Int()
    var likeCount = Int()
    var tagByUserId = Int()
    var tagByUserName = String()
    var tagByUserImageUrl = String()
    var tagsUsersArray:NSMutableArray!
    var followed_id = Int()
    var isFriend = Bool()
    var followed_users_status = Bool()
}
