//
//  InviteFriendsCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 30/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class InviteFriendsCtrl: UIViewController,UITableViewDelegate,UITableViewDataSource,AppServiceDelegate {
 @IBOutlet weak var headerView: UIView!
    let ser:AppService = AppService()
    var hangUser = UserContactInfo()
     var selectedCity = String()
    var isComingFrom = String()
    var fileType = String()
     var imgMoment = UIImage()
    var imgThumnail = UIImage()
      var urlFile = String()
     var videoURL:NSURL?
    var selectedCellInfo:((_ usersInfo:NSMutableArray ,_ cityName : String) -> Void)?
    @IBOutlet weak var friendsTable: UITableView!
    var userFriendsArray:NSMutableArray = []
    var selectedFriendsArray:NSMutableArray = []
    override func viewDidLoad() {
        callFriendsService()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func callFriendsService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        
        
        self.ser.getFriendsList()
    }
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        Loader.sharedLoader.hideLoader()
        
        if response.statusCode == true
        {
            
            if(apiName == "getFriendsList")
            {
                
                userFriendsArray = (response as! HangUsersParserInfo).hangUsers
                friendsTable.reloadData()
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
       
        return userFriendsArray.count
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inviteFriendCell", for: indexPath as IndexPath) as! FriendCell
        
        hangUser  = userFriendsArray[indexPath.row] as! UserContactInfo
         if hangUser.fullName != "" {
        cell.lblName.text = hangUser.fullName
        }
        cell.btnSelect.tag  = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(InviteFriendsCtrl.selectButtonHandler(_:)), for: .touchUpInside)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    func selectButtonHandler(_ sender: UIButton)
        
        
    {
        hangUser =  userFriendsArray[sender.tag] as! UserContactInfo
        
        if sender.isSelected
        {
            sender.isSelected = false
             selectedFriendsArray.remove(hangUser)
        }
        else
        {
            sender.isSelected = true
            selectedFriendsArray.add(hangUser)
            
        }
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        return 60.0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return ""
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "inviteHeaderCell") as! InviteHeaderCell
        
      return headerCell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
    }

    @IBAction func backPressed(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tagBtnPressed(_ sender: Any)
    {
        if isComingFrom == "editPhoto"
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let shareVC = mainStoryboard.instantiateViewController(withIdentifier: "share") as! ShareViewController
            
           shareVC.imageForShare = imgMoment
           shareVC.fileType = fileType
           shareVC.imgThumnailShare = imgThumnail
           shareVC.videoURLToShare = videoURL
           shareVC.tagPersons = selectedFriendsArray
           self.navigationController?.pushViewController(shareVC, animated: true)
        
        }
        else{
        
        if let didDismiss = self.selectedCellInfo
        {
            didDismiss(selectedFriendsArray,selectedCity)
            _ = self.navigationController?.popViewController(animated: true)
        }
        }
        
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
