//
//  ReviewCell.swift
//  HangApp
//
//  Created by Evan Luthra on 16/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
   @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var txtReview: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
