//
//  Messages.swift
//  HangApp
//
//  Created by Evan Luthra on 10/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class Messages: NSObject
{
    var is_read:Bool!
    var body:String!
    var message_date:String!
    var id = Int()
    var sender_id = Int()
    var sender_name : String!
     var imageUrl = String()
    var user_id = Int()
}
