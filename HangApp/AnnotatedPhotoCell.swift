//
//  AnnotatedPhotoCell.swift
//  RWDevCon
//
//  Created by Mic Pringle on 26/02/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit

class AnnotatedPhotoCell: UICollectionViewCell {
  
  @IBOutlet  weak var imageView: UIImageView!
  @IBOutlet  weak var userImageView: UIImageView!
  @IBOutlet  weak var lblComment: UILabel!
  @IBOutlet  weak var lblNoOfView: UILabel!
    @IBOutlet  weak var lblUerName: UILabel!
  @IBOutlet var imgPlay: UIImageView!
 @IBOutlet var lblCommentCount: UILabel!
  @IBOutlet weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!
 

 
  
}
