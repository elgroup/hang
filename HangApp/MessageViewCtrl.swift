//
//  MessageViewCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 31/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class MessageViewCtrl: UIViewController ,AppServiceDelegate, UITextViewDelegate
{
    let ser:AppService = AppService()
    @IBOutlet var txtMessaage: UITextView!
     @IBOutlet var lblReceiver: UILabel!
    var messageReceiver_id = String()
    var messageReceiver_name = String()
    override func viewDidLoad() {
        lblReceiver.text = messageReceiver_name
        print(messageReceiver_id)
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backPressed(_ sender: Any)
    {
        let presentingViewController = self.presentingViewController
        self.dismiss(animated: true) {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
        
    }
    @IBAction func sendPressed(_ sender: Any)
    {
       
        sendMessage()
    }
    func sendMessage()
    {
        let message:Messages = Messages()
        message.body = txtMessaage.text
        message.user_id = Int(messageReceiver_id)!
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.sendMessage(message: message)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            
            if(apiName == "sendMessage")
            {
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: response.statusDescription!, viewcontroller: self)
                let presentingViewController = self.presentingViewController
                self.dismiss(animated: true) {
                    presentingViewController?.dismiss(animated: true, completion: nil)
                }
                
               
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Write message") {
            textView.text  = ""
            textView.textColor = UIColor.darkGray
            
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = "Write message"
                textView.textColor = UIColor.lightGray
            }
            
        }
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if (text == "\n") {
                textView.resignFirstResponder()
                return false
            }
            return true
        }
        
  

}
