//
//  Validator.swift
//  BluLint
//
//  Created by Akhilesh on 3/7/16.
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import Foundation

///Singleton class, Validates fields
class Validator{
    static let sharedValidator = Validator()
    
    /// Validate Email address
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    /// Validate phone number
    func validatePhoneNo(value: String) -> Bool {
        if value.characters.count < 10
        {
            return false
        }
        
        return true
    }
    
    func validateUserName(userName:String,invertCharcterSet:CharacterSet) -> Bool
    {
     let filteredString:String = userName .components(separatedBy: invertCharcterSet).joined(separator: "")
        //        let filteredString:NSString  = userName.componentsSeparatedByCharactersInSet(  invertCharcterSet).joinWithSeparator("")
        return userName == filteredString as String
    }
    
}
