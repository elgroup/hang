//
//  SelectionLocationViewController.swift
//  BluLint
//
//  Created by hament miglani on 04/07/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit
import Alamofire
import MapKit


class SelectionLocationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextViewDelegate ,AppServiceDelegate,CameraViewControllerDelegate{
   
    @IBOutlet var searchBar:UISearchBar!
     var hangUser = UserContactInfo()
    var isComingFrom = String()
    var cityName = String()
    var lat = Double()
    var long = Double()
     let ser:AppService = AppService()
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet var tableView:UITableView!
     @IBOutlet var txtCaption:UITextView!
     @IBOutlet var txtCheckInDetail:UITextView!
     @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bottomView: UIView!
     @IBOutlet weak var imgCheckIn: UIImageView!
     @IBOutlet weak var topView: UIView!
     var selectedUsersArray:NSMutableArray = []
    var cityInfo :  CityResponse? = nil
    var selectedCellInfo:((_ placeInfo:CityInfo) -> Void)?

    override func viewDidLoad() {
        
        self.navigationController?.navigationBar.isHidden = true
         Loader.sharedLoader.showLoaderOnScreen(vc: self)
      super.viewDidLoad()
        getDataforFirstTime()
      searchBar.placeholder = MiscUtils.sharedMiscUtils.getStringResource(key: "Enter Place Name")
        
        
                    
          
        
}
    func setImage(_ image: UIImage!)
    {
        
    }
        
    @IBAction func checkInButtonClicked(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.2, delay: 1.2, options: [.repeat, .curveEaseInOut], animations: {}) { (bool) in
            self.tableView.isHidden = false
        }
       
        
    }

    @IBAction func photoButtonClicked(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let cameraVC = mainStoryboard.instantiateViewController(withIdentifier: "photocamera") as! CameraViewController
        cameraVC.comingfrom = "checkin"
        cameraVC.delegate = self
        let navigationVC = UINavigationController.init(rootViewController: cameraVC)
        self.present(navigationVC, animated: true, completion: nil)

    }
    @IBAction func tagFriendButtonClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let inviteFriendVC = mainStoryboard.instantiateViewController(withIdentifier: "InviteFriends") as! InviteFriendsCtrl
        
        inviteFriendVC.selectedCellInfo = {(usersInfo:NSMutableArray,cityName:String) -> Void in
           
           self.selectedUsersArray = usersInfo
            self.createClickableLink(cityName: cityName)
        }
        inviteFriendVC.selectedCity = cityName
        self.navigationController?.pushViewController(inviteFriendVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if (searchBar.text?.characters.count)! > 0{
            getCityNameResponse()
        }else{
         cityInfo?.citiesInfo.removeAll()
         tableView.reloadData()
        }
    }
    func setCheckIn(_ image: UIImage!)
    {
        mapView.isHidden = true
        imgCheckIn.isHidden = false
      imgCheckIn.image = image
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
    }
    
    //MARK: Custom Methods
    
    func getDataforFirstTime(){
        

                        if self.searchBar.text?.characters.count == 0{
                        GlobalInfo.sharedInstance.getCityNameManually(handler: { (countryName) -> Void in
                        
                            let str = String(format:"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=0.0,0.0&radius=1000&language=en&key=AIzaSyD-s_xp57cr7c3X4fNX6wugT1I0mh1o_Gw",countryName!).addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                            
                            self.getLocationFromString(str: str!)
                        })
                        }



        
       
 
    }
    
    func getCityNameResponse()
    {
        let str = String(format:"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&language=en&key=AIzaSyD-s_xp57cr7c3X4fNX6wugT1I0mh1o_Gw",searchBar.text!).addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)

        
        
        getLocationFromString(str: str!)

    }
    
    func showMap(latitude : Float , longitude :Float ,placeName :String ,detailedAddr :String )
    {
        
        //mapView.isScrollEnabled = false;
        lat = Double(latitude)
        long = Double(longitude)
        let location = CLLocationCoordinate2D(
            latitude: Double(latitude),
            longitude: Double(longitude)
        )
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = placeName
        annotation.subtitle = detailedAddr
        mapView.addAnnotation(annotation)
        
        
        let options = MKMapSnapshotOptions()
        
        options.region = self.mapView.region
        options.scale = UIScreen.main.scale
        options.size = self.mapView.frame.size;
        //let snapshotter = MKMapSnapshotter.init(options: options)
        //snapshotter.start(with: DispatchQueue.global(qos: DispatchQoS.QoSClass.default)) { snapshot, error in
            //guard let snapshot = snapshot else {
                //print("Snapshot error: \(error)")
                //fatalError()
            //}
            
          //  let pin = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
            //let image = snapshot.image
            
           // UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale)
            //image.draw(at: CGPoint.zero)
            
           // let visibleRect = CGRect(origin: CGPoint.zero, size: image.size)
            //for annotation in self.mapView.annotations {
             //   var point = snapshot.point(for: annotation.coordinate)
              //  if visibleRect.contains(point) {
              //      point.x = point.x + pin.centerOffset.x - (pin.bounds.size.width / 2)
               //     point.y = point.y + pin.centerOffset.y - (pin.bounds.size.height / 2)
                //    pin.image?.draw(at: point)
               // }
           // }
            
           // let compositeImage = UIGraphicsGetImageFromCurrentImageContext()
            //UIGraphicsEndImageContext()
            
            //let data = UIImagePNGRepresentation(compositeImage!)
            
           // let imageMap : UIImage  = UIImage(data: data!)!
            //self.imgMap.image = imageMap
            
            
        }

    
    
     func getLocationCoordinateFromPlaceID(str:String)
     {
        Alamofire.request(str, method:.get, encoding: JSONEncoding.default, headers: nil)
            .validate().responseJSON
            { response in
                switch response.result
                {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        let appParser:AppParser = AppParser()
                        let response:Location = appParser.parseCitiesResponseForLatLong(json: json)
                        if response.statusDescription?.caseInsensitiveCompare("OK") == ComparisonResult.orderedSame
                        {
                             Loader.sharedLoader.hideLoader()
                           
                            self.showMap(latitude: response.lat, longitude: response.long ,placeName: response.placeName ,detailedAddr: response.placeDetailedAddress )
                        }
                        else
                        {
                            self.tableView.isHidden = true
                        }
                    }
                case .failure(let error):
                    MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString: error.localizedDescription, viewcontroller: self)
                }
        }
    }
    
    
  func getLocationFromString(str:String){
       
        Alamofire.request(str, method:.get, encoding: JSONEncoding.default, headers: nil)
            .validate().responseJSON
            { response in
                switch response.result
                {
                case .success:
                    if let value = response.result.value {
                        DispatchQueue.main.async {
                            Loader.sharedLoader.hideLoader()
                        }
                        let json = JSON(value)
                        let appParser:AppParser = AppParser()
                        let response:CityResponse = appParser.parseCitiesResponse (json: json)
                        if response.statusDescription?.caseInsensitiveCompare("OK") == ComparisonResult.orderedSame
                        {
                           
                            self.cityInfo = response
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                            
                        }
                        else
                        {
                            self.tableView.isHidden = true
                        }
                    }
                case .failure(let error):
                    MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString: error.localizedDescription, viewcontroller: self)
                }
        }
    }
    
    //MARK:IBActions
    @IBAction func cancelButtonClicked(_ sender:AnyObject){
    self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareButtonClicked(_ sender:AnyObject){
        let info:CheckinInfo = CheckinInfo()
        
        if self.tableView.isHidden
        {
            let userIdArray = NSMutableArray()
            
            for person  in selectedUsersArray
            {
                let strId =  String(format:"%d", (person as! UserContactInfo).userId )
                
                
                userIdArray.add(strId)
            }
            
            
            if imgCheckIn.image != nil
            {
                info.imagedata = UIImagePNGRepresentation(imgCheckIn.image!)! as Data?
            }
            info.loc_lat = lat
            info.loc_long = long
            info.tag_Users = userIdArray
            info.fileType = ".png"
            info.address = cityName
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.postCheckinData(checkInfo: info)
        }
        else
        {
        
            let alert = UIAlertView()
            alert.title = ""
            alert.addButton(withTitle: "OK")
            alert.message = "Please select location"
            alert.show()
         
        
        }
        
            
        
       
    }
    
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        if response.statusCode == true
        {
            if(apiName == "postCheckinData")
            {
                DispatchQueue.main.async()
                    {
                        Loader.sharedLoader.hideLoader()
                        self.dismiss(animated: true, completion: nil)
                }

            }
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
        
        
    }

    
    
    //MARK: Table view delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.cityInfo != nil && (self.cityInfo?.citiesInfo.count)! > 0{
            tableView.backgroundView = nil
            tableView.separatorStyle = .singleLine
            return 1
        }
        else{
            let blankTableMessageLabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height) )
            blankTableMessageLabel.text = MiscUtils.sharedMiscUtils.getStringResource(key: "No Result Found")
            blankTableMessageLabel.textColor = UIColor.lightGray // grayColor()
            blankTableMessageLabel.numberOfLines = 0
            blankTableMessageLabel.textAlignment = .center
            blankTableMessageLabel.font = UIFont(name: "Roboto-Regular", size: 16.0)
            tableView.backgroundView = blankTableMessageLabel
            tableView.separatorStyle = .none
        }
        return 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        if self.cityInfo?.citiesInfo.count == 0
        {
            cell.textLabel!.text  = MiscUtils.sharedMiscUtils.getStringResource(key: "FindBook NoResult Found")
        }
        else{
            cell.textLabel!.text = self.cityInfo?.citiesInfo[indexPath.row].cityName}
        return cell
    }

    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Write caption") {
            textView.text  = ""
            textView.textColor = UIColor.darkGray
            
           
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.cityInfo != nil
        {
            switch (self.cityInfo?.citiesInfo.count)! {
            case 0 :
                return 1
            default:
                return (self.cityInfo?.citiesInfo.count)!
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        return 60.0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return ""
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
//        let headerView = UIView()
//        let headerCell = tableView.dequeueReusableCell(withIdentifier: "customTableViewCell") as! CustomTableViewCell
//        
//        
//        
//        headerView.addSubview(headerCell)
        return searchBar
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
      
        
       
        
        if isComingFrom == "places"
        {
                    tableView.isHidden = true
                    bottomView.isHidden = false
              Loader.sharedLoader.showLoaderOnScreen(vc: self)
                    cityName = (self.cityInfo?.citiesInfo[indexPath.row].cityName)!
                   createClickableLink(cityName: cityName)
            
            
                      let str = String(format:"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyD-s_xp57cr7c3X4fNX6wugT1I0mh1o_Gw",(self.cityInfo?.citiesInfo[indexPath.row].placeID)!).addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            
                    
                    
                      getLocationCoordinateFromPlaceID(str: str!)
            
        }
        else{
           
            if let didDismiss = self.selectedCellInfo
            {
                didDismiss((self.cityInfo?.citiesInfo[indexPath.row])!)
                self.dismiss(animated: true, completion: nil)
            }
        }

        
        
        
        
    }
    
    
    func createClickableLink(cityName : String)
    {
        var stringArray = [String]()
        
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
        imgUser.clipsToBounds = true;
        if let url  = NSURL(string: UserDefaults.standard.object(forKey: "userImage") as! String),
            let data = NSData(contentsOf: url as URL)
        {
            imgUser.image = UIImage(data: data as Data)
        }
      
        for var i in (0..<selectedUsersArray.count).reversed()
        {
           hangUser = selectedUsersArray[i] as! UserContactInfo
            if hangUser.fullName != "" {
                 stringArray.append(hangUser.fullName)
            }
            else
            {
              stringArray.append("N/A")
            }
           
        }
       
        let string = stringArray.joined(separator: ",")
        
        
        let userName =  UserDefaults.standard.object(forKey: "userFullName")
        
      
        
        var main_string = String(format:"%@- with %@ at %@",userName as! String ,string,cityName)
        
        if string == ""
        {
           main_string = main_string.replacingOccurrences(of: " with ", with: "")
        }
        
        
          let attribute = NSMutableAttributedString.init(string: main_string)
       let range = (main_string as NSString).range(of: cityName)
        
        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(colorLiteralRed: 0.86, green: 0.00, blue: 0.43, alpha: 1.00) , range: range)
        attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (txtCheckInDetail.font?.fontName)!, size: 15) as Any , range: range)
        attribute.addAttribute(NSLinkAttributeName, value:"open", range: range)
        
        
        let range1 = (main_string as NSString).range(of: userName as! String)
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range1)
         attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (txtCheckInDetail.font?.fontName)!, size: 17) as Any , range: range1)
        let range2 = (main_string as NSString).range(of: string)
         attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range1)
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range2)
        attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (txtCheckInDetail.font?.fontName)!, size: 17) as Any , range: range2)
        txtCheckInDetail.linkTextAttributes = [ NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.86, green: 0.00, blue: 0.43, alpha: 1.00)]
        txtCheckInDetail.attributedText  = attribute
        
    }
func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
     checkInButtonClicked(self)
     return true;
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    
}
