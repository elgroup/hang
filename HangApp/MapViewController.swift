//
//  MapViewController.swift
//  HangApp
//
//  Created by Evan Luthra on 30/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
 @IBOutlet weak var mapView: MKMapView!
    var checkinInfo = CheckinInfo()
   
    override func viewDidLoad() {
        
        showMap(latitude: checkinInfo.loc_lat, longitude: checkinInfo.loc_long, placeName: checkinInfo.address)
        
         self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showMap(latitude : Double , longitude :Double ,placeName :String)
    {
    
        let location = CLLocationCoordinate2D(
            latitude: Double(latitude),
            longitude: Double(longitude)
        )
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = placeName
        mapView.addAnnotation(annotation)
        
        
       
        
    }

}
