//
//  PlacesViewCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 01/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import MapKit
class PlacesViewCtrl: UIViewController ,UITableViewDataSource,UITableViewDelegate ,UITextViewDelegate,UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate,AppServiceDelegate
{
@IBOutlet weak var placesTable: UITableView!
    let ser:AppService = AppService()
    var currentPlace = String()
    var comingFrom = String()
    var userId = String()
    var userPlacesArray:NSMutableArray = []
    var results = [String]()
    var tagsString =  String()
    override func viewDidLoad() {
        //placesTable.backgroundColor = UIColor.red
       callPlacesService()

        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func callPlacesService()
    {
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.getCheckInDataById(id: userId)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userPlacesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
         let place = userPlacesArray[indexPath.row] as! CheckinInfo
        if place.commentsArray.count == 0
        {
             return 260.0
        }
        
        return 320.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placesCell", for: indexPath as IndexPath) as! FeedsCell
        
      
         let place = userPlacesArray[indexPath.row] as! CheckinInfo
         let strUserId = String(format:"%@",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
         if place.tag_Users.count>0
         {
        if strUserId != String(place.tagByUserId)
        {
            if place.tag_Users.count>1
            {
             results = [place.tagByUserName ,"other","You",String(place.tagByUserId)]
             tagsString = String(format:"%@ checked in with You & other", place.tagByUserName)
            }
            else
            {
                results = [place.tagByUserName ,"You",String(place.tagByUserId)]
                tagsString = String(format:"%@ checked in with You", place.tagByUserName)
            }

            
            
        }
        else
        {
            if place.tag_Users.count>1
            {
            results = [(place.tag_Users[0] as! Tags).tagUser_Name  ,"other","You",String((place.tag_Users[0] as! Tags).tagUser_id )]
            tagsString = String(format:"You checked in with %@ & other", (place.tag_Users[0] as! Tags).tagUser_Name )
            }
            else
            {
            results = [(place.tag_Users[0] as! Tags).tagUser_Name  ,"You",String((place.tag_Users[0] as! Tags).tagUser_id )]
            tagsString = String(format:"You checked in with %@", (place.tag_Users[0] as! Tags).tagUser_Name )
            }
        
        }
            cell.checkInDescr.attributedText = createClickableLink(userNames: results, checkInDescr:  cell.checkInDescr, main_string: tagsString)
            cell.checkInDescr.tag = indexPath.row
            if  (place.tagByUserImageUrl != "")
                
            {
                let url  = NSURL(string:place.tagByUserImageUrl)
                cell.mapView.isHidden = true
                let image = UIImage(named: "defaultUserImage.png")
                
                
                cell.imgUser.sd_setImage(with: url as! URL, placeholderImage: image)
            }

        }
        else
         {
            let userName =  UserDefaults.standard.object(forKey: "userFullName")
            let userImage =  UserDefaults.standard.object(forKey: "userImage") as! String
            
            results = [userName as! String  ,"You",String(userId)]
            tagsString = String(format:"%@ checked in", userName as! String )
            cell.checkInDescr.attributedText = createClickableLink(userNames: results, checkInDescr:  cell.checkInDescr, main_string: tagsString)
            if  (userImage != "")
                
            {
                let url  = NSURL(string:userImage)
                cell.mapView.isHidden = true
                let image = UIImage(named: "defaultUserImage.png")
                
                
                cell.imgUser.sd_setImage(with: url as! URL, placeholderImage: image)
            }
            

        
         }
        
        
       
        
        if  (place.placesUrl != "")
            
        {
            let url  = NSURL(string: place.placesUrl)
            cell.mapView.isHidden = true
            let image = UIImage(named: "heng")
            
           
            cell.imgPlaces.sd_setImage(with: url as! URL, placeholderImage: image)
        }
        else
        {
        
            cell.mapView.isHidden = false
            cell.mapView.tag  = indexPath.row
            let tapOnMap = UITapGestureRecognizer(target: self, action: #selector(PlacesViewCtrl.handleTapOnMap(gestureRecognizer:)))
            tapOnMap.numberOfTapsRequired = 1
            tapOnMap.numberOfTouchesRequired = 1
            
            cell.mapView.addGestureRecognizer(tapOnMap)
            showMap(latitude: place.loc_lat, longitude: place.loc_long, cell: cell)

        }
        let tapOntextBox = UITapGestureRecognizer(target: self, action: #selector(PlacesViewCtrl.handleTapOnTextBox(gestureRecognizer:)))
        tapOntextBox.numberOfTapsRequired = 1
        tapOntextBox.numberOfTouchesRequired = 1
        let tapOntextBox1 = UITapGestureRecognizer(target: self, action: #selector(PlacesViewCtrl.handleTapOnTextBox(gestureRecognizer:)))
        tapOntextBox1.numberOfTapsRequired = 1
        tapOntextBox1.numberOfTouchesRequired = 1

        
        cell.txtComment.addGestureRecognizer(tapOntextBox)
        cell.txtComment.tag = indexPath.row
        cell.txtComment1.addGestureRecognizer(tapOntextBox1)
        cell.txtComment1.tag = indexPath.row
        let distance   = place.checkinDistance
        cell.lblDistance.text =  String(format:"%0.1f km",distance! )
        cell.lblLocation.text =  String(format:"%@",place.address)
        cell.lblDuration.text = MiscUtils.sharedMiscUtils.getDurationFromCurrentDate(dateString: place.checkinDate)
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        cell.imgCommentUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgCommentUser.clipsToBounds = true;
        if place.commentsArray.count > 0
        {
            let cmt  = place.commentsArray[0] as! Comment
            
       cell.imgCommentUser.sd_setImage(with: NSURL(string: cmt.imageUrl) as! URL, placeholderImage: UIImage(named: "defaultUserImage.png"))
        cell.lblComment.text = cmt.commentDesc
         cell.lblComment.isHidden = false;
            cell.imgCommentUser.isHidden = false
            cell.txtComment1.isHidden = true;

            //cell.txtComment.topAnchor.constraint(
               // equalTo: cell.lineview2.bottomAnchor,
                //constant: 2).isActive = true

        }
        else
        {
            cell.txtComment1.isHidden = false;
            cell.lblComment.isHidden = true
            cell.imgCommentUser.isHidden = true
            cell.txtComment.translatesAutoresizingMaskIntoConstraints = false
           // cell.txtComment.topAnchor.constraint(
                //equalTo: cell.lineview1.bottomAnchor,
             //   constant: 2).isActive = true
       
        }
       
        cell.checkInDescr.isScrollEnabled = false
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
         if comingFrom == "profile"
         {
         return 60.0
         }
         return 0.0
    }
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
    return ""
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if comingFrom == "profile" {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60))
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "customTableViewCell") as! CustomTableViewCell
            headerCell.locationName.text = getCurrentLocation()
            headerCell.frame = headerView.frame
            
            headerView.addSubview(headerCell)
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(PlacesViewCtrl.handleTap(gestureRecognizer:)))
            headerCell.btnCheckin.addTarget(self, action: #selector(PlacesViewCtrl.checkinButtonHandler(_:)), for: .touchUpInside)
            tapRecognizer.numberOfTapsRequired = 1
            tapRecognizer.numberOfTouchesRequired = 1
            headerView.addGestureRecognizer(tapRecognizer)
            return headerView
        }
        return nil
    }
    
    func checkinButtonHandler(_ sender: UIButton)
    {
        
        
        if sender.isSelected
        {
            sender.isSelected = false
        }
        else
        {
            sender.isSelected = true
            sender.setTitle("", for: .selected)
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            let info:CheckinInfo = CheckinInfo()
            

            let userIdArray = NSMutableArray()
            
            info.loc_lat = AppDelegate.getAppDelegate().coordinates?.latitude
            info.loc_long = AppDelegate.getAppDelegate().coordinates?.longitude
            info.tag_Users = userIdArray
            info.fileType = ".png"
            info.address = getCurrentLocation()
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.postCheckinData(checkInfo: info)
           
            
            
        }
    }
    
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        
        if response.statusCode == true
        {
            if(apiName == "postCheckinData")
            {
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: "", viewcontroller: self)
                callPlacesService()
            }
            
            if(apiName == "getCheckInDataById")
            {
                 let profiledata = response as! UserContactInfo
                userPlacesArray = profiledata.momentsArray
                placesTable.reloadData()

            }
            
        }
    }
    func getCurrentLocation()->String
    {
       
                
                    GlobalInfo.sharedInstance.getCityNameManually(handler: { (countryName) -> Void in
                        self.currentPlace = countryName!
                        self.placesTable.reloadData()
                      
                    })
            
                
                
                
        
        
        return currentPlace
    }
    
    
    func showMap(latitude : Double , longitude :Double ,cell : FeedsCell )
    {
        
        
    
       cell.mapView.isScrollEnabled = false;
        
        let location = CLLocationCoordinate2D(
            latitude: Double(latitude),
            longitude: Double(longitude)
        )
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        cell.mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        cell.mapView.addAnnotation(annotation)
        let options = MKMapSnapshotOptions()
        options.region = cell.mapView.region
        options.scale = UIScreen.main.scale
        options.size = cell.mapView.frame.size;
        
        
        
    }

    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
       
            MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    
        
    }

    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
       searchLocation()
    }
    
    func handleTapOnMap(gestureRecognizer: UIGestureRecognizer)
    {
     
      let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "mapview") as! MapViewController
        popupVC.modalPresentationStyle = .overCurrentContext
        popupVC.modalTransitionStyle = .crossDissolve
        popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        let pVC = popupVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = self.view
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        let mapView = gestureRecognizer.view as! MKMapView!
        let tag = mapView?.tag
        
        let place = userPlacesArray[tag!] as! CheckinInfo
        popupVC.checkinInfo = place
        self.present(popupVC, animated: true, completion: nil)
    }
    
    
    func handleTapOnTextBox(gestureRecognizer: UIGestureRecognizer)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "CommentListVC") as! CommentListVC
        let place = userPlacesArray[(gestureRecognizer.view?.tag)!] as! CheckinInfo
        popupVC.apiname = "/comment_places"
        popupVC.moment_id = place.checkinId
        popupVC.user_id = place.tagByUserId
        popupVC.userCommentArray = place.commentsArray
        popupVC.modalPresentationStyle = .overCurrentContext
        popupVC.modalTransitionStyle = .crossDissolve
        popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        let pVC = popupVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = gestureRecognizer.view
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        
        present(popupVC, animated: true, completion: nil)
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
        
     }
    func searchLocation()
        
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle: nil)
        let selectionLocationViewController = mainStoryboard.instantiateViewController(withIdentifier: "SelectionLocationViewController") as! SelectionLocationViewController
        selectionLocationViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        let nav = UINavigationController.init(rootViewController: selectionLocationViewController)
        self.navigationController?.present(nav, animated: true, completion: nil)
        //        self.presentViewController(selectionLocationViewController, animated: true, completion: nil)
        
      selectionLocationViewController.isComingFrom = "places"
        selectionLocationViewController.selectedCellInfo = {(placeInfo:CityInfo) -> Void in
//            self.cityInfo = placeInfo
//            self.lblLocation.text = self.cityInfo?.cityName
        }
    }
    

    
    func createClickableLink(userNames : [String] , checkInDescr:UITextView , main_string:String ) -> NSMutableAttributedString
    {
        
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        for var i in (0..<userNames.count).reversed()
        {
            
            
            
            let range = (main_string as NSString).range(of: userNames[i] as String)
            
            //let replaced = String(userNames[i].characters.filter {$0 != " "})
            
            let userId  =  userNames.last
            let fullNameArr = userNames[i].components(separatedBy: " ")
            let replaced = fullNameArr.joined(separator:"@")
            
            let resultStr = replaced+"#"+userId!
            
            attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 18) as Any , range: range)
            attribute.addAttribute(NSLinkAttributeName, value:resultStr, range: range)
            checkInDescr.linkTextAttributes = [ NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.86, green: 0.00, blue: 0.43, alpha: 1.00)]
            
            
            
        }
        var  strCheckIn = String()
        if main_string.contains("checked in with") {
            strCheckIn = "checked in with"
        }
        else
        {
         strCheckIn = "checked in"
        }
        
        
        let range1 = (main_string as NSString).range(of: strCheckIn)
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range1)
        attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 18) as Any , range: range1)
        let range2 = (main_string as NSString).range(of: "&")
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range2)
        attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 18) as Any , range: range2)
        return attribute
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "popUp") as! PopUpViewCtrl
        
        
        var array = String(describing: URL).components(separatedBy: "#")
        if array[0] != "other" &&  array[0] != "You"
        {
            popupVC.messageReceiver_id = array.last!
            popupVC.messageReceiver_name = array[0]
            
            popupVC.modalPresentationStyle = .overCurrentContext
            popupVC.modalTransitionStyle = .crossDissolve
            popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
            let pVC = popupVC.popoverPresentationController
            pVC?.permittedArrowDirections = .any
            pVC?.delegate = self
            pVC?.sourceView = self.view
            pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
            
            self.present(popupVC, animated: true, completion: nil)
        }
        else if array[0] == "other"
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle: nil)
            let tagsController = mainStoryboard.instantiateViewController(withIdentifier: "tagUsers") as! TagUsersVC
            tagsController.modalPresentationStyle = .overCurrentContext
            tagsController.modalTransitionStyle = .crossDissolve
            tagsController.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
            let pVC = popupVC.popoverPresentationController
            pVC?.permittedArrowDirections = .any
            pVC?.delegate = self
            pVC?.sourceView = self.view
            pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
             let place = userPlacesArray[textView.tag] as! CheckinInfo
            tagsController.tagUsersArray = place.tag_Users
            
            
            self.present(tagsController, animated: true, completion: nil)
        }
        
        
        return true;
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
       

    }

}

extension UIView {
    
func addConstaintsToSuperview(leftOffset: CGFloat, topOffset: CGFloat , view : UIView , superView: UIView) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
       view.addConstraint(NSLayoutConstraint(item: view,
                                                         attribute: .leading,
                                                         relatedBy: .equal,
                                                         toItem: superView,
                                                         attribute: .leading,
                                                         multiplier: 1,
                                                         constant: leftOffset))
        
        view.addConstraint(NSLayoutConstraint(item: self,
                                                         attribute: .top,
                                                         relatedBy: .equal,
                                                         toItem: self.superview,
                                                         attribute: .top,
                                                         multiplier: 1,
                                                         constant: topOffset))
    }
    
    func addConstaints(height: CGFloat, width: CGFloat) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraint(NSLayoutConstraint(item: self,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1,
                                              constant: height))
        
        
        self.addConstraint(NSLayoutConstraint(item: self,
                                              attribute: .width,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1,
                                              constant: width))
    }
    
}
