//
//  MessageCell.swift
//  HangApp
//
//  Created by Evan Luthra on 04/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var lblMessageDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
