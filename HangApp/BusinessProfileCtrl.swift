//
//  BusinessProfileCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 16/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class BusinessProfileCtrl: UIViewController ,AppServiceDelegate,CAPSPageMenuDelegate{
    
   
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    let ser:AppService = AppService()
    var momentsArray:NSMutableArray = []
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    var pageMenu : CAPSPageMenuNew?
    
    
    @IBOutlet weak var bottomView: UIView!
    
   
    
    
    @IBAction func editProfileBtnClicked(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "editProfile") as! EditProfileController
        
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    @IBAction func openMessages(_ sender: Any)
    {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let messageVC = mainStoryboard.instantiateViewController(withIdentifier: "messageList") as! MessageListViewCtrl
        
        self.navigationController?.pushViewController(messageVC, animated: true)
        
    }
    
    
    func open(scheme: URL) {
        
    }
    @IBAction func openFacebook(_ sender: Any)
    {
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "fb://profile/\("")", iTuneUrlString: "https://itunes.apple.com/in/app/facebook/id284882215?mt=8")
    }
    @IBAction func openTwitter(_ sender: Any)
    {
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "twitter://", iTuneUrlString: "https://itunes.apple.com/in/app/twitter/id409789998?mt=12")
        
    }
    @IBAction func openInstagram(_ sender: Any) {
        
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "instagram://user?username=johndoe", iTuneUrlString: "https://itunes.apple.com/in/app/instagram/id389801252?mt=8")
        
    }
    @IBAction func openSnapchat(_ sender: Any)
    {
        
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "snapchat://", iTuneUrlString: "https://itunes.apple.com/in/app/snapchat/id447188370?mt=8")
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        callProfileService()
       
        super.viewWillAppear(animated)
    }
    
    func layOutBottomView()
    {
        var controllerArray : [UIViewController] = []
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 : PhotoStreamViewController = mainStoryboard.instantiateViewController(withIdentifier: "photoStream") as! PhotoStreamViewController
        
        controller1.title = "Photos"
        controller1.userMomentsArray = momentsArray
        controllerArray.append(controller1)
        
        
        let controller2 : PlacesViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "places") as! PlacesViewCtrl
        
        controller2.comingFrom  = "profile"
        controller2.title = "Checkins"
        controllerArray.append(controller2)
        let controller3 : ReviewsViewController = mainStoryboard.instantiateViewController(withIdentifier: "reviews") as! ReviewsViewController
        
        controller3.title = "Reviews"
        controllerArray.append(controller3)
        
        // Customize menu (Optional)
        
        let navColor : UIColor =  UIColor(patternImage: UIImage(named: "navBg.png")!)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(navColor),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
            .menuHeight(40.0),
            .menuItemWidth(110.0),
            .centerMenuItems(false)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenuNew(viewControllers: controllerArray, frame: CGRect(x: 0.0, y:0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        self.addChildViewController(pageMenu!)
        self.bottomView.addSubview(pageMenu!.view)
        
        
        pageMenu!.didMove(toParentViewController: self)
        
    }
    
    func callProfileService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        
        let strUserId = String(format:"%@",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
        
        self.ser.getProfileData(id: strUserId)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        
        
        
        if response.statusCode == true
        {
            
            if(apiName == "getProfileData")
            {
                
                let profiledata = response as! UserContactInfo
                lblUserName.text = profiledata.fullName;
                momentsArray =   profiledata.momentsArray
                UserDefaults.standard.set(lblUserName.text, forKey: "userFullName")
                UserDefaults.standard.set(profiledata.imageUrl, forKey: "userImage")
                               
                DispatchQueue.main.async
                    {
                        self.layOutBottomView()
                        Loader.sharedLoader.hideLoader()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            if let url  = NSURL(string: profiledata.imageUrl),
                                let data = NSData(contentsOf: url as URL)
                            {
                                self.imgUser.image = UIImage(data: data as Data)
                                
                            }
                        })
                        
                }
                
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        layOutBottomView()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    func didMoveToPage(_ controller: UIViewController, index: Int)
    {
        print(index)
        
    }
    
    
    
    
}
