//
//  DashBoardCtrl.swift
//  PageMenuDemoStoryboard
//
//  Created by Evan Luthra on 27/02/17.
//  Copyright © 2017 CAPS. All rights reserved.
//

import UIKit

class DashBoardCtrl: UIViewController {
  var bannersArray:NSMutableArray = []
   
     @IBOutlet weak var cameraButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let swipeUp = UISwipeGestureRecognizer()
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        swipeUp.addTarget(self, action: #selector(DashBoardCtrl.openCamera(_:)))
        cameraButton.addGestureRecognizer(swipeUp)
       // getbanners()
    }

    @IBAction func openCamera(_ sender: Any)
    {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let cavaraVC = mainStoryboard.instantiateViewController(withIdentifier: "photocamera") as! CameraViewController
        let navigationVC = UINavigationController.init(rootViewController: cavaraVC)
        self.present(navigationVC, animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
               
        self.navigationController?.setNavigationBarHidden(true, animated: true)

      
    }
    
    
    
    
   

    
}

    



