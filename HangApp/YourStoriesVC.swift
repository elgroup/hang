
import UIKit


class YourStoriesVC: UIViewController ,UIScrollViewDelegate{
var yourstoriesArray = NSMutableArray()
   
    var timer = Timer()
    var progressCount : Int = 0
    var slideshowTime : Float = 0.0
   
    var bannerImg = MomentsInfo()
    
    @IBOutlet  var progressBarFlatRainbow :YLProgressBar!

    @IBOutlet var scrollView: UIScrollView!
    override func viewDidLoad() {
        addBanner()
        addprograssView()
        let count : Int = yourstoriesArray.count
        let slideShowTotalTime : Float = 7.0
        let value1 : CGFloat = 1.0
        let value2 : CGFloat = CGFloat(Float(count))
        let value3   = value1/value2
        slideshowTime = slideShowTotalTime*Float(value3)
            progressBarFlatRainbow.setProgress(0, animated: true)
         timer = Timer.scheduledTimer(timeInterval: TimeInterval(slideshowTime), target: self, selector:  #selector(YourStoriesVC.moveToNextPage), userInfo: nil, repeats: true)
       
        progressCount = 1
        progressBarFlatRainbow.setProgress(value3*CGFloat(progressCount), animated: true)
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addBanner()
    {
        
        self.scrollView.frame = CGRect(x: 0, y: 45, width: self.view.frame.width, height:  self.view.frame.height)
        let scrollViewWidth: CGFloat = self.scrollView.frame.width
        let scrollViewHeight: CGFloat = self.scrollView.frame.height
       
       
            for var i in (0..<self.yourstoriesArray.count)
            {
                
                let imgBanner = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(i), y: 0, width: scrollViewWidth, height: scrollViewHeight-20))
                imgBanner.contentMode = UIViewContentMode.scaleToFill
                self.bannerImg  = self.yourstoriesArray[i] as! MomentsInfo
                let image = UIImage(named: "heng")
                imgBanner.sd_setImage(with: NSURL(string: self.bannerImg.momentsUrl) as! URL, placeholderImage: image)
                
                
                self.scrollView.addSubview(imgBanner)
                
        }
       
        
        
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(yourstoriesArray.count) , height: self.scrollView.frame.height)
        self.scrollView.delegate = self
       
    }
    
    func addprograssView()
        
    {
        let tintColorArray : [UIColor] = [UIColor.init(colorLiteralRed: 33/255, green: 180/255 , blue: 162/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 33/255, green: 180/255 , blue: 162/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 3/255, green: 137/255 , blue: 166/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 91/255, green: 63/255 , blue: 150/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 87/255, green: 26/255 , blue: 70/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 126/255, green: 26/255 , blue: 36/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 149/255, green: 37/255 , blue: 36/255 , alpha: 1.0), UIColor.init(colorLiteralRed: 228/255, green: 69/255 , blue: 39/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 245/255, green: 166/255 , blue: 35/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 33/255, green: 180/255 , blue: 162/255 , alpha: 1.0),UIColor.init(colorLiteralRed: 111/255, green: 188/255 , blue: 84/255 , alpha: 1.0)]
    
        progressBarFlatRainbow.type               = YLProgressBarType(rawValue: 1)!
        progressBarFlatRainbow.progressTintColors = tintColorArray
        progressBarFlatRainbow.hideStripes        = true;
        progressBarFlatRainbow.hideTrack          = true;
        progressBarFlatRainbow.behavior           = YLProgressBarBehavior(rawValue: 0)!
        
    }
    
    func moveToNextPage (){
        progressCount += 1
        let pageWidth:CGFloat = self.scrollView.frame.width
        let count : Int = yourstoriesArray.count
        let value1 : CGFloat = 1.0
        let value2 : CGFloat = CGFloat(Float(count))
        let value3   = value1/value2
        
        progressBarFlatRainbow.setProgress(value3*CGFloat(progressCount), animated: true)
        
        let maxWidth:CGFloat = pageWidth * CGFloat(count)
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        let slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            
            self.dismiss(animated: true, completion: nil)
            timer.invalidate()
        }
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        
        if scrollView == self.scrollView
        {
            
        }
        
    }
    

}
