//
//  FeedsCell.swift
//  HangApp
//
//  Created by Evan Luthra on 17/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import MapKit
import GUIPlayerView
class FeedsCell: UITableViewCell ,GUIPlayerViewDelegate{
    @IBOutlet var imgPlaces: UIImageView!
    @IBOutlet var imgMoments: UIImageView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgPlay: UIImageView!
    @IBOutlet var imgCommentUser: UIImageView!
    @IBOutlet var checkInDescr: UITextView!
    @IBOutlet var txtComment: UITextView!
    @IBOutlet var txtComment1: UITextView!
    @IBOutlet var txtCaption: UITextView!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblUseName: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblComment: UILabel!
    @IBOutlet var lblNoOfView: UILabel!
    @IBOutlet var playerView:GUIPlayerView!
     @IBOutlet var feedsvideoView:UIView!
    @IBOutlet var lblCommentCount: UILabel!
    @IBOutlet var lblLikesCount: UILabel!
    @IBOutlet var lblViewsCount: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var btnPlay: UIButton!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var btnReport: UIButton!
    @IBOutlet var btnTag: UIButton!
    @IBOutlet var reportView: UIView!
     @IBOutlet var lineview2: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
