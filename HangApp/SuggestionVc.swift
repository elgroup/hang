//
//  SuggestionVc.swift
//  HangApp
//
//  Created by Evan Luthra on 14/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class SuggestionVc: UIViewController , UITableViewDataSource ,UITableViewDelegate ,AppServiceDelegate{
    @IBOutlet weak var suggestionTable: UITableView!
  @IBOutlet var bgViewSin: UIView!
     @IBOutlet var lblNoSuggestion: UILabel!
      let ser:AppService = AppService()
      var suggestionUsersArray:NSMutableArray = []
      var bannersArray:NSMutableArray = []
      var hangUser = UserContactInfo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if suggestionUsersArray.count == 0 {
            lblNoSuggestion.isHidden = false
        }
        else
        {
            lblNoSuggestion.isHidden = true

        }
      
        
        bgViewSin.layer.cornerRadius = 6;
        bgViewSin.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        Loader.sharedLoader.hideLoader()
        if response.statusCode == true
        {
            if(apiName == "followUser")
            {
               
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: response.statusDescription!, viewcontroller: self)
            }
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestionUsersArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionCell", for: indexPath as IndexPath) as! SuggestionCell
        
        hangUser  = suggestionUsersArray[indexPath.row] as! UserContactInfo
        cell.lblName.text = hangUser.fullName
        if hangUser.userName != "" {
             cell.lblUserName.text =  String(format:"@%@",hangUser.userName)
        }
       
        
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        if let url  = NSURL(string: hangUser.imageUrl),
            let data = NSData(contentsOf: url as URL)
        {
             cell.imgUser.image = UIImage(data: data as Data)
        }
        cell.btnStatus.tag  = hangUser.accountId
        cell.btnStatus.addTarget(self, action: #selector(SuggestionVc.followButtonHandler(_:)), for: .touchUpInside)
      
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    
    @IBAction func nextButtonClicked(_ sender: Any)
    {
      
        
             goToDashBoard()
        
    }
    @IBAction func skipButtonClicked(_ sender: Any)
    {
         goToDashBoard()
    }
    
    func followButtonHandler(_ sender: UIButton)
        
        
    {
        let accountId =  String(format:"%d",sender.tag )
        
        if sender.isSelected
              {
                sender.isSelected = false
               }
                else
                {
                    sender.isSelected = true
                    sender.setTitle("", for: .selected)
                    Loader.sharedLoader.showLoaderOnScreen(vc: self)
                    ser.delegate = self;
                    self.ser.followUserWithUserId(id: accountId)
                
                }
        
       
        
        
    }
   
   
    func goToDashBoard()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let dashboardVC = mainStoryboard.instantiateViewController(withIdentifier: "dashboard") as! DashBoardCtrl
        self.navigationController?.pushViewController(dashboardVC, animated: true)
        
        
    }
    
    
   

}
