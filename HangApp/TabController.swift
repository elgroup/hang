
//
//  TabController.swift
//  HomeCollectionView
//
//  Created by hament miglani on 20/03/2016 .
//  Copyright © 2016 hament miglani. All rights reserved.
//kProfilePICURL

import UIKit



var isGroupViewVisible:Bool?
class TabController: UITabBarController,UITabBarControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet var tabbar: UITabBar!
    var groupNameLabel = UILabel()
    var groupNameLabelTemp = UILabel()
    var groupImage =  UIImageView()
    var profileBGimage = UIImageView()
    var widthConstraint:NSLayoutConstraint?
   
    var  bgView = UIView()
    var tapGesture:UITapGestureRecognizer?
    var chatCountLabel = UILabel()
    var chatCountImageView = UIImageView(image: UIImage(named: "chat_msg_bg"))
    var likeDislikeView:UIView?
    let likeCountImageView = UIImageView(image: UIImage(named: "filled-circle_Vote"))
    let dislikeCountImageView = UIImageView(image: UIImage(named: "chat_msg_bg"))
    let likeCountLabel = UILabel()
    let dislikeCountLabel = UILabel()
    var voteUnvoteBtn:UIButton?
    
    var likeDislikeBarButton:UIBarButtonItem?
    var backButton:UIButton?
    var chatButton:UIButton?
    
   
    static let sharedTabController = TabController()

   
    
   
    
   override func viewWillAppear(_ animated: Bool)
   {
    super.viewWillAppear(animated)
    UITabBar.appearance().backgroundColor = UIColor.red
    self.tabBarController?.delegate = self
  

    self.tabBar.barTintColor = .white
    self.tabBar.layer.borderWidth = 0.50
    self.tabBar.layer.borderColor = UIColor.clear.cgColor
    self.tabBar.clipsToBounds = true
    
    
    
    }
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tapGesture = UITapGestureRecognizer()
        tapGesture?.delegate = self
        tapGesture?.cancelsTouchesInView = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       

    }
    
  
    
  
   
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
       
     
    
    }
    
   
    
    
  
   
    
    //MARK: Tap Gesture Selector
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
       
        return true
    }
    
 
    
}
