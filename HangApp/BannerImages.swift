//
//  BannerImages.swift
//  HangApp
//
//  Created by Evan Luthra on 16/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class BannerImages: NSObject
{
    var title = String()
    var imageUrl = String()
    var iconUrl = String()
     var bannerId = Int()
}
