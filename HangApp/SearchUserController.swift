//
//  SearchUserController.swift
//  HangApp
//
//  Created by Evan Luthra on 24/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class SearchUserController: UIViewController ,UITableViewDataSource ,UITableViewDelegate ,AppServiceDelegate ,UITextFieldDelegate,UISearchBarDelegate {
@IBOutlet weak var suggestionTable: UITableView!
   
    let ser:AppService = AppService()
    var searchUsersArray:NSMutableArray = []
   
    var hangUser = UserContactInfo()
    override func viewDidLoad() {
       
 
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func searchBtnClicked(_ sender: Any)
    {
       self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnPressed(_ sender: Any)
    {
    _ = self.navigationController?.popViewController(animated: true)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            if(apiName == "searchUsers")
            {
                
            searchUsersArray = (response as! HangUsersParserInfo).hangUsers
                suggestionTable.reloadData()
            }
            if(apiName == "followUser")
            {
                
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: response.statusDescription!, viewcontroller: self)
            }
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var searchStr = textField.text
        
        if string != ""  {
           searchStr = String(format:"%@%@",searchStr!,string)
        }
        else
        {
           searchStr = searchStr?.substring(to: (searchStr?.index(before: (searchStr?.endIndex)!))!)
            
           
            
        }
        
        //Loader.sharedLoader.showLoaderOnScreen(vc: self)
        AppService.sharedInstance.delegate=self
        AppService.sharedInstance.searchUsers(searchString: searchStr!)
        return true
    }
    
    
    func textFieldShouldReturn(_ userText: UITextField) -> Bool
    {
        userText.resignFirstResponder()
        return true;
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchUsersArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionCell", for: indexPath as IndexPath) as! SuggestionCell
        
        hangUser  = searchUsersArray[indexPath.row] as! UserContactInfo
        if  hangUser.fullName != "" {
             cell.lblName.text = hangUser.fullName
        }
        else
        {
            cell.lblName.text = "N/A"

        
        }
       
        if hangUser.userName != "" {
            cell.lblUserName.text =  String(format:"@%@",hangUser.userName)
        }
        else
        {
         cell.lblUserName.text = "N/A"
        }
        
        
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
//        if let url  = NSURL(string: hangUser.imageUrl)
//        {
            let image = UIImage(named: "defaultUserImage")
            cell.imgUser.sd_setImage(with:  NSURL(string: hangUser.imageUrl) as! URL, placeholderImage: image)
        //}
        cell.btnStatus.tag  = hangUser.accountId
        cell.btnStatus.addTarget(self, action: #selector(SearchUserController.followButtonHandler(_:)), for: .touchUpInside)
        if hangUser.isFriend
        {
            cell.btnStatus.setTitle("Friend", for: .normal)
            cell.btnStatus.isUserInteractionEnabled = false
            
            
        }
        else if hangUser.followed_users_status
        {
         cell.btnStatus.setTitle("Following", for: .normal)
          cell.btnStatus.isUserInteractionEnabled = false
        }
        else
        {
            cell.btnStatus.setTitle("Add", for: .normal)
            cell.btnStatus.isUserInteractionEnabled = true
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        if hangUser.userType == "business"
        {
            
            let controller : OtherBusinessProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "otherBusinessAccount") as! OtherBusinessProfileVC
            hangUser  = searchUsersArray[indexPath.row] as! UserContactInfo
            controller.user_id = hangUser.userId

            controller.comingFrom = "searchuser"

            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            let controller : OthersAccountViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "otherAccount") as! OthersAccountViewCtrl
            hangUser  = searchUsersArray[indexPath.row] as! UserContactInfo
            controller.user_id = hangUser.userId
            controller.comingFrom = "searchuser"

             self.navigationController?.pushViewController(controller, animated: true)
        
        }
        
       
        
        
    }
    func followButtonHandler(_ sender: UIButton)
        
        
    {
        let accountId =  String(format:"%d",sender.tag )
        
        if sender.isSelected
        {
            sender.isSelected = false
        }
        else
        {
            sender.isSelected = true
            sender.setTitle("", for: .selected)
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.followUserWithUserId(id: accountId)
            
        }
        
        
        
        
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }

}
