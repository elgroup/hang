//
//  NearByMomentsVC.swift
//  HangApp
//
//  Created by Evan Luthra on 11/08/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class NearByMomentsVC: UIViewController,AppServiceDelegate {
    @IBOutlet var listView:UIView!
    @IBOutlet var gridView:UIView!
     var feedsArray: NSMutableArray = NSMutableArray()
     let ser:AppService = AppService()
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.1, execute:
            {
                self.getMomentsById(momentId: 0)

        })
            // Do any additional setup after loading the view.
    }
    func getMomentsById(momentId : Int)
    {
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        let strId = String(format:"%d", momentId )
        self.ser.getMomentDataById(id: strId)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func menuClicked(_ sender: UIButton)
    {
       
        if sender.isSelected
        {
            
            let vc =  self.childViewControllers[0]
            let nearFeeds:NearByFeedsVC = vc as! NearByFeedsVC
            nearFeeds.feedsArray = feedsArray
            
            UIView.transition(with: listView, duration: 1.0, options: .transitionFlipFromRight, animations: { _ in
                self.listView.isHidden = false
                self.gridView.isHidden = true
                
            }, completion: nil)
            nearFeeds.reloadTable()
            sender.isSelected = false
            
          
        }
        else
        {
            let vc =  self.childViewControllers[1]
            
            let gridVC:PhotoStreamViewController = vc as! PhotoStreamViewController
            gridVC.userMomentsArray = feedsArray
            
            UIView.transition(with: gridView, duration: 1.0, options: .transitionFlipFromLeft, animations: { _ in
                self.listView.isHidden = true
                self.gridView.isHidden = false
            }, completion: nil)
            
            gridVC.reloadGrid()
            
          
            sender.isSelected = true
            
          
        }
        
            
            
        
    }
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        Loader.sharedLoader.hideLoader()
        
        
        if response.statusCode == true
        {
            
            if(apiName == "getMomentDataById")
            {
                
                let profiledata = response as! UserContactInfo
                feedsArray =   profiledata.momentsArray
                let vc =  self.childViewControllers[0]
                
                let nearFeeds:NearByFeedsVC = vc as! NearByFeedsVC
                nearFeeds.feedsArray = feedsArray

                self.listView.isHidden = false
                self.gridView.isHidden = true
                 nearFeeds.reloadTable()
                
            }
           
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }


   

}
