//
//  TestViewController.swift
//  NFTopMenuController
//
//  Created by Niklas Fahl on 12/16/14.
//  Copyright (c) 2014 Niklas Fahl. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController ,AppServiceDelegate,CAPSPageMenuDelegate{

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblBio: UILabel!
     @IBOutlet var btnEdit: UIButton!
   let ser:AppService = AppService()
    var momentsArray:NSMutableArray = []
    var friendsArray:NSMutableArray = []
    var checkInsArray:NSMutableArray = []
    var checkInUserId: String = ""
    var boolEdit: Bool = true
    override func viewDidLoad() {
      
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
      var pageMenu : CAPSPageMenuNew?
     
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var badgeView: UIView!
    
    @IBOutlet weak var lblBadgeCount: UILabel!
    
    
    @IBAction func editProfileBtnClicked(_ sender: Any)
    {
        btnEdit.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let editProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "editProfile") as! EditProfileController
             self.btnEdit.isEnabled = true
            self.navigationController?.pushViewController(editProfileVC, animated: true)
        })

       
    }
    
   
    @IBAction func openMessages(_ sender: Any)
    {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let messageVC = mainStoryboard.instantiateViewController(withIdentifier: "messageList") as! MessageListViewCtrl
        
        self.navigationController?.pushViewController(messageVC, animated: true)

    }
    
    
    func open(scheme: URL) {
              
    }
    @IBAction func openFacebook(_ sender: Any)
    {
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "fb://profile/\("")", iTuneUrlString: "https://itunes.apple.com/in/app/facebook/id284882215?mt=8")
    }
    @IBAction func openTwitter(_ sender: Any)
    {
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "twitter://", iTuneUrlString: "https://itunes.apple.com/in/app/twitter/id409789998?mt=12")
      
    }
    @IBAction func openInstagram(_ sender: Any) {
        
         MiscUtils.sharedMiscUtils.openAppUrl(urlString: "instagram://user?username=johndoe", iTuneUrlString: "https://itunes.apple.com/in/app/instagram/id389801252?mt=8")
        
    }
    @IBAction func openSnapchat(_ sender: Any)
    {
        
        MiscUtils.sharedMiscUtils.openAppUrl(urlString: "snapchat://", iTuneUrlString: "https://itunes.apple.com/in/app/snapchat/id447188370?mt=8")
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        badgeView.isHidden = true
        callProfileService()
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
        imgUser.clipsToBounds = true;
        super.viewWillAppear(animated)
    }
    
     func layOutBottomVie()
     {
        var controllerArray : [UIViewController] = []
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 : PhotoStreamViewController = mainStoryboard.instantiateViewController(withIdentifier: "photoStream") as! PhotoStreamViewController
        
        controller1.title = "Moments"
        controller1.userMomentsArray = momentsArray
        controllerArray.append(controller1)
        
        
        let controller2 : FriendsViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "friends") as! FriendsViewCtrl
        controller2.userFriendsArray = friendsArray
        
        controller2.title = "Accounts"
        controllerArray.append(controller2)
        let controller3 : PlacesViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "places") as! PlacesViewCtrl
         controller3.comingFrom  = "profile"
        controller3.title = "Places"
        controller3.userId = checkInUserId
        controllerArray.append(controller3)
        
        // Customize menu (Optional)
        
        let navColor : UIColor =  UIColor(patternImage: UIImage(named: "navBg.png")!)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(navColor),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
            .menuHeight(40.0),
            .menuItemWidth(110.0),
            .centerMenuItems(false)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenuNew(viewControllers: controllerArray, frame: CGRect(x: 0.0, y:0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
         pageMenu?.delegate = self
        self.addChildViewController(pageMenu!)
        self.bottomView.addSubview(pageMenu!.view)
        
         pageMenu?.moveToPage(AppDelegate.getAppDelegate().profileCategoryID)
        pageMenu!.didMove(toParentViewController: self)
    
     }
    
    func callProfileService()
    {
       
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
       
        let strUserId = String(format:"%@",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
       
        self.ser.getProfileData(id: strUserId)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
      
        
        
        
       
        if response.statusCode == true
        {
            
            if(apiName == "getProfileData")
            {
                
                
                let profiledata = response as! UserContactInfo
                checkInUserId = String(profiledata.accountId)
                lblUserName.text = profiledata.fullName
                let data : NSData =  profiledata.bioGraphy.data(using: String.Encoding.utf8)! as NSData
                let biographyText = String(data: data as Data, encoding: String.Encoding.nonLossyASCII)
                lblBio.text = biographyText
                 lblUserName.text = profiledata.fullName
                lblBadgeCount.text  = String(format:"%d",profiledata.badgeNumber)
                momentsArray =   profiledata.momentsArray
                friendsArray = profiledata.friendsArray
                checkInsArray  =  profiledata.checkInArray
                UserDefaults.standard.set(lblUserName.text, forKey: "userFullName")
                UserDefaults.standard.set(profiledata.imageUrl, forKey: "userImage")
                if lblBadgeCount.text == "0" {
                    badgeView.isHidden = true
                }
                else
                {
                
                 badgeView.isHidden = false
                }
                lblLocation.text  = profiledata.location
               
               
                
                DispatchQueue.main.async
                    {
                        self.layOutBottomVie()
                        Loader.sharedLoader.hideLoader()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            if let url  = NSURL(string: profiledata.imageUrl),
                                let data = NSData(contentsOf: url as URL)
                            {
                                self.imgUser.image = UIImage(data: data as Data)
                                
                            }
                        })

                       }
                
                
                
                }
                
            }
           
            
            
        
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        layOutBottomVie()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    func didMoveToPage(_ controller: UIViewController, index: Int)
    {
        AppDelegate.getAppDelegate().profileCategoryID = index
        print(index)
        
    }
    
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
   

}
