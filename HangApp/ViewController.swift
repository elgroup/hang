//
//  ViewController.swift
//  HangApp
//
//  Created by Sujeet Singh on 22/02/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//


import UIKit
import AVKit
import AVFoundation
class ViewController: UIViewController,AppServiceDelegate {
    
    
    @IBOutlet var bgView: UIView!
     @IBOutlet var bgViewSinIn: UIView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var lblError: UILabel!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var btnRememberMe: UIButton!
     let rememberMeKey = "RememberMe"
     let userNameKey = "Username"
     let passwordKey = "Password"
     let ser:AppService = AppService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //bgView.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
        bgViewSinIn.layer.cornerRadius = 6;
        bgViewSinIn.layer.masksToBounds = true
        // lblError.isHidden = true
        
        //        GlobalInfo.hideTabBar(tabBarController: self.tabBarController!)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //UIApplication.shared.statusBarStyle = .lightContent
        
        // Do any additional setup after loading the view, typically from a nib.
        btnSignIn.layer.cornerRadius = 5;
        var isRememberMe :Bool = false
        if((UserDefaults.standard.value(forKey: rememberMeKey)) != nil){
        isRememberMe = UserDefaults.standard.value(forKey: rememberMeKey) as! Bool
       
        if(isRememberMe){
            if((UserDefaults.standard.value(forKey: userNameKey)) != nil){
                let userName :String = UserDefaults.standard.value(forKey: userNameKey) as! String
                txtEmail.text = userName
            }
            if((UserDefaults.standard.value(forKey: passwordKey)) != nil){
            let password :String = UserDefaults.standard.value(forKey: passwordKey) as! String
          
            txtPassword.text = password
            }
           
        }
        }
        
        btnRememberMe.isSelected = isRememberMe;
        self.setRememberMeButtonImage(isSelected: isRememberMe)
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpBtnClicked(_ sender: AnyObject)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signUpVC = mainStoryboard.instantiateViewController(withIdentifier: "SignUpCtrl") as! SignUpCtrl
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    
    @IBAction func forgotBtnClicked(_ sender: Any)
    {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let forgotVC = mainStoryboard.instantiateViewController(withIdentifier: "forgot") as! ForgotPasswordCtrl
        self.present(forgotVC, animated: true, completion: nil)
    }
    
    @IBAction func signInBtnClicked(_ sender: AnyObject)
    {
        
        if validateAndProceed(email: txtEmail.text!, password: txtPassword.text!)
        {
           callLoginService()
        }
        

    }
    
    @IBAction func rememberMe(sender: UIButton){
       
        btnRememberMe.isSelected = !btnRememberMe.isSelected

        if(btnRememberMe.isSelected){
            
            UserDefaults.standard.set(btnRememberMe.isSelected, forKey: rememberMeKey)
            UserDefaults.standard.set(txtEmail.text, forKey: userNameKey)
            UserDefaults.standard.set(txtPassword.text, forKey: passwordKey)
            
        }
        else{
            
            
            UserDefaults.standard.removeObject(forKey:rememberMeKey )
            UserDefaults.standard.removeObject(forKey:userNameKey )
            UserDefaults.standard.removeObject(forKey:passwordKey )
        }
        
        self.setRememberMeButtonImage(isSelected:btnRememberMe.isSelected)
    }
    
    func setRememberMeButtonImage(isSelected:Bool){
    
        btnRememberMe.setImage(UIImage.init(named: isSelected ?"CheckMark":"UncheckMark"), for:UIControlState.normal)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        self.view.endEditing(true)
        
        if response.statusCode == true
        {
            if(apiName == "sign_in")
            {
                goToDashBoard()
                UserDefaults.standard.set(true, forKey: kIsAppAlreadyLogin)
            }
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        
        if  response.statusDescription! == "You have to confirm your email address before continuing."
        {
           
            
            let alert = UIAlertController(title: "", message: response.statusDescription! , preferredStyle: UIAlertControllerStyle.alert)
            
            let continueAction = UIAlertAction(title: "Contiue", style: .default, handler: { (void) in
                self.callLoginService()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (void) in
                self.dismiss(animated: true, completion: nil)
                
            })
            
            alert.addAction(cancelAction)
            alert.addAction(continueAction)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
         MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
        }
       
    }
    func callLoginService()
    {
        let info:UserInfo = UserInfo()
        info.emailId = txtEmail.text! as NSString!
        info.password = txtPassword.text as NSString!
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.loginUserWithInfo(userInfo: info)
    }
   

    func validateAndProceed(email:String,password:String)->Bool{
      
        if email.characters.count <= 0
        {  // lblError.isHidden = false
           // lblError.text = "Please enter email address"
            MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString: "Please enter email address", viewcontroller: self)
            return false
        }
        else if !(Validator.sharedValidator.isValidEmail(testStr: email)){
           // lblError.isHidden = false
           // lblError.text = "Please enter valid email address"
           MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString: "Please enter valid email address", viewcontroller: self)
            return false
        }else if password.characters.count == 0{
           // lblError.isHidden = false
           // lblError.text = "Please enter password"
           MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString: "Please enter password", viewcontroller: self)
            return false
        }
       // lblError.isHidden = true
        return true
    }
    

    //MARK: Textfield delegates
    
    func textFieldShouldReturn(_ userText: UITextField) -> Bool
    {
        switch userText
        {
        case txtEmail:
            txtPassword.becomeFirstResponder()
        default:
            break
        }
        userText.resignFirstResponder()
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {

        switch textField
        {
            
        case txtPassword:
            if DeviceType.IS_IPHONE_4_OR_LESS
            {
                self.view.frame.origin.y = -100
            }
            else if DeviceType.IS_IPHONE_5
            {
                self.view.frame.origin.y = -40
            }
            else if DeviceType.IS_IPHONE_6
            {
                self.view.frame.origin.y = -20
            }
            
        default:
            print("")
        }
    }
    
    func isEmpty(text:String) -> Bool{
        if text.isEmpty == true{
            return true
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.frame.origin.y = 0
        switch textField
        {
       
        case txtEmail:
            if isEmpty(text: txtEmail.text!) == false
                
            {
                if !(Validator.sharedValidator.isValidEmail(testStr: txtEmail.text!))
                {
                    
                    MiscUtils.sharedMiscUtils.showWarningAlertWithMessage(messageString: "Please enter valid email address", viewcontroller: self)

                   // lblError.isHidden = false
                  // lblError.text = "Please enter valid email address"
                }
                else{
                
               // lblError.isHidden = true
                }
            }
            break
            
        default:
            break
        }
    }
    
      
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
   
    
    
    func goToDashBoard()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let dashboardVC = mainStoryboard.instantiateViewController(withIdentifier: "dashboard") as! DashBoardCtrl
        self.navigationController?.pushViewController(dashboardVC, animated: true)
        
        
    }
    
      
    
    
   
}

