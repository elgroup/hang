//
//  TagUsersVC.swift
//  HangApp
//
//  Created by Evan Luthra on 31/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class TagUsersVC:UIViewController ,UITableViewDataSource ,UITableViewDelegate ,AppServiceDelegate ,UITextFieldDelegate,UISearchBarDelegate,UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate {
    @IBOutlet weak var suggestionTable: UITableView!
    
    let ser:AppService = AppService()
    var tagUsersArray:NSMutableArray = []
    
    var hangUser = Tags()
    // var tagUser = Tags()
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func searchBtnClicked(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnPressed(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            
            if(apiName == "followUser")
            {
                
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: response.statusDescription!, viewcontroller: self)
            }
        }
    }
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
 
   
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagUsersArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionCell", for: indexPath as IndexPath) as! SuggestionCell
        
        hangUser  = tagUsersArray[indexPath.row] as! Tags
        if  hangUser.tagUser_Name != "" {
            cell.lblName.text = hangUser.tagUser_Name
        }
        
       
        
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        //        if let url  = NSURL(string: hangUser.imageUrl)
        //        {
        let image = UIImage(named: "defaultUserImage")
        cell.imgUser.sd_setImage(with:  NSURL(string: hangUser.tagUserImageUrl) as! URL, placeholderImage: image)
        //}
        cell.btnStatus.tag  = hangUser.tagUser_id
        cell.btnStatus.addTarget(self, action: #selector(SearchUserController.followButtonHandler(_:)), for: .touchUpInside)
        if hangUser.isFriend
{
            cell.btnStatus.setTitle("Friend", for: .normal)
            cell.btnStatus.isUserInteractionEnabled = false
            
            
        }
        else if hangUser.followed_users_status
        {
        cell.btnStatus.setTitle("Following", for: .normal)
            cell.btnStatus.isUserInteractionEnabled = false
        }
        else
        {
            cell.btnStatus.setTitle("Add", for: .normal)
            cell.btnStatus.isUserInteractionEnabled = true
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = mainStoryboard.instantiateViewController(withIdentifier: "popUp") as! PopUpViewCtrl
        popupVC.messageReceiver_id = String(hangUser.tagUser_id)
        popupVC.messageReceiver_name = hangUser.tagUser_Name
        
        popupVC.modalPresentationStyle = .overCurrentContext
        popupVC.modalTransitionStyle = .crossDissolve
        popupVC.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        let pVC = popupVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = self.view
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        
        self.present(popupVC, animated: true, completion: nil)

        
        
        
    }
    func followButtonHandler(_ sender: UIButton)
        
        
    {
        let accountId =  String(format:"%d",sender.tag )
        
        if sender.isSelected
        {
            sender.isSelected = false
        }
        else
        {
            sender.isSelected = true
            sender.setTitle("", for: .selected)
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.followUserWithUserId(id: accountId)
            
        }
        
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
}
