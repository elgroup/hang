//
//  TestTableViewController.swift
//  NFTopMenuController
//
//  Created by Niklas Fahl on 12/17/14.
//  Copyright (c) 2014 Niklas Fahl. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController , UITableViewDataSource ,UITableViewDelegate ,AppServiceDelegate{
    

    @IBOutlet weak var tblNotification: UITableView!
    var follwedArray:NSMutableArray = []
    var followerArray:NSMutableArray = []
    var tagsArray:NSMutableArray = []
    var messageArray:NSMutableArray = []
    // var follower = UserFollowers()
     var tag = Tags()
     //var followed = Followed_Users()
     var message = Messages()
    let ser:AppService = AppService()
    override func viewDidLoad() {
        super.viewDidLoad()
       

    }
    override func viewWillAppear(_ animated: Bool) {
         callNotificationService()
    }
    
    func callNotificationService()
    {
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.getNotifications()
  
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        self.view.endEditing(true)
        DispatchQueue.main.async
            {

        Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
           
            if(apiName == "getNotifications")
            {
                
              followerArray = (response as! Notifications).folowers
              follwedArray  =  (response as! Notifications).followedUsers
              messageArray = (response as! Notifications).messages
              tagsArray  =  (response as! Notifications).tags
                tblNotification.reloadData()
            }
            if(apiName == "acceptFollowRequestUserId")
            {
                callNotificationService()
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: response.statusDescription!, viewcontroller: self)
                
            }
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if section == 0
        {
           rowCount = follwedArray.count
        }
        else if section == 1
        {
            rowCount = messageArray.count
        }
        else if section == 2
        {
             rowCount = tagsArray.count
        }
        else
        {
             rowCount = followerArray.count
        }
        
        return rowCount
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! NotificationCell
        
        let image = UIImage(named: "defaultUserImage")
        if indexPath.section == 0
        {
           let followed  = follwedArray[indexPath.row] as! UserFollowers
            
            let strNotification = String(format:"%@\nAdd you as friend", followed.fullName )
            let results = [followed.fullName]
            cell.txtDesc.attributedText = createClickableLink(userNames: results , checkInDescr: cell.txtDesc, main_string: strNotification)
            
           
            cell.imgUser.sd_setImage(with:  NSURL(string: followed.imageUrl) as! URL, placeholderImage: image)
            cell.btnStatus.tag  = followed.id
            cell.btnStatus.addTarget(self, action: #selector(NotificationViewController.followButtonHandler(_:)), for: .touchUpInside)
            cell.imgStatus.image  = UIImage(named: "Check.png")
        }
        if indexPath.section == 1
        {
            message  = messageArray[indexPath.row] as! Messages
            
            let strNotification = String(format:"%@\n%@", message.sender_name,message.body )
            let results = [message.sender_name]
            cell.txtDesc.attributedText = createClickableLink(userNames: results as! [String], checkInDescr: cell.txtDesc, main_string: strNotification)
            
       
            if let url  = NSURL(string: message.imageUrl)
            {
            cell.imgUser.sd_setImage(with: url as URL, placeholderImage: image)
            }
           
        }
        if indexPath.section == 2
        {
           let tag  = tagsArray[indexPath.row] as! Tags
            
            let results = [tag.tagByUser_Name]
            let strNotification = String(format:"%@ tag you", tag.tagByUser_Name)

            cell.txtDesc.attributedText = createClickableLink(userNames: results as! [String], checkInDescr: cell.txtDesc, main_string: strNotification)
        
            if let url  = NSURL(string: tag.tagUserImageUrl)
                {
            cell.imgUser.sd_setImage(with: url as URL, placeholderImage: image)
                
            }
           
           
        }
       
        if indexPath.section == 3
        {
           let follower  = followerArray[indexPath.row] as! UserFollowers
            
            let strNotification = String(format:"%@\nAccepted your friend requested", follower.fullName )
            let results = [follower.fullName]
            cell.txtDesc.attributedText = createClickableLink(userNames: results, checkInDescr: cell.txtDesc, main_string: strNotification)
            
            if let url  = NSURL(string: follower.imageUrl)
            {
                
                cell.imgUser.sd_setImage(with: url as URL, placeholderImage: image)
            }
            
        }

       
       
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    
    func createClickableLink(userNames : [String] , checkInDescr:UITextView , main_string:String ) -> NSMutableAttributedString
    {
        
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        for var i in (0..<userNames.count).reversed()
        {
            
            
            
            let range = (main_string as NSString).range(of: userNames[i] as String)
            
            let replaced = String(userNames[i].characters.filter {$0 != " "})
            //let trimmedString = userNames[i].trimmingCharacters(in: .whitespaces)
            attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (checkInDescr.font?.fontName)!, size: 15) as Any , range: range)
            attribute.addAttribute(NSLinkAttributeName, value:replaced, range: range)
            checkInDescr.linkTextAttributes = [ NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.86, green: 0.00, blue: 0.43, alpha: 1.00)]
            
            
            
        }
        let range1 = (main_string as NSString).range(of: "Accepted your friend requested")
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.lightGray , range: range1)
        return attribute
    }
    func followButtonHandler(_ sender: UIButton)
        
        
    {
            let accountId =  String(format:"%d",sender.tag )
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.acceptFollowRequestUserId(id: accountId)
            
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        
    }
    func openAction()
    {
        
    }
    
}
