//
//  CommentListVC.swift
//  HangApp
//
//  Created by Evan Luthra on 22/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class CommentListVC:UIViewController,UITableViewDataSource,UITableViewDelegate ,AppServiceDelegate, FloatRatingViewDelegate ,UITextViewDelegate {
    var moment_id = Int()
    var user_id = Int()
    var apiname = String()
    let ser:AppService = AppService()
    var comment = Comment()
    var reviewComment:String!
    var ratingCount:String!
    @IBOutlet weak var reviewTable: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtComment: UITextView!
     @IBOutlet weak var bgView: UIView!
    var userCommentArray:NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.layer.cornerRadius = 6;
        bgView.layer.masksToBounds = true
        
        btnSend.layer.cornerRadius = btnSend.frame.size.width / 2;
        btnSend.clipsToBounds = true;
        
        txtComment.becomeFirstResponder()
        reviewTable.estimatedRowHeight = 80
        reviewTable.rowHeight = UITableViewAutomaticDimension
        
        reviewTable.setNeedsLayout()
        reviewTable.layoutIfNeeded()
        
        reviewTable.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        reviewComment = ""
        
    }
    
    func callPostCommentService()
    {
        let comment:Comment = Comment()
        
        let messageData = reviewComment.data(using: String.Encoding.nonLossyASCII)
        let finalMessage = String(data: messageData!, encoding: String.Encoding.utf8)
        comment.commentDesc = finalMessage
        comment.moment_id = moment_id
        comment.user_id = user_id
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        if apiname == "/comments"
        {
        self.ser.postCommentMoment(comment:comment)
        }
        else
        {
         self.ser.postCommentPlace(comment: comment, apiName: apiname)
        
        }
    }
    
    
    func getCommentService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        self.ser.getCommentsById(id: String(moment_id))
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        Loader.sharedLoader.hideLoader()
        
        if response.statusCode == true
        {
            
            if(apiName == "postComment")
            {
                MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Success", messageString: response.statusDescription!, viewcontroller: self)
                txtComment.text = ""
                if apiname == "/comments"
                    {
                        
                     self.getCommentService()
                    }
                else
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                        self.closeBtnClicked(self)
                    })
                 //txtComment.resignFirstResponder()
                
                }
               
                
                //reviewTable.reloadData()
            }
            if(apiName == "getCommentsById")
            {
                
                let profiledata = response as! UserContactInfo
                userCommentArray = profiledata.commentsArray
                
                reviewTable.reloadData()
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userCommentArray.count
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 80.0
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath as IndexPath) as! ReviewCell
        
        comment  = userCommentArray[indexPath.row] as! Comment
       
        let tap = UITapGestureRecognizer()
        
        tap.addTarget(self, action: #selector(ReviewsViewController.tap(sender:)))
        tap.numberOfTapsRequired = 1
        cell.addGestureRecognizer(tap)
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        let image = UIImage(named: "defaultUserImage")
        cell.imgUser.sd_setImage(with: NSURL(string: comment.imageUrl) as! URL, placeholderImage: image)
        let data : NSData = comment.commentDesc.data(using: String.Encoding.utf8)! as NSData
        let commentText = String(data: data as Data, encoding: String.Encoding.nonLossyASCII)
        
        cell.lblName.attributedText = self.createAttributedString(userName: comment.name, commentLabel:  cell.lblName, date_string: MiscUtils.sharedMiscUtils.getDurationFromCurrentDate(dateString: comment.commentDate), comment_string: commentText!)
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    func createAttributedString(userName : String , commentLabel:UILabel , date_string:String ,comment_string:String) -> NSMutableAttributedString
    {
        
         let  main_string = String(format:"%@ %@\n\n%@",comment.name,comment_string,MiscUtils.sharedMiscUtils.getDurationFromCurrentDate(dateString: comment.commentDate) )
        let attribute = NSMutableAttributedString.init(string: main_string)
        
            
            
            let range = (main_string as NSString).range(of: userName)
        
            
            attribute.addAttribute(NSFontAttributeName, value:UIFont(name: (commentLabel.font?.fontName)!, size: 17) as Any , range: range)
         attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.black , range: range)
        

            

        let range1 = (main_string as NSString).range(of: comment_string)
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range1)
        let range2 = (main_string as NSString).range(of: date_string)
        
        attribute.addAttribute(NSForegroundColorAttributeName, value:UIColor.darkGray , range: range2)
        
        return attribute
    }

    
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Write comment") {
            textView.text  = ""
            textView.textColor = UIColor.darkGray
            
            
        }
        
        
        if DeviceType.IS_IPHONE_5
        {
            self.view.frame.origin.y = -246
        }
        else if DeviceType.IS_IPHONE_6
        {
            self.view.frame.origin.y = -246
        }
        else if DeviceType.IS_IPHONE_6P
        {
            self.view.frame.origin.y = -256
        }
        
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write comment"
            textView.textColor = UIColor.lightGray
        }
       self.view.frame.origin.y = 0
    }
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        reviewComment = textView.text
        
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @IBAction func postBtnClicked(_ sender: Any)
    {
        self.view.endEditing(true)
        
        callPostCommentService()
    }
    func tap(sender: UITapGestureRecognizer )
    {
        self.view.endEditing(true)
        
    }
    
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float)
    {
        ratingCount =  String(rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
    {
        
    }
    
    
    
    
}
