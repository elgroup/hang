//
//  Followed_Users.swift
//  HangApp
//
//  Created by Evan Luthra on 10/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class Followed_Users: NSObject
{
    var is_followed:Bool!
    var name:String!
    var id = Int()
    var followed_id = Int()
    var user_id = Int()
}
