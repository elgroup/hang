//
//  ShareViewController.swift
//  HangApp
//
//  Created by Evan Luthra on 18/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class ShareViewController: UIViewController, UIScrollViewDelegate , UICollectionViewDelegate,UICollectionViewDataSource,AppServiceDelegate{
    @IBOutlet var locationName:UILabel!
    @IBOutlet var imgMoment: UIImageView!
    @IBOutlet var btnPlay: UIButton!
    var imgThumnailShare = UIImage()
    @IBOutlet var txtComment: UITextView!
     var fileType = String()
    let ser:AppService = AppService()
      var lat = Double()
     var long = Double()
    var selectedIndex = Int()
     var selectedCategoryId = Int()
    var categoryListArray = NSMutableArray()
    var bannerImage = BannerImages()
    var urlFile = String()
    var videoURLToShare:NSURL?
   var tagPersons = NSMutableArray()

    @IBOutlet var categoryCollectionView: UICollectionView!
    var imageForShare : UIImage!
     var currentPlace = String()
    var selectedCategory = Int()
    override func viewDidLoad() {
        
        selectedIndex = -1
        selectedCategoryId = -1
        if fileType == ".mov"
        {
           imgMoment.image = imgThumnailShare
            btnPlay.isHidden = false
        }
        else{
        
        imgMoment.image = imageForShare
        }
        
        categoryListArray = AppDelegate.getAppDelegate().bannerImagesArray
        locationName.addTapGesture(tapNumber: 1, target: self, action: #selector(ShareViewController.handleTap(gestureRecognizer:)))
       getCurrentLocation()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func playButtonClicked(_ sender: Any)
    {
        playVideo(videoUrl: videoURLToShare as! URL)
    }
   public  func playVideo( videoUrl : URL) {
        
        let player = AVPlayer(url: videoUrl)
      NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        searchLocation()
    }
    func getCurrentLocation()
    {
        
               self.lat = (AppDelegate.getAppDelegate().coordinates?.latitude)! as Double
               self.long = (AppDelegate.getAppDelegate().coordinates?.longitude)! as Double
                
                GlobalInfo.sharedInstance.getCityNameManually(handler: { (countryName) -> Void in
                    self.locationName.text = countryName
                    
                    
                })
                
     }
    @IBAction func shareBtnPressed(_ sender: Any)
    {
        
        
         var caption = self.txtComment.text as String!
     caption =  caption?.replacingOccurrences(of: "\n", with: " ")
        let delayTime : Float = 0.5
        
        
        if caption == "Write Caption"
        {
            caption = ""
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + TimeInterval(delayTime), execute: {
           
        let info:MomentsInfo = MomentsInfo()
        
        let userIdArray = NSMutableArray()
        
        for person  in self.tagPersons
        {
            let strId =  String(format:"%d", (person as! UserContactInfo).userId )
            userIdArray.add(strId)
        }
        
        info.categoryId = self.selectedCategoryId
        if self.fileType != ".mov"
        {
         info.fileType = ".png"
          info.imagedata = UIImagePNGRepresentation(self.imageForShare)! as Data?
        }
        else{
            let momentVideoData = NSData(contentsOf: self.videoURLToShare as! URL )
            info.videodata = momentVideoData as Data!
            info.fileType = self.fileType as String!
        
        }
       
        
            
    
    
        info.caption = caption
        info.loc_lat = self.lat
        info.loc_long = self.long
        info.tag_Users = userIdArray
       
        if self.selectedCategoryId != -1
        {
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            self.ser.delegate = self;
            self.ser.postMomentData(momentInfo: info)
            
        }
        else
        {
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: "Please select moment category", viewcontroller: self)
        }
            
        })

       
    }
    
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
       
        
        
        if response.statusCode == true
        {
            if(apiName == "postMoment")
            {
                DispatchQueue.main.async()
                    {
                        Loader.sharedLoader.hideLoader()
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: CameraViewController.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                }
                
                
            }
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        
       
            MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
        
        
    }
    func searchLocation()
        
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle: nil)
        let selectionLocationViewController = mainStoryboard.instantiateViewController(withIdentifier: "SelectionLocationViewController") as! SelectionLocationViewController
        selectionLocationViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        let nav = UINavigationController.init(rootViewController: selectionLocationViewController)
        self.navigationController?.present(nav, animated: true, completion: nil)
    
        selectionLocationViewController.selectedCellInfo = {(placeInfo:CityInfo) -> Void in
            
                        self.locationName.text = placeInfo.cityName
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Write Caption") {
            textView.text  = ""
            textView.textColor = UIColor.darkGray
            
            
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (textView.text == "") {
            textView.text  = "Write Caption"
            textView.textColor = UIColor.lightGray
            
            
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func selectButtonHandler(_ sender: UIButton)
        
        
    {
          self.view.endEditing(true)      
        if sender.isSelected
        {
            sender.isSelected = false
            
        }
        else
        {
            sender.isSelected = true
            
        }
        selectedIndex  = sender.tag
        
         bannerImage  = categoryListArray[selectedIndex] as! BannerImages
        
        let catId = String(bannerImage.bannerId)
        
        selectedCategoryId = Int(catId)!
        categoryCollectionView.reloadData()
        
        
    }
    //MARK:collection view delegate
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return categoryListArray.count;
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        bannerImage  = categoryListArray[indexPath.row] as! BannerImages
        let image = UIImage(named: "heng")
        
        cell.categoryImageView.sd_setImage(with: NSURL(string: bannerImage.iconUrl) as! URL, placeholderImage: image)
        
        cell.categoryLabel.text = bannerImage.title
        cell.categoryImageView.layer.cornerRadius = cell.categoryImageView.frame.width/2
        cell.categoryImageView.layer.masksToBounds = true
        let tap = UITapGestureRecognizer()
        
        tap.addTarget(self, action: #selector(ReviewsViewController.tap(sender:)))
        tap.numberOfTapsRequired = 1
        cell.addGestureRecognizer(tap)
        if selectedIndex == indexPath.row {
              cell.btnSelect.isSelected = true
        }
        else
        {
        
         cell.btnSelect.isSelected = false
        }
       
        cell.btnSelect.tag  = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(InviteFriendsCtrl.selectButtonHandler(_:)), for: .touchUpInside)
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width:UIScreen.main.bounds.size.width/2-5, height:100)
    }
    
    func collectionView(_collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 1;
    }
    
    func collectionView(_collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1;
    }
    
    func selectItemAtIndexPath(indexPath: NSIndexPath?, animated: Bool, scrollPosition: UICollectionViewScrollPosition)
    {
        
    }

    @IBAction func backPressed(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func tap(sender: UITapGestureRecognizer )
    {
        self.view.endEditing(true)
        
    }
  

}
extension UIView {
    
    func addTapGesture(tapNumber : Int, target: Any , action : Selector) {
        
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
        
    }
}
