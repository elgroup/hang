//
//  FriendCell.swift
//  HangApp
//
//  Created by Evan Luthra on 28/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnSelect: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
