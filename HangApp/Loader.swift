//
//  Loader.swift
//  BluLint
//
//  Created by hament miglani on 24/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class Loader : NSObject
{
    static let sharedLoader = Loader()
    let view:UIView
    override init() {
        view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        let spinnerView = SpinnerView()
        spinnerView.frame = CGRect(x:UIScreen.main.bounds.size.width/2 - 35, y:UIScreen.main.bounds.size.height/2 - 35, width:70, height:70)
        view.addSubview(spinnerView)

    }
    
    func showLoaderOnScreen(vc:UIViewController)
    {
     
        AppDelegate.getAppDelegate().window?.addSubview(view)
    }
    
    func hideLoader()
    {
        view.removeFromSuperview()
    }

    
}
