//
//  MessageListViewCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 04/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class MessageListViewCtrl: UIViewController,UITableViewDataSource,UITableViewDelegate, AppServiceDelegate
{
    
    @IBOutlet weak var tblMessages: UITableView!
       let ser:AppService = AppService()
    var messagesArray:NSMutableArray = []
  
    @IBOutlet weak var tblMessage: UITableView!
    override func viewDidLoad() {
        
        
        callMessagesService()
        tblMessages.estimatedRowHeight = 100
        tblMessages.rowHeight = UITableViewAutomaticDimension
        
        tblMessages.setNeedsLayout()
        tblMessages.layoutIfNeeded()
        
       tblMessages.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
                super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! MessageCell
       let  messages  = messagesArray[indexPath.row] as! Messages
        
        cell.lblName.text = messages.sender_name
        cell.lblMessage.text = messages.body
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
        cell.imgUser.clipsToBounds = true;
        if let url  = NSURL(string: messages.imageUrl),
            let data = NSData(contentsOf: url as URL)
        {
            cell.imgUser.image = UIImage(data: data as Data)
        }
        cell.lblMessageDate.text = MiscUtils.sharedMiscUtils.getDurationFromCurrentDate(dateString: messages.message_date)
        
        return cell
    }
    @IBAction func backPressed(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func callMessagesService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        
        let strUserId = String(format:"%@",UserDefaults.standard.value(forKey: "userId") as! CVarArg)
        
        self.ser.getMessageById(id: strUserId)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
        
        Loader.sharedLoader.hideLoader()

        
        
        if response.statusCode == true
        {
            
            if(apiName == "getMessageById")
            {
                
                let profiledata = response as! UserContactInfo
               
                messagesArray =   profiledata.messagesArray
               tblMessages.reloadData()
                
                
            }
            
    }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
       
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    

}
