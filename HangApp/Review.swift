//
//  Review.swift
//  HangApp
//
//  Created by Evan Luthra on 17/05/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class Review: Response
{
    var imageUrl:String!
    var reviewDate:String!
   var name:String!
    var reviewDesc:String!
    var reviewId = Int()
    var ratingCount :String!
    var user_id = Int()
}
