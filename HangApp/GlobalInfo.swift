//
//  GlobalInfo.swift
//  BluLint
//
//  Created by hament miglani on 21/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit
import ContactsUI
import CoreTelephony
import CoreLocation
import AudioToolbox
import CoreData
import KDCircularProgress


@objc protocol GlobalInfoDelegate
{
    func progressViewCompleted()
    
    
}

class GlobalInfo: NSObject {
    
//     Mark: Show or Hide Tab Bar
    static let sharedInstance = GlobalInfo() // for making singlton
    
    var delegate:GlobalInfoDelegate?
    var pageMenu : CAPSPageMenu?
          var contactsArray : Array = [NSMutableDictionary]()
    var contactDictionaryAccordingToContacts = NSMutableDictionary()
//    var tabController:TabController?
    var customView:UIView?
    var backButton:UIButton?
    var voteButton:UIButton?
    var chatButtonView:UIView?
    var progress: KDCircularProgress!
    let countryCodes : [String] = ["972", "IL",
                                   "93" , "AF",
                                   "355", "AL",
                                   "213", "DZ",
                                   "1"  , "AS",
                                   "376", "AD",
                                   "244", "AO",
                                   "1"  , "AI",
                                   "1"  , "AG",
                                   "54" , "AR",
                                   "374", "AM",
                                   "297", "AW",
                                   "61" , "AU",
                                   "43" , "AT",
                                   "994", "AZ",
                                   "1"  , "BS",
                                   "973", "BH",
                                   "880", "BD",
                                   "1"  , "BB",
                                   "375", "BY",
                                   "32" , "BE",
                                   "501", "BZ",
                                   "229", "BJ",
                                   "1"  , "BM",
                                   "975", "BT",
                                   "387", "BA",
                                   "267", "BW",
                                   "55" , "BR",
                                   "246", "IO",
                                   "359", "BG",
                                   "226", "BF",
                                   "257", "BI",
                                   "855", "KH",
                                   "237", "CM",
                                   "1"  , "CA",
                                   "238", "CV",
                                   "345", "KY",
                                   "236", "CF",
                                   "235", "TD",
                                   "56", "CL",
                                   "86", "CN",
                                   "61", "CX",
                                   "57", "CO",
                                   "269", "KM",
                                   "242", "CG",
                                   "682", "CK",
                                   "506", "CR",
                                   "385", "HR",
                                   "53" , "CU" ,
                                   "537", "CY",
                                   "420", "CZ",
                                   "45" , "DK" ,
                                   "253", "DJ",
                                   "1"  , "DM",
                                   "1"  , "DO",
                                   "593", "EC",
                                   "20" , "EG" ,
                                   "503", "SV",
                                   "240", "GQ",
                                   "291", "ER",
                                   "372", "EE",
                                   "251", "ET",
                                   "298", "FO",
                                   "679", "FJ",
                                   "358", "FI",
                                   "33" , "FR",
                                   "594", "GF",
                                   "689", "PF",
                                   "241", "GA",
                                   "220", "GM",
                                   "995", "GE",
                                   "49" , "DE",
                                   "233", "GH",
                                   "350", "GI",
                                   "30" , "GR",
                                   "299", "GL",
                                   "1"  , "GD",
                                   "590", "GP",
                                   "1"  , "GU",
                                   "502", "GT",
                                   "224", "GN",
                                   "245", "GW",
                                   "595", "GY",
                                   "509", "HT",
                                   "504", "HN",
                                   "36" , "HU",
                                   "354", "IS",
                                   "91" , "IN",
                                   "62" , "ID",
                                   "964", "IQ",
                                   "353", "IE",
                                   "972", "IL",
                                   "39" , "IT",
                                   "1"  , "JM",
                                   "81", "JP", "962", "JO", "77", "KZ",
                                   "254", "KE", "686", "KI", "965", "KW", "996", "KG",
                                   "371", "LV", "961", "LB", "266", "LS", "231", "LR",
                                   "423", "LI", "370", "LT", "352", "LU", "261", "MG",
                                   "265", "MW", "60", "MY", "960", "MV", "223", "ML",
                                   "356", "MT", "692", "MH", "596", "MQ", "222", "MR",
                                   "230", "MU", "262", "YT", "52","MX", "377", "MC",
                                   "976", "MN", "382", "ME", "1", "MS", "212", "MA",
                                   "95", "MM", "264", "NA", "674", "NR", "977", "NP",
                                   "31", "NL", "599", "AN", "687", "NC", "64", "NZ",
                                   "505", "NI", "227", "NE", "234", "NG", "683", "NU",
                                   "672", "NF", "1", "MP", "47", "NO", "968", "OM",
                                   "92", "PK", "680", "PW", "507", "PA", "675", "PG",
                                   "595", "PY", "51", "PE", "63", "PH", "48", "PL",
                                   "351", "PT", "1", "PR", "974", "QA", "40", "RO",
                                   "250", "RW", "685", "WS", "378", "SM", "966", "SA",
                                   "221", "SN", "381", "RS", "248", "SC", "232", "SL",
                                   "65", "SG", "421", "SK", "386", "SI", "677", "SB",
                                   "27", "ZA", "500", "GS", "34", "ES", "94", "LK",
                                   "249", "SD", "597", "SR", "268", "SZ", "46", "SE",
                                   "41", "CH", "992", "TJ", "66", "TH", "228", "TG",
                                   "690", "TK", "676", "TO", "1", "TT", "216", "TN",
                                   "90", "TR", "993", "TM", "1", "TC", "688", "TV",
                                   "256", "UG", "380", "UA", "971", "AE", "44", "GB",
                                   "1", "US", "598", "UY", "998", "UZ", "678", "VU",
                                   "681", "WF", "967", "YE", "260", "ZM", "263", "ZW",
                                   "591", "BO", "673", "BN", "61", "CC", "243", "CD",
                                   "225", "CI", "500", "FK", "44", "GG", "379", "VA",
                                   "852", "HK", "98", "IR", "44", "IM", "44", "JE",
                                   "850", "KP", "82", "KR", "856", "LA", "218", "LY",
                                   "853", "MO", "389", "MK", "691", "FM", "373", "MD",
                                   "258", "MZ", "970", "PS", "872", "PN", "262", "RE",
                                   "7", "RU", "590", "BL", "290", "SH", "1", "KN",
                                   "1", "LC", "590", "MF", "508", "PM", "1", "VC",
                                   "239", "ST", "252", "SO", "47", "SJ",
                                   "963","SY",
                                   "886",
                                   "TW", "255",
                                   "TZ", "670",
                                   "TL","58",
                                   "VE","84",
                                   "VN",
                                   "284", "VG",
                                   "340", "VI",
                                   "678","VU",
                                   "681","WF",
                                   "685","WS",
                                   "967","YE",
                                   "262","YT",
                                   "27","ZA",
                                   "260","ZM",
                                   "263","ZW"]
    
    var countryCodeListDict:NSDictionary? = nil
    
   func getContacts() -> [NSString]
    {
        let store = CNContactStore()
        let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .phoneticFullName),
            CNContactEmailAddressesKey] as [Any]
        
        let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch as! [CNKeyDescriptor])
        var cont = [NSString]()
        do {
            try store.enumerateContacts(with: fetchRequest, usingBlock: { ( contact, stop) -> Void in
                let cn:CNContact = contact
                if(cn.emailAddresses.count > 1)
                {
                    for contactEmail in cn.emailAddresses
                    {
                        let contactDict = NSMutableDictionary()
                        contactDict.setObject(String(format: "%@ %@",cn.givenName,cn.familyName), forKey: kContactName as NSCopying)
                        contactDict.setObject((contactEmail.value ), forKey: kEmail as NSCopying)
                        cont.append(contactEmail.value)
                    }
                }
                else
                {
                    if !cn.emailAddresses.isEmpty
                    {
                        let contactDict = NSMutableDictionary()
                        contactDict.setObject(String(format: "%@ %@",cn.givenName,cn.familyName), forKey: kContactName as NSCopying)
                        contactDict.setObject((cn.emailAddresses[0].value ), forKey: kEmail as NSCopying)
                        cont.append(cn.emailAddresses[0].value)
                    }
                }
            })
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        return cont
    }


    static func showTabBar(tabBarController:UITabBarController)
    {
        tabBarController.tabBar.isHidden = false
    }
    
    static func hideTabBar(tabBarController:UITabBarController)
    {
        tabBarController.tabBar.isHidden =  true
    }
    
    class func netWorkNotAvailable()
    {
        Loader.sharedLoader.hideLoader()
        let alert = UIAlertView()
        alert.title = ""
        alert.message = MiscUtils.sharedMiscUtils.getStringResource(key: "No internet connection")   // "Problem with network. Please try again"
        alert.addButton(withTitle: "OK")
        alert.show()
    }
    
   
    
    
    
    
    func archieveObject(object:AnyObject) -> NSData
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        return data as NSData
    }
    
    func unarchieveObject(data : NSData) -> AnyObject
    {
        return NSKeyedUnarchiver.unarchiveObject(with: data as Data)! as AnyObject
    }
    
    
    // MARK:Dictionary From Contacts Array
//    func makeDictionaryFromArray(contactsArray:[NonReefUserContactInfo]) ->(contactDictionary:NSMutableDictionary,contactKeys:NSMutableArray)
//    {
//        let contactDictionary = NSMutableDictionary()
//        let contactKeys = NSMutableArray()
//        for contact in contactsArray
//        {
//            var name:NSString = contact.contactName.trimming() as NSString
////            name.stringByTrimming()
//            if(name.length == 0)
//            {
//                name = "#"
//            }
//            
//            var array = contactDictionary.object(forKey: name.substring(to: 1).uppercased())
//            if (array == nil)
//            {
//                array = NSMutableArray()
//                contactDictionary.setObject(array!, forKey: name.substring(to: 1).uppercased() as NSCopying)
//                contactKeys.add(name.substring(to: 1).uppercased())
//            }
////            array
//            (array as! NSMutableArray).add(contact)
//        }
//        return(contactDictionary,contactKeys)
//    }
//    
    
    
    
    
    
    
    
    
    
   static func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF ) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
   static func convertImageToBase64(image:UIImage) -> String
    {
        //Now use image to create into NSData format
//        let imageData = UIImagePNGRepresentation(image)
       let imageData = UIImageJPEGRepresentation(image, 0.5)
         let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
//        let base64String = imageData!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        return base64String
        
    }
    
    func getDataFromUrl(url:NSURL, completion: @escaping ((_ data: NSData?, _ response: URLResponse?, _ error: NSError? ) -> Void)) {
        URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
            completion(data as NSData?, response, error as NSError?)
            }.resume()
    }
    
    func downloadImage(url: NSURL,completionHandler : @escaping ((_ isResponse : Bool,_ image:UIImage) -> Void))
    {
        print("Download Started")
        print("lastPathComponent: " + (url.lastPathComponent ?? ""))
        getDataFromUrl(url: url) { (data, response, error)  in
            //dispatch_get_main_queue().asynchronously()
            DispatchQueue.main.async
                { () -> Void in
                guard let data = data , error == nil else { return }
                print(response?.suggestedFilename ?? "")
                print("Download Finished")
              completionHandler(true,UIImage(data: data as Data)!)
            }
        }
    }
    func getCityNameManually( handler:@escaping (_ countryName:String?)-> Void){
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: (AppDelegate.getAppDelegate().coordinates?.latitude)! , longitude: (AppDelegate.getAppDelegate().coordinates?.longitude)!), completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                else{
                    let pm = placemarks! as [CLPlacemark]
                    if pm.count > 0 {
                        handler(pm[0].locality!)
                        //self.currentLocation = pm[0].locality!
                    }
                }
        })
    }
    
    
    func hideNavigation()
    {
//        let navC =   AppDelegate.getAppDelegate().window?.rootViewController as! UINavigationController
//        navC.navigationBar.isHidden = true
    }
    
    func showNavigation()
    {
        let navC =   AppDelegate.getAppDelegate().window?.rootViewController as! UINavigationController
        navC.navigationBar.isHidden = false
    }
   
    
    
    

    func sendToRoot()
    {
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle: nil)
//        let multipleLogin = mainStoryboard.instantiateViewController(withIdentifier: "MultipleLogin") as! MultipleLogin
//        let navControl = UINavigationController(rootViewController: multipleLogin)
//        AppDelegate.getAppDelegate().window?.rootViewController = navControl
//        getDeviceToken()
    }
    
    
   
    
    func getCountryDialingCodeFromCountryName(countryName:String) -> String
    {
        if countryCodeListDict == nil
        {
            var keys = [String]()
            var values = [String]()
            let whitespace = NSCharacterSet.decimalDigits
            
            for i in countryCodes {
//                i.rangeOfCharacter(from: whitespace)
                if  (i.rangeOfCharacter(from: whitespace) != nil) {
                    values.append(i)
                }
                else {
                    keys.append(i)
                }
            }
            countryCodeListDict = NSDictionary(objects: values as [String], forKeys: keys as [String] as [NSCopying])
        }
//        countryCodeListDict?.object(forKey: countryName)
        if let _: AnyObject = countryCodeListDict?.object(forKey: countryName) as AnyObject?{//countryCodeListDict![countryName] {
            return countryCodeListDict![countryName] as! String
        } else
        {
            return ""
        }
    }
    
    func getCountryCode() -> String
    {
        let networkInfo =  CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        if carrier?.isoCountryCode != nil
        {
           return getCountryDialingCodeFromCountryName(countryName: (carrier?.isoCountryCode?.uppercased())!)
        }
        else
        {
            getCountryCodeManually(handler: { (countryCode) -> Void in
                if let countryCode = countryCode{
                  self.getCountryDialingCodeFromCountryName(countryName: countryCode)
                }
            })
            return ""
        }
    }
    
    func getCountryCodeManually( handler:@escaping (_ countryCode:String?)-> Void)
    {
        let networkInfo =  CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        if carrier?.isoCountryCode != nil
        {
            handler(getCountryDialingCodeFromCountryName(countryName: (carrier?.isoCountryCode?.uppercased())!))
        }
        else
        {

        }
    }
    
   
    
    internal func createGroupName(groupId:String,userName:String) -> String
    {
        
        let groupName=groupId+"-"+userName

        return groupName
        
    }
    
    
   
    
 
   
  
    
   
   
    
    func downloadandSetImage(imageView:UIImageView)
    {

    }
    
    
    
   
    
    func setPlaceholderColorToWhite(txtfield:UITextField){
        txtfield.attributedPlaceholder = NSAttributedString(string:txtfield.placeholder!,
                                                            attributes:[NSForegroundColorAttributeName: UIColor.white.withAlphaComponent(0.7)])
    }
    
    
    func hideNavigationView(){
        customView?.removeFromSuperview()
    }
    
    func setBlurEffectonImage()->UIView{
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.init(rawValue:100 )!)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        return blurEffectView

        
        return UIView()
    }
    func setBlurEffectonImage(image:UIImage)->UIImage{
        let imageToBlur = CIImage(image: image)
        let blurfilter = CIFilter(name: "CIGaussianBlur")
        blurfilter!.setValue(imageToBlur, forKey: "inputImage")
        let resultImage = blurfilter!.value(forKey: "outputImage") as! CIImage
        let croppedImage: CIImage = resultImage.cropping(to: CGRect(x: 0, y: 0, width: imageToBlur!.extent.size.width, height: imageToBlur!.extent.size.height))
    
        let context = CIContext(options: nil)
        let blurredImage = UIImage (cgImage: context.createCGImage(croppedImage, from: croppedImage.extent)!)
        return blurredImage
        
          }
    
    func isHideChatButton(status:Bool){
        chatButtonView?.isHidden = status
    }
    
    func accessDeviceContacts()
    {
        
    }
    func getDeviceToken()
    {
        let settings = UIUserNotificationSettings(types: .alert, categories: nil)
        //        let settings = UIUserNotificationSettings(forTypes: .Alert, categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings);
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func addCircularProgressView(contentView:UIView)
    {
    progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: contentView.frame.size.width, height: contentView.frame.size.height))
    progress.startAngle = -90
    progress.progressThickness = 1.0
    progress.trackThickness = 0.0
    progress.clockwise = true
    progress.gradientRotateSpeed = 2
    progress.roundedCorners = false
    progress.glowMode = .forward
    progress.glowAmount = 0.9
    progress.set(colors: UIColor.cyan ,UIColor.white, UIColor.magenta, UIColor.white, UIColor.orange)
    progress.center = CGPoint(x: contentView.center.x-105, y: contentView.center.y-190)
    contentView.addSubview(progress)
    }
    
    func startCircularProgressView(gesture : UILongPressGestureRecognizer)
    {
       
    progress.animate(fromAngle: 0, toAngle: 360, duration: 10) { completed in
    if completed {
    print("animation stopped, completed")
       self.progress.stopAnimation()
        self.progress.removeFromSuperview()
        self.delegate?.progressViewCompleted()
    } else
    {
    print("animation stopped, was interrupted")
    }
    }
    }
    
    
    func goToHome(parentCtrl : DashBoardCtrl)
    {
        var controllerArray : [UIViewController] = []
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 : NotificationViewController = mainStoryboard.instantiateViewController(withIdentifier: "notify") as! NotificationViewController
        
        controller1.title = ""
        controllerArray.append(controller1)
        
        
        let controller2 : HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "home") as! HomeViewController
        
        controller2.bannersImagesArray = (parentCtrl ).bannersArray
        controller2.title = ""
        controllerArray.append(controller2)
        let strUserType = UserDefaults.standard.object(forKey: "user_type") as! String
      
        
        if  strUserType == "business"
            
        {
            let controller3 : BusinessProfileCtrl = mainStoryboard.instantiateViewController(withIdentifier: "businessAccount") as! BusinessProfileCtrl
            controller3.title = ""
            controllerArray.append(controller3)
            
        }
        else
        {
       
        let controller3 : AccountViewController = mainStoryboard.instantiateViewController(withIdentifier: "account") as! AccountViewController
            controller3.title = ""
            controllerArray.append(controller3)
        }
        
       
        
        // Customize menu (Optional)
        
        let navColor : UIColor =  UIColor(patternImage: UIImage(named: "navBg.png")!)
        
        var menuWidth = 60.0
        
        if DeviceType.IS_IPHONE_6 {
            menuWidth =  70.0
        }
        if DeviceType.IS_IPHONE_6P {
           menuWidth = 78
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(navColor),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.white),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
            .menuMargin(3),
            .menuHeight(50.0),
            .menuItemWidth(CGFloat(menuWidth)),
            .centerMenuItems(false)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: parentCtrl.containerView.frame.width, height: parentCtrl.containerView.frame.height), pageMenuOptions: parameters)
        
        parentCtrl.addChildViewController(pageMenu!)
        parentCtrl.containerView.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: parentCtrl)
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
     var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
     func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    func getCurrentTime() -> String
    {
        let currentDateTime = NSDate()
        let datePlus60Minutes:NSDate = currentDateTime.addingTimeInterval(60*60)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        dateFormatter.dateFormat = "yyyy/MM/dd hh:mm:ss a"
        let stringDate = dateFormatter .string(from: currentDateTime as Date)
        let time:String = stringDate .components(separatedBy: " ")[1]
        let amPm:String = stringDate .components(separatedBy: " ")[2]
        
        return String(format: "%@ %@", time,amPm)
    }
    
    func colorWithHexString (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    

}


