//
//  ForgotPasswordCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 03/03/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class ForgotPasswordCtrl: UIViewController ,AppServiceDelegate{

    @IBOutlet weak var txtEmail: UITextField!
     let ser:AppService = AppService()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBackButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
         }

    @IBAction func sendPasswordBtnClicked(_ sender: Any)
    {
        if (txtEmail.text?.characters.count)! <= 0
        {
           MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: "Please enter email address", viewcontroller: self)
        }
        else if !(Validator.sharedValidator.isValidEmail(testStr: txtEmail.text!))
        {
          MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "", messageString: "Please enter valid email address", viewcontroller: self)
           
        }
        else
        {
        
            Loader.sharedLoader.showLoaderOnScreen(vc: self)
            ser.delegate = self;
            self.ser.forgotPassword(email: txtEmail.text!)
        
        }
        
    }
    func textFieldShouldReturn(_ userText: UITextField) -> Bool
    {
       
        userText.resignFirstResponder()
        return true;
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        DispatchQueue.main.async {
            Loader.sharedLoader.hideLoader()
        }
        if response.statusCode == true
        {
            if(apiName == "forgotPassword")
            {
               
                let alert = UIAlertController(title: "Alert", message: response.statusDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                      self.dismiss(animated: true, completion: nil)
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
           
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
