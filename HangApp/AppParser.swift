//
//  AppParser.swift



import UIKit


class AppParser: NSObject
{
    func convertStringToDictionary(text: String) -> [String:AnyObject]?
    {
        if let data = text.data(using:
            String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func perseResponse( s : String) -> Response
    {
        let dict:NSDictionary = convertStringToDictionary(text: s)! as NSDictionary
        let rspnse:UserParsedInfo = UserParsedInfo()
        rspnse.statusCode = dict.object(forKey: "success") as? Bool
        rspnse.statusDescription = dict.object(forKey: "msg") as? String
        
         return rspnse
    }
    
    
    func perseLikeResponse( s : String) -> Response
    {
        let dict:NSDictionary = convertStringToDictionary(text: s)! as NSDictionary
        let rspnse:UserParsedInfo = UserParsedInfo()
        rspnse.statusCode = dict.object(forKey: "success") as? Bool
        rspnse.statusDescription = dict.object(forKey: "msg") as? String
        rspnse.momentLikeCount = dict.object(forKey: "likecount") as? Int != nil ?dict["likecount"] as! Int : 0
        
        
        return rspnse
    }
    
    func perseJSONResponse( jsonString : JSON) -> Response
    {
        let rspnse:UserParsedInfo = UserParsedInfo()
        rspnse.statusCode =   jsonString[KStatusCode].boolValue
        rspnse.statusDescription = jsonString[KStatusDescription].stringValue
        return rspnse
    }
    
    func parseContactInfo(responseString:String) -> HangUsersParserInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
       
        let rspnse:HangUsersParserInfo = HangUsersParserInfo()
         rspnse.statusDescription = dict.object(forKey: "error") as? String
        if  rspnse.statusDescription == nil
        {
            rspnse.statusCode = dict.object(forKey: "success") as? Bool
            rspnse.statusDescription = dict.object(forKey: "msg") as? String
        }
       
        if rspnse.statusCode == true
        {
            rspnse.hangUsers = NSMutableArray()
            let contacts:NSArray = (dict ).object(forKey: "follows") as? NSArray != nil ?(dict).object(forKey: "follows") as! NSArray : []
            
            for contact in contacts
            {
                let  contact = contact as! NSDictionary
                let hangUser = UserContactInfo()
                
                
                
                hangUser.fullName = (contact ).object(forKey: "name") as? String != nil ?(contact ).object(forKey: "name") as! String : "N/A"
                
                 hangUser.authToken = (contact ).object(forKey: "authentication_token") as? String != nil ?(contact ).object(forKey: "authentication_token") as! String : ""
                
                hangUser.emailId =  (contact ).object(forKey: "email") as? String != nil ?(contact ).object(forKey: "email") as! String : ""
                 hangUser.accountId =  (contact ).object(forKey: "id") as! Int
                let dictUrl  =  (contact ).object(forKey: "image") as? NSDictionary
                  let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    hangUser.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    hangUser.imageUrl = ""
                }
                hangUser.userName = contact["username"] as? String != nil ?contact["username"] as! String : ""
                 hangUser.location = contact["location"] as? String != nil ?contact["location"] as! String : ""
                 hangUser.userId = contact["id"] as? Int != nil ?contact["id"] as! Int : 0
                
                rspnse.hangUsers.add(hangUser)
                
            }
        
            
        }
        return rspnse
    }
    
    func parseMessage(responseString:String) -> UserContactInfo
    {
        
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {
            
            let messagesArray:NSArray = dict.object(forKey: "messages") as! NSArray
            let userMessages = NSMutableArray()
            
            
            for message in messagesArray
            {
                let  message = message as! NSDictionary
                let messageInfo = Messages()
                messageInfo.id = (message ).object(forKey: "id") as? Int != nil ?(message ).object(forKey: "id") as! Int : 0
                messageInfo.body =  (message ).object(forKey: "body") as? String != nil ?(message ).object(forKey: "body") as! String : ""
                
                messageInfo.sender_name =  (message ).object(forKey: "name") as? String != nil ?(message ).object(forKey: "name") as! String : "N/A"
               
               // let dictUrl  =  (message ).object(forKey: "user_image") as? NSDictionary
                let imgUrl  =  (message).object(forKey: "image") as? NSString
                if imgUrl != nil
                {
                    messageInfo.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    messageInfo.imageUrl = ""
                }
                messageInfo.message_date = (message ).object(forKey: "created_at") as? String != nil ?(message ).object(forKey: "created_at") as! String : ""
                
                userMessages.add(messageInfo)
                
            }
            hangUser.messagesArray  = userMessages
        }
        
        
        return hangUser
    }
    
    func parseReview(responseString:String) -> UserContactInfo
    {
        
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {

        let reviews:NSArray = dict.object(forKey: "Reviews") as! NSArray
        let userReviews = NSMutableArray()
        
        
        for review in reviews
        {
            let  review = review as! NSDictionary
            let reviewInfo = Review()
            reviewInfo.reviewId = (review ).object(forKey: "id") as? Int != nil ?(review ).object(forKey: "id") as! Int : 0
            reviewInfo.reviewDesc =  (review ).object(forKey: "description") as? String != nil ?(review ).object(forKey: "description") as! String : ""
            
            reviewInfo.name =  (review ).object(forKey: "user_name") as? String != nil ?(review ).object(forKey: "user_name") as! String : "N/A"
            reviewInfo.ratingCount =  (review).object(forKey: "rating") as! String
            let dictUrl  =  (review ).object(forKey: "user_image") as? NSDictionary
            let imgUrl  =  (dictUrl)?.object(forKey: "url") as? NSString
            if imgUrl != nil
            {
                reviewInfo.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                
            }
            else
            {
                reviewInfo.imageUrl = ""
            }
            reviewInfo.reviewDate = (review ).object(forKey: "created_at") as? String != nil ?(review ).object(forKey: "created_at") as! String : ""
            
            userReviews.add(reviewInfo)
            
        }
            hangUser.reviewArray  = userReviews
    }
        
        
        return hangUser
    }
    
    
    func parseComments(responseString:String) -> UserContactInfo
    {
        
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {
            
            let comments:NSArray = dict.object(forKey: "comments") as! NSArray
            let userComments = NSMutableArray()
            
            
            for comment in comments
            {
                let  comment = comment as! NSDictionary
                let commentInfo = Comment()
                commentInfo.commentId = (comment ).object(forKey: "id") as? Int != nil ?(comment ).object(forKey: "id") as! Int : 0
                commentInfo.commentDesc =  (comment ).object(forKey: "body") as? String != nil ?(comment ).object(forKey: "body") as! String : ""
                
                commentInfo.name =  (comment ).object(forKey: "name") as? String != nil ?(comment ).object(forKey: "name") as! String : "N/A"
                
                
                let imgUrl  =  (comment).object(forKey: "image") as? NSString
                if imgUrl != nil
                {
                    commentInfo.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    commentInfo.imageUrl = ""
                }
                commentInfo.commentDate = (comment ).object(forKey: "created_at") as? String != nil ?(comment ).object(forKey: "created_at") as! String : ""
                
                userComments.add(commentInfo)
                
            }
            hangUser.commentsArray  = userComments
        }
        
        
        return hangUser
    }

    func parseNotifications(responseString:String) -> Notifications
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        
        let rspnse:Notifications = Notifications()
        rspnse.statusDescription = dict.object(forKey: "error") as? String
        if  rspnse.statusDescription == nil
        {
            rspnse.statusCode = dict.object(forKey: "success") as? Bool
            rspnse.statusDescription = dict.object(forKey: "msg") as? String
        }
        
        if rspnse.statusCode == true
        {
            rspnse.followedUsers = NSMutableArray()
            rspnse.folowers = NSMutableArray()
            rspnse.tags = NSMutableArray()
            rspnse.messages = NSMutableArray()
            let followed_users:NSArray = (dict ).object(forKey: "followed_user") as? NSArray != nil ?(dict).object(forKey: "followed_user") as! NSArray : []
            let tags:NSArray = (dict ).object(forKey: "Tags") as? NSArray != nil ?(dict).object(forKey: "Tags") as! NSArray : []
            let followers:NSArray = (dict ).object(forKey: "followers") as? NSArray != nil ?(dict).object(forKey: "followers") as! NSArray : []
            let messages:NSArray = (dict ).object(forKey: "message") as? NSArray != nil ?(dict).object(forKey: "message") as! NSArray : []
            
            for contact in followed_users
            {
                let  contact = contact as! NSDictionary
                let hangUser = UserFollowers()
                
                
                
                hangUser.fullName = (contact ).object(forKey: "name") as? String != nil ?(contact ).object(forKey: "name") as! String : "N/A"
                
                hangUser.authToken = (contact ).object(forKey: "authentication_token") as? String != nil ?(contact ).object(forKey: "authentication_token") as! String : ""
                
                hangUser.id =  (contact ).object(forKey: "id") as! Int
                let dictUrl  =  (contact ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    hangUser.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    hangUser.imageUrl = ""
                }
                hangUser.userName = contact["username"] as? String != nil ?contact["username"] as! String : ""
              
                hangUser.user_id = contact["user_id"] as? Int != nil ?contact["user_id"] as! Int : 0
                
                rspnse.followedUsers.add(hangUser)
                
            }
            
            for follower in followers
            {
                let  follower = follower as! NSDictionary
                let hangUser = UserFollowers()
                
                
                
                hangUser.fullName = (follower ).object(forKey: "name") as? String != nil ?(follower ).object(forKey: "name") as! String : "N/A"
                
    
                hangUser.id =  (follower ).object(forKey: "id") as! Int
                let dictUrl  =  (follower ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    hangUser.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    hangUser.imageUrl = ""
                }
                hangUser.user_id = follower["user_id"] as? Int != nil ?follower["user_id"] as! Int : 0
                
                rspnse.folowers.add(hangUser)
                
            }
            for tag in tags
            {
                let  tag = tag as! NSDictionary
                let tagsUser = Tags()
                
                
                
                tagsUser.tagByUser_Name = (tag ).object(forKey: "name") as? String != nil ?(tag ).object(forKey: "name") as! String : "N/A"
             
                tagsUser.tagByUser_id =  (tag ).object(forKey: "user_id") as? Int  != nil ?tag["user_id"] as! Int : 0
                let imgUrl  =  (tag ).object(forKey: "image") as? NSString
                
                if imgUrl != nil
                {
                    tagsUser.tagUserImageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    tagsUser.tagUserImageUrl = ""
                }
               
               
                tagsUser.moment_id = tag["moment_id"] as? Int != nil ?tag["moment_id"] as! Int : 0
                
                rspnse.tags.add(tagsUser)
                
            }
            
            for message in messages
            {
                let  msg = message as! NSDictionary
                let userMessage = Messages()
                
                
                userMessage.body = (msg ).object(forKey: "body") as? String != nil ?(msg ).object(forKey: "body") as! String : ""
                
                userMessage.user_id =  (msg ).object(forKey: "user_id") as! Int
                
                
                userMessage.sender_id = msg["sender_id"] as? Int != nil ?msg["sender_id"] as! Int : 0
                userMessage.sender_name =  (msg ).object(forKey: "name") as? String != nil ?(msg ).object(forKey: "name") as! String : "N/A"
                let dictUrl  =  (msg ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    userMessage.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    userMessage.imageUrl = ""
                }
                rspnse.messages.add(userMessage)
                
            }
            
            
        }
        return rspnse
    }

    
    
    func parseSearchContactInfo(responseString:String) -> HangUsersParserInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        
        let rspnse:HangUsersParserInfo = HangUsersParserInfo()
        rspnse.statusDescription = dict.object(forKey: "error") as? String
        if  rspnse.statusDescription == nil
        {
            rspnse.statusCode = dict.object(forKey: "success") as? Bool
            rspnse.statusDescription = dict.object(forKey: "msg") as? String
        }
        
        if rspnse.statusCode == true
        {
            rspnse.hangUsers = NSMutableArray()
            let contacts:NSArray = (dict ).object(forKey: "users") as? NSArray != nil ?(dict).object(forKey: "users") as! NSArray : []
            
            for contact in contacts
            {
                let  contact = contact as! NSDictionary
                let hangUser = UserContactInfo()
                
                hangUser.fullName = (contact ).object(forKey: "name") as? String != nil ?(contact ).object(forKey: "name") as! String : "N/A"
                
                hangUser.authToken = (contact ).object(forKey: "authentication_token") as? String != nil ?(contact ).object(forKey: "authentication_token") as! String : ""
                
                hangUser.emailId =  (contact ).object(forKey: "email") as? String != nil ?(contact ).object(forKey: "email") as! String : ""
                hangUser.accountId =  (contact ).object(forKey: "id") as! Int
                let dictUrl  =  (contact ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    hangUser.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    hangUser.imageUrl = ""
                }
                hangUser.userName = contact["username"] as? String != nil ?contact["username"] as! String : ""
                hangUser.location = contact["location"] as? String != nil ?contact["location"] as! String : ""
                hangUser.userId = contact["id"] as? Int != nil ?contact["id"] as! Int : 0
                hangUser.userType = contact["user_type"] as? String != nil ?contact["user_type"] as! String : ""
                hangUser.followed_id =  (contact ).object(forKey: "followed_id") as? Int != nil ?contact["followed_id"] as! Int : 0
                hangUser.isFriend =  (contact ).object(forKey: "is_follow") as! Bool
                hangUser.followed_users_status =  (contact ).object(forKey: "followed_users_status") as! Bool

                rspnse.hangUsers.add(hangUser)
                
            }
            
            
        }
        return rspnse
    }
    

    
    func parseMomentData(responseString:String) -> UserContactInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {
            let moments:NSArray = dict.object(forKey: "moments") as! NSArray
            let userMoments = NSMutableArray()
            
            
            for moment in moments
            {
                let  moment = moment as! NSDictionary
                let momentinfo = MomentsInfo()
                momentinfo.momentId = (moment ).object(forKey: "id") as? Int != nil ?(moment ).object(forKey: "id") as! Int : 0
           
                
                momentinfo.address =  (moment ).object(forKey: "address") as? String != nil ?(moment ).object(forKey: "address") as! String : ""
                
                let dictUser  =  (moment ).object(forKey: "user") as? NSDictionary
                 momentinfo.momentPostedByUserId = (dictUser )?.object(forKey: "user_id") as? Int != nil ?(dictUser )?.object(forKey: "user_id") as! Int : 0
               
               let dictUserImage  =  (dictUser )?.object(forKey: "user_image") as? NSDictionary
                 let imgUserUrl = (dictUserImage )?.object(forKey: "url") as? String != nil ?(dictUserImage )?.object(forKey: "url") as! String : ""
                momentinfo.momentPostedByUserImage =  String(format:"%@%@",kBaseURL, imgUserUrl )
                momentinfo.momentPostedByUser =  (dictUser )?.object(forKey: "name") as? String != "" ?(dictUser )?.object(forKey: "name") as! String : "N/A"
              
                momentinfo.caption =  (moment ).object(forKey: "caption") as! String
                momentinfo.categoryId =  (moment ).object(forKey: "banner_id") as! Int
                //momentinfo.viewsCount =  (moment ).object(forKey: "views_count") as! Int
                momentinfo.fileType  =     (moment ).object(forKey: "moment_type") as! String
                momentinfo.commentCount =  (moment ).object(forKey: "comment_count") as! Int
                 momentinfo.likeCount =  (moment ).object(forKey: "comment_count") as! Int
                 momentinfo.isLike =  (moment ).object(forKey: "is_like") as! Bool

                let dictUrl  =  (moment ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    momentinfo.momentsUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    momentinfo.momentsUrl = ""
                }
                
                let dictVideo  =  (moment ).object(forKey: "video") as? NSDictionary
                let videoUrl  =  (dictVideo )?.object(forKey: "url") as? NSString
                if videoUrl != nil
                {
                    momentinfo.momentsVideoUrl  = String(format:"%@%@",kBaseURL, videoUrl! )
                    
                }
                else
                {
                    momentinfo.momentsVideoUrl = ""
                }
                
                let dictVideoThumbUrl  =  (dictVideo )?.object(forKey: "thumb") as? NSDictionary
                let videoThumbUrl  =  (dictVideoThumbUrl )?.object(forKey: "url") as? NSString
                if videoThumbUrl != nil
                {
                    momentinfo.momentsVideoThumbUrl  = String(format:"%@%@",kBaseURL, videoThumbUrl! )
                    
                }
                else
                {
                    momentinfo.momentsVideoThumbUrl = ""
                }
                

                
                userMoments.add(momentinfo)
                
            }
            hangUser.momentsArray  = userMoments
            
            
        }
        
        return hangUser
    }
    
    
    
    func parseCheckInData(responseString:String) -> UserContactInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {
            let momentDict:NSDictionary = dict.object(forKey: "place") as! NSDictionary
            let moments:NSArray = momentDict.object(forKey: "moment_info") as! NSArray
            let userMoments = NSMutableArray()
            
            
            for moment in moments
            {
                let  moment = moment as! NSDictionary
                let momentinfo = CheckinInfo()
                momentinfo.checkinId = (moment ).object(forKey: "id") as? Int != nil ?(moment ).object(forKey: "id") as! Int : 0
                
                momentinfo.address =  (moment ).object(forKey: "name") as? String != nil ?(moment ).object(forKey: "name") as! String : ""
                
                
                momentinfo.loc_lat =  Double ((moment ).object(forKey: "latitude") as! String)                 
            momentinfo.loc_long =  Double ((moment ).object(forKey: "longitude") as! String)
                momentinfo.checkinDistance = (moment ).object(forKey: "distance") as? Double != nil ?(moment ).object(forKey: "distance") as! Double : 0.0
               
               // momentinfo.fileType  =     (moment ).object(forKey: "moment_type") as! String
                
                let dictUrl  =  (moment ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    momentinfo.placesUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    momentinfo.placesUrl = ""
                }
              momentinfo.checkinDate  = (moment).object(forKey: "create") as? String != nil ?(moment ).object(forKey: "create") as! String : ""
                
                let momentCommentData:NSArray = moment.object(forKey: "comments") as! NSArray
                
                
                let usersComments = NSMutableArray()
                for comment in momentCommentData
                {
                    
                    let comment  =  comment as! NSDictionary
                    
                    let  momentComment = Comment()
                    
                    momentComment.commentId = (comment).object(forKey: "id") as? Int != nil ?(comment).object(forKey: "id") as! Int : 0
                    momentComment.name =  (comment ).object(forKey: "name") as? String != nil ?(comment ).object(forKey: "name") as! String : "N/A"
                    
                    momentComment.commentDesc = (comment ).object(forKey: "body") as? String != nil ?(comment ).object(forKey: "body") as! String : ""
                    
                    momentComment.commentDate = (comment ).object(forKey: "created_at") as? String != nil ?(comment ).object(forKey: "created_at") as! String : ""
                    
                    let dictImgUrl  =  (comment).object(forKey: "image") as? NSDictionary
                     let imgUrl =  (dictImgUrl)?.object(forKey: "url") as? NSString
                    if imgUrl != nil
                    {
                        momentComment.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                        
                    }
                    else
                    {
                        momentComment.imageUrl = ""
                    }
                    usersComments.add(momentComment)
                    
                }
                momentinfo.commentsArray = usersComments
                
                    let tag_ByUser:NSArray = moment.object(forKey: "tags_user") as! NSArray
                
                for tagByUser in tag_ByUser
                {
                    let  tagByUser = tagByUser as! NSDictionary
                    momentinfo.tagByUserId = (tagByUser).object(forKey: "id") as? Int != nil ?(tagByUser ).object(forKey: "id") as! Int : 0
                        momentinfo.tagByUserName = (tagByUser ).object(forKey: "name") as? String != nil ?(tagByUser ).object(forKey: "name") as! String : "N/A"
                       
                        momentinfo.isFriend =  (tagByUser ).object(forKey: "is_follow") as? Bool != nil ?(tagByUser).object(forKey: "is_follow") as! Bool : false
                        momentinfo.followed_users_status =  (tagByUser ).object(forKey: "followed_users_status") as? Bool != nil ?(tagByUser).object(forKey: "followed_users_status") as! Bool : false
                        
                        
                        let dictTagByUrl  =  (tagByUser ).object(forKey: "image") as? NSDictionary
                       let tagByUserImgUrl  =  (dictTagByUrl )?.object(forKey: "url") as? NSString
                        if tagByUserImgUrl != nil
                        {
                            momentinfo.tagByUserImageUrl  = String(format:"%@%@",kBaseURL, tagByUserImgUrl! )
                            
                        }
                    else
                    {
                        momentinfo.tagByUserImageUrl = ""
                    }
                }
                let tag_moments:NSArray = moment.object(forKey: "tags_by_user") as! NSArray
                let tagsUsers = NSMutableArray()
                    for tagUser in tag_moments
                    {
                        
                        let dictUser  =  tagUser as! NSDictionary
                        
                        
                        let tags = Tags()
                        tags.tagUser_id = (dictUser ).object(forKey: "id") as? Int != nil ?(dictUser ).object(forKey: "id") as! Int : 0
                        
                        tags.tagUser_Name =  (dictUser ).object(forKey: "name") as? String != nil ?(dictUser ).object(forKey: "name") as! String : "N/A"
                        tags.isFriend =  (dictUser ).object(forKey: "is_follow") as? Bool != nil ?(dictUser ).object(forKey: "is_follow") as! Bool : false
                        tags.followed_users_status =  (dictUser ).object(forKey: "followed_users_status") as? Bool != nil ?(dictUser ).object(forKey: "followed_users_status") as! Bool : false
                        
                        
                        let dictUrlTagUser  =  (dictUser).object(forKey: "image") as? NSDictionary
                        let imgUrlTag  =  (dictUrlTagUser )?.object(forKey: "url") as? NSString
                        if imgUrlTag != nil
                        {
                            tags.tagUserImageUrl  = String(format:"%@%@",kBaseURL, imgUrlTag! )
                            
                        }
                        else
                        {
                            tags.tagUserImageUrl = ""
                        }
                        tagsUsers.add(tags)
                        
                    }
                    
                
                momentinfo.tag_Users = tagsUsers
                        
                
                    
                
                              // let dictVideo  =  (moment ).object(forKey: "video") as? NSDictionary
                //let videoUrl  =  (dictVideo )?.object(forKey: "url") as? NSString
                //if videoUrl != nil
                //{
                   // momentinfo.momentsVideoUrl  = String(format:"%@%@",kBaseURL, videoUrl! )
                    
                //}
                //else
               // {
                   // momentinfo.momentsVideoUrl = ""
                //}
                
                //let dictVideoThumbUrl  =  (dictVideo )?.object(forKey: "thumb") as? NSDictionary
                //let videoThumbUrl  =  (dictVideoThumbUrl )?.object(forKey: "url") as? NSString
                //if videoThumbUrl != nil
                //{
                    //momentinfo.momentsVideoThumbUrl  = String(format:"%@%@",kBaseURL, videoThumbUrl! )
                    
                //}
                //else
                //{
                   // momentinfo.momentsVideoThumbUrl = ""
                //}
                
                
                
                userMoments.add(momentinfo)
                
            }
            
            hangUser.momentsArray  = userMoments
            
            
        }
        
        return hangUser
    }

    
    func parsYourStories(responseString:String) -> UserContactInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {
            let moments:NSArray = dict.object(forKey: "yourstories") as! NSArray
            let userMoments = NSMutableArray()
            
            
            for moment in moments
            {
                let  moment = moment as! NSDictionary
                let momentinfo = MomentsInfo()
                momentinfo.momentId = (moment ).object(forKey: "id") as? Int != nil ?(moment ).object(forKey: "id") as! Int : 0
                
                let dictUrl  =  (moment ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    momentinfo.momentsUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    momentinfo.momentsUrl = ""
                }
                
                
                userMoments.add(momentinfo)
                
            }
            hangUser.momentsArray  = userMoments
            
            
        }
        
        return hangUser
    }
    
    
    func parseProfileInfo(responseString:String) -> UserContactInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        print(dict)
        let hangUser = UserContactInfo()
        
        
        hangUser.statusCode = dict.object(forKey: "success") as? Bool
        hangUser.statusDescription = dict.object(forKey: "msg") as? String
        
        if hangUser.statusCode == true
        {
            
            let contact:NSDictionary = dict.object(forKey: "user") as! NSDictionary
            hangUser.badgeNumber = dict.object(forKey: "unreade_message") as? Int != nil ?(dict ).object(forKey: "unreade_message") as! Int : 0

            hangUser.fullName = (contact ).object(forKey: "name") as? String != nil ?(contact ).object(forKey: "name") as! String : "N/A"
            if hangUser.fullName == ""
            {
                hangUser.fullName =  "N/A"
            }
            
            hangUser.authToken = (contact ).object(forKey: "authentication_token") as? String != nil ?(contact ).object(forKey: "authentication_token") as! String : ""
            
            hangUser.emailId =  (contact ).object(forKey: "email") as? String != nil ?(contact ).object(forKey: "email") as! String : ""
            hangUser.accountId =  (contact ).object(forKey: "id") as! Int
            let dictUrl  =  (contact ).object(forKey: "image") as? NSDictionary
            let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
            if imgUrl != nil
            {
                hangUser.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
            }
            else
            {
                hangUser.imageUrl = ""
            }
            hangUser.userName = contact["username"] as? String != nil ?contact["username"] as! String : ""
            hangUser.bioGraphy = contact["biography"] as? String != nil ?contact["biography"] as! String : "N/A"
            hangUser.location = contact["location"] as? String != nil ?contact["location"] as! String : "N/A"
            if hangUser.location == "" {
                hangUser.location =  "N/A"
            }
            if hangUser.bioGraphy == "" {
                hangUser.bioGraphy =  "N/A"
            }
            hangUser.dob = contact["dob"] as? String != nil ?contact["dob"] as! String : ""
            
            let moments:NSArray = contact.object(forKey: "moments") as! NSArray
            let userMoments = NSMutableArray()
            
            
            for moment in moments
            {
                let  moment = moment as! NSDictionary
                let momentinfo = MomentsInfo()
                momentinfo.momentId = (moment ).object(forKey: "id") as? Int != nil ?(moment ).object(forKey: "id") as! Int : 0
                
                momentinfo.address =  (moment ).object(forKey: "address") as? String != nil ?(moment ).object(forKey: "address") as! String : ""
                momentinfo.loc_lat =  Double ((moment ).object(forKey: "latitude") as! String) != nil ?Double ((moment ).object(forKey: "latitude") as! String): 0.0

                momentinfo.loc_long =  Double ((moment ).object(forKey: "longitude") as! String) != nil ?Double ((moment ).object(forKey: "longitude") as! String) : 0.0

                momentinfo.caption =  (moment ).object(forKey: "caption") as! String
                momentinfo.categoryId =  (moment ).object(forKey: "banner_id") as? Int != nil ?(moment ).object(forKey: "banner_id") as! Int : 0

                momentinfo.viewsCount =  (moment ).object(forKey: "views_count") as! Int
                momentinfo.fileType  =     (moment ).object(forKey: "moment_type") as! String
                
                let dictUrl  =  (moment ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    momentinfo.momentsUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    momentinfo.momentsUrl = ""
                }
                
                let dictVideo  =  (moment ).object(forKey: "video") as? NSDictionary
                let videoUrl  =  (dictVideo )?.object(forKey: "url") as? NSString
                if videoUrl != nil
                {
                    momentinfo.momentsVideoUrl  = String(format:"%@%@",kBaseURL, videoUrl! )
                    
                }
                else
                {
                    momentinfo.momentsVideoUrl = ""
                }
                
                let dictVideoThumbUrl  =  (dictVideo )?.object(forKey: "thumb") as? NSDictionary
                let videoThumbUrl  =  (dictVideoThumbUrl )?.object(forKey: "url") as? NSString
                if videoThumbUrl != nil
                {
                momentinfo.momentsVideoThumbUrl  = String(format:"%@%@",kBaseURL, videoThumbUrl! )
                    
                }
                else
                {
                    momentinfo.momentsVideoThumbUrl = ""
                }
                
                
                
                userMoments.add(momentinfo)
                
            }
          //  let places:NSArray = contact.object(forKey: "places") as! NSArray
           // let userPlacess = NSMutableArray()
            
            
            //for place in places
            //{
                //let  place = place as! NSDictionary
                //let checkininfo = CheckinInfo()
                //checkininfo.checkinId = (place ).object(forKey: "id") as? Int != nil ?(place ).object(forKey: "id") as! Int : 0
                
                //checkininfo.address =  (place ).object(forKey: "address") as? String != nil ?(place ).object(forKey: "address") as! String : ""
                
              
                //checkininfo.loc_lat =  Double ((place ).object(forKey: "latitude") as! String)
                //checkininfo.loc_long = Double ((place ).object(forKey: "longitude") as! String)
               // checkininfo.fileType  =     (place ).object(forKey: "moment_type") as! String
                
                //let dictUrl  =  (place ).object(forKey: "image") as? NSDictionary
                //let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                //if imgUrl != nil
                //{
                   // checkininfo.placesUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                //}
                //else
                //{
                    //checkininfo.placesUrl = ""
                //}
                
                
                //userPlacess.add(checkininfo)
                
            //}
            
            let reviews:NSArray = dict.object(forKey: "Reviews") as! NSArray
            let useReviews = NSMutableArray()
            
            
            for review in reviews
            {
                let  review = review as! NSDictionary
                let reviewInfo = Review()
                reviewInfo.reviewId = (review ).object(forKey: "id") as? Int != nil ?(review ).object(forKey: "id") as! Int : 0
                reviewInfo.reviewDesc =  (review ).object(forKey: "description") as? String != nil ?(review ).object(forKey: "description") as! String : ""
                
                  reviewInfo.name =  (review ).object(forKey: "user_name") as? String != nil ?(review ).object(forKey: "user_name") as! String : "N/A"
                reviewInfo.ratingCount =  (review).object(forKey: "rating") as! String
                 let dictUrl  =  (review ).object(forKey: "user_image") as? NSDictionary
                let imgUrl  =  (dictUrl)?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    reviewInfo.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    reviewInfo.imageUrl = ""
                }
                reviewInfo.reviewDate = (review ).object(forKey: "created_at") as? String != nil ?(review ).object(forKey: "created_at") as! String : ""
                
                useReviews.add(reviewInfo)
                
            }
            
            let contacts:NSArray = (dict ).object(forKey: "fr") as? NSArray != nil ?(dict).object(forKey: "fr") as! NSArray : []
             let userFriends = NSMutableArray()
            
            for contact in contacts
            {
                let  contact = contact as! NSDictionary
                let hangUser = UserContactInfo()
                
                
                
                hangUser.fullName = (contact ).object(forKey: "name") as? String != nil ?(contact ).object(forKey: "name") as! String : "N/A"
                
                hangUser.authToken = (contact ).object(forKey: "authentication_token") as? String != nil ?(contact ).object(forKey: "authentication_token") as! String : ""
                
                hangUser.emailId =  (contact ).object(forKey: "email") as? String != nil ?(contact ).object(forKey: "email") as! String : ""
                hangUser.accountId =  (contact ).object(forKey: "id") as! Int
                let dictUrl  =  (contact ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    hangUser.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    hangUser.imageUrl = ""
                }
                hangUser.userName = contact["username"] as? String != nil ?contact["username"] as! String : ""
                hangUser.location = contact["location"] as? String != nil ?contact["location"] as! String : ""
                hangUser.userId = contact["id"] as? Int != nil ?contact["id"] as! Int : 0
                
                 hangUser.userType = (contact ).object(forKey: "user_type") as? String != nil ?(contact ).object(forKey: "user_type") as! String : "N/A"
                
                
                
                userFriends.add(hangUser)
                
            }
            
            hangUser.friendsArray = userFriends
            hangUser.momentsArray  = userMoments
            hangUser.reviewArray  = useReviews
            //hangUser.checkInArray = userPlacess
            
        }
        
        return hangUser
    }
    
    func parseMomentDetail(responseString:String) -> MomentsInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
         print(dict)
        let momentInfo = MomentsInfo()

        
            momentInfo.statusCode = dict.object(forKey: "success") as? Bool
            momentInfo.statusDescription = dict.object(forKey: "msg") as? String
        
        if momentInfo.statusCode == true
        {
            
            let momentData:NSDictionary = dict.object(forKey: "moment") as! NSDictionary
           
            
             let moment:NSDictionary = momentData.object(forKey: "moment_info") as! NSDictionary
            
            momentInfo.momentId = (moment ).object(forKey: "id") as? Int != nil ?(moment ).object(forKey: "id") as! Int : 0
            
            momentInfo.address =  (moment ).object(forKey: "address") as? String != nil ?(moment ).object(forKey: "address") as! String : ""
            momentInfo.loc_lat = Double ((moment ).object(forKey: "latitude") as! String) != nil ?Double ((moment ).object(forKey: "latitude") as! String) : 0.0
            
            momentInfo.loc_long = Double ((moment ).object(forKey: "longitude") as! String) != nil ?Double ((moment ).object(forKey: "longitude") as! String) : 0.0
            momentInfo.caption =  (moment ).object(forKey: "caption") as? String != nil ?(moment ).object(forKey: "caption") as! String : "N/A"
            momentInfo.momentDate =  (moment).object(forKey: "created_at") as? String != nil ?(moment ).object(forKey: "created_at") as! String : ""
            momentInfo.momentDistance =  (momentData).object(forKey: "distance") as? Double != nil ?(momentData ).object(forKey: "distance") as! Double : 0.0
            
          momentInfo.categoryId =  (moment ).object(forKey: "banner_id") as? Int != nil ?(moment ).object(forKey: "banner_id") as! Int : 0

            momentInfo.viewsCount =  (moment ).object(forKey: "views_count") as? Int != nil ?(moment ).object(forKey: "views_count") as! Int : 0

            momentInfo.fileType  =     (moment ).object(forKey: "moment_type") as! String
            momentInfo.likeCount = (momentData ).object(forKey: "likecount") as? Int != nil ?(momentData ).object(forKey: "likecount") as! Int : 0

            
            let dictUrl  =  (moment ).object(forKey: "image") as? NSDictionary
            let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
            if imgUrl != nil
            {
                momentInfo.momentsUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                
            }
            else
            {
                momentInfo.momentsUrl = ""
            }
            
            
            let dictVideo  =  (moment ).object(forKey: "video") as? NSDictionary
            let videoUrl  =  (dictVideo )?.object(forKey: "url") as? NSString
            if videoUrl != nil
            {
                momentInfo.momentsVideoUrl  = String(format:"%@%@",kBaseURL, videoUrl! )
                
            }
            else
            {
                momentInfo.momentsVideoUrl = ""
            }
            
            let dictVideoThumbUrl  =  (dictVideo )?.object(forKey: "thumb") as? NSDictionary
            let videoThumbUrl  =  (dictVideoThumbUrl )?.object(forKey: "url") as? NSString
            if videoThumbUrl != nil
            {
                momentInfo.momentsVideoThumbUrl  = String(format:"%@%@",kBaseURL, videoThumbUrl! )
                
            }
            else
            {
                momentInfo.momentsVideoThumbUrl = ""
            }
            

            let tag_moments:NSArray = momentData.object(forKey: "tag_moments") as! NSArray
            let tagsUsers = NSMutableArray()
            for tagmoment in tag_moments
            {
                let  tagmoment = tagmoment as! NSArray
                
                for tagUser in tagmoment
                {
                    
                    let dictUser  =  (tagUser as! NSDictionary).object(forKey: "user") as? NSDictionary
                     let dictTag_user  =  (tagUser as! NSDictionary).object(forKey: "tag_user") as? NSDictionary
                
                let tags = Tags()
                 tags.tagUser_id = (dictTag_user )?.object(forKey: "id") as? Int != nil ?(dictTag_user )?.object(forKey: "id") as! Int : 0
                 tags.tagByUser_id = (dictUser )?.object(forKey: "id") as? Int != nil ?(dictUser )?.object(forKey: "id") as! Int : 0
                tags.tagByUser_Name =  (dictUser )?.object(forKey: "name") as? String != nil ?(dictUser )?.object(forKey: "name") as! String : "N/A"
                tags.tagUser_Name =  (dictTag_user )?.object(forKey: "name") as? String != nil ?(dictTag_user )?.object(forKey: "name") as! String : "N/A"
              tags.isFriend =  (dictTag_user )?.object(forKey: "is_follow") as? Bool != nil ?(dictTag_user )?.object(forKey: "is_follow") as! Bool : false
                tags.followed_users_status =  (dictTag_user )?.object(forKey: "followed_users_status") as? Bool != nil ?(dictTag_user )?.object(forKey: "followed_users_status") as! Bool : false

                    
                let dictUrl  =  (dictUser )?.object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    tags.tagByUserImageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                    
                }
                else
                {
                    tags.tagByUserImageUrl = ""
                }
                    
                 let dictUrlTagUser  =  (dictTag_user )?.object(forKey: "image") as? NSDictionary
                    let imgUrlTag  =  (dictUrlTagUser )?.object(forKey: "url") as? NSString
                    if imgUrlTag != nil
                    {
                        tags.tagUserImageUrl  = String(format:"%@%@",kBaseURL, imgUrlTag! )
                        
                    }
                    else
                    {
                        tags.tagUserImageUrl = ""
                    }
                  tagsUsers.add(tags)
            
           }
           
        }
             momentInfo.tag_Users = tagsUsers
             let momentCommentData:NSArray = momentData.object(forKey: "Comments") as! NSArray
            
           
                let usersComments = NSMutableArray()
                for comment in momentCommentData
                {
                    
                    let comment  =  comment as! NSDictionary
                    
                    let  momentComment = Comment()
                
                    momentComment.commentId = (comment).object(forKey: "id") as? Int != nil ?(comment).object(forKey: "id") as! Int : 0
                    momentComment.name =  (comment ).object(forKey: "name") as? String != nil ?(comment ).object(forKey: "name") as! String : "N/A"
                    
                   momentComment.commentDesc = (comment ).object(forKey: "body") as? String != nil ?(comment ).object(forKey: "body") as! String : ""
                    
                     momentComment.commentDate = (comment ).object(forKey: "created_at") as? String != nil ?(comment ).object(forKey: "created_at") as! String : ""
                    
                    let imgUrl  =  (comment).object(forKey: "image") as? NSString
                    if imgUrl != nil
                    {
                        momentComment.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                        
                    }
                    else
                    {
                        momentComment.imageUrl = ""
                    }
                    usersComments.add(momentComment)
                    
                }
                momentInfo.commentsArray = usersComments
            
            
            
        }
        
        return momentInfo
    }
    
    func parseFollowerList(responseString:String) -> HangUsersParserInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        let rspnse:HangUsersParserInfo = HangUsersParserInfo()
        rspnse.statusCode = dict.object(forKey: "success") as? Bool
        rspnse.statusDescription = dict.object(forKey: "msg") as? String
        if rspnse.statusCode == true
        {
            rspnse.hangUsers = NSMutableArray()
            let contacts:NSArray = dict.object(forKey: "followed_users") as! NSArray
            
            for contact in contacts
            {
                let  contact = contact as! NSDictionary
                let userFollower = UserFollowers()
                
                
                
                userFollower.fullName = (contact ).object(forKey: "name") as? String != nil ?(contact ).object(forKey: "name") as! String : "N/A"
                
                userFollower.authToken = (contact ).object(forKey: "authentication_token") as? String != nil ?(contact ).object(forKey: "authentication_token") as! String : ""
                
                userFollower.emailId =  (contact ).object(forKey: "email") as? String != nil ?(contact ).object(forKey: "email") as! String : ""
                userFollower.id =  (contact ).object(forKey: "id") as! Int
                userFollower.user_id =  (contact ).object(forKey: "user_id") as! Int
                userFollower.followed_id =  (contact ).object(forKey: "followed_id") as! Int
                userFollower.is_followed =  (contact ).object(forKey: "is_followed") as! Bool
                
                let dictUrl  =  (contact ).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    userFollower.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    userFollower.imageUrl = ""
                }
                userFollower.userName = contact["username"] as? String != nil ?contact["username"] as! String : "N/A"
                
                rspnse.hangUsers.add(userFollower)
                
            }
            
            
        }
        return rspnse
    }
    

    
    func parseBannerImages(responseString:String) -> HangUsersParserInfo
    {
        let dict:NSDictionary = convertStringToDictionary(text: responseString)! as NSDictionary
        let rspnse:HangUsersParserInfo = HangUsersParserInfo()
        rspnse.statusCode = dict.object(forKey: "success") as? Bool
        rspnse.statusDescription = dict.object(forKey: "msg") as? String
        if rspnse.statusCode == true
        {
            rspnse.hangUsers = NSMutableArray()
            let images:NSArray = dict.object(forKey: "banner_uploads") as! NSArray
            
            for image in images
            {
                let  image = image as! NSDictionary
                let banerImg = BannerImages()
                
                
                
                banerImg.title = (image ).object(forKey: "title") as? String != nil ?(image ).object(forKey: "title") as! String : ""
                
                
                let dictUrl  =  (image).object(forKey: "image") as? NSDictionary
                let imgUrl  =  (dictUrl )?.object(forKey: "url") as? NSString
                if imgUrl != nil
                {
                    banerImg.imageUrl  = String(format:"%@%@",kBaseURL, imgUrl! )
                }
                else
                {
                    banerImg.imageUrl = ""
                }
                let dictUrl1  =  (image).object(forKey: "icon") as? NSDictionary
                let imgUrl1  =  (dictUrl1 )?.object(forKey: "url") as? NSString
                if imgUrl1 != nil
                {
                    banerImg.iconUrl  = String(format:"%@%@",kBaseURL, imgUrl1! )
                }
                else
                {
                    banerImg.iconUrl = ""
                }
              banerImg.bannerId = (image ).object(forKey: "id") as? Int != nil ?(image ).object(forKey: "id") as! Int : 0
                
                rspnse.hangUsers.add(banerImg)
                
            }
            
            
        }
        return rspnse
    }
    


    
    
    func perseLoginResponse( s : String) -> Response
   {
        let dict:NSDictionary = convertStringToDictionary(text: s)! as NSDictionary
        let rspnse:UserParsedInfo = UserParsedInfo()
    rspnse.statusDescription = dict.object(forKey: "error") as? String
    if  rspnse.statusDescription == nil
    {
        rspnse.statusCode = dict.object(forKey: "success") as? Bool
        rspnse.statusDescription = dict.object(forKey: "msg") as? String
    }
    
        if rspnse.statusCode == true
        {
            let responseDict:NSDictionary = dict.object(forKey: "user") as! NSDictionary
            let prefs:UserDefaults = UserDefaults.standard
            rspnse.emailId = responseDict.object(forKey: "email") as! String
            prefs.set(rspnse.emailId, forKey: kemailIdKey)
            rspnse.authToken = responseDict.object(forKey: "authentication_token") as! String
            prefs.set(rspnse.authToken, forKey: kauthKey)
            rspnse.createdDate = responseDict.object(forKey: "created_at") as! String
            prefs.set(rspnse.createdDate, forKey: "createDate")
            rspnse.userId = responseDict.object(forKey: "id") as! Int
            print("userId = \(rspnse.userId)")
            prefs.set(rspnse.userId, forKey: "userId")
            rspnse.userType = responseDict.object(forKey: "user_type") as! String
            prefs.set(rspnse.userType, forKey: "user_type")
        
        }
        return rspnse
   }
    func parseCitiesResponse(json:JSON) -> CityResponse
    {
        print(json)
        let cityResp = CityResponse()
        if json["status"].stringValue == "OK"
        {
            cityResp.statusDescription = json["status"].stringValue
            let cityArray = json["predictions"] != JSON.null ? json["predictions"].arrayValue : []
            cityResp.citiesInfo = [CityInfo]()
            for cityInfo in cityArray
            {
                let city = CityInfo()
                city.cityName = cityInfo["description"] != JSON.null ? cityInfo["description"].stringValue : ""
                city.placeID = cityInfo["place_id"] != JSON.null ? cityInfo["place_id"].stringValue : ""
                cityResp.citiesInfo.append(city)
            }
        }
        return cityResp
    }
    
    func parseCitiesResponseForLatLong(json:JSON) -> Location
    {
        print(json)
        let location = Location()
        if json["status"].stringValue == "OK"
        {
            location.statusDescription = json["status"].stringValue
            let cityDict = json["result"]
            let adressDict = cityDict["geometry"]
             let locationDict = adressDict["location"].dictionary

            
                location.lat = locationDict?["lat"]?.floatValue
                location.long = locationDict?["lng"]?.floatValue
                location.placeName = cityDict["name"].stringValue
                 location.placeDetailedAddress = cityDict["formatted_address"].stringValue
            
            
        }
        return location
    }
    
    
    
    func perseSignUpResponse( s : String) -> Response
    {
        let dict:NSDictionary = convertStringToDictionary(text: s)! as NSDictionary
        let rspnse:UserParsedInfo = UserParsedInfo()
        
        rspnse.statusCode = dict.object(forKey: "success") as? Bool
        rspnse.statusDescription = dict.object(forKey: "msg") as? String
        
        if rspnse.statusCode == true
        {
            let responseDict:NSDictionary = dict.object(forKey: "user") as! NSDictionary
            let prefs:UserDefaults = UserDefaults.standard
            rspnse.emailId = responseDict.object(forKey: "email") as! String
            prefs.set(rspnse.emailId, forKey: kemailIdKey)
            rspnse.authToken = responseDict.object(forKey: "authentication_token") as! String
            prefs.set(rspnse.password, forKey: kauthKey)
            rspnse.createdDate = responseDict.object(forKey: "created_at") as! String
            prefs.set(rspnse.password, forKey: kauthKey)
            rspnse.userId = responseDict.object(forKey: "id") as! Int
            prefs.set(rspnse.userId, forKey: "userId")

            
        }
        return rspnse
    }
    


    
    
}


