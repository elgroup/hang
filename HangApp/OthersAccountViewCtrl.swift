//
//  OtherAccountViewCtrl.swift
//  HangApp
//
//  Created by Evan Luthra on 03/04/17.
//  Copyright © 2017 EL GROUP. All rights reserved.
//

import UIKit

class OthersAccountViewCtrl: UIViewController,AppServiceDelegate,CAPSPageMenuDelegate{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblBio: UILabel!
     var user_id = Int()
    let ser:AppService = AppService()
     var comingFrom = String()
    var pageMenu : CAPSPageMenuNew?
     var momentsArray:NSMutableArray = []
    var friendsArray:NSMutableArray = []
    var checkInsArray:NSMutableArray = []
    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func viewWillAppear(_ animated: Bool)
    {
        
        callProfileService()
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
        imgUser.clipsToBounds = true;
        super.viewWillAppear(animated)
    }
    
    func layOutBottomVie()
    {
        var controllerArray : [UIViewController] = []
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 : PhotoStreamViewController = mainStoryboard.instantiateViewController(withIdentifier: "photoStream") as! PhotoStreamViewController
        
        controller1.title = "Moments"
        controller1.userMomentsArray = momentsArray
        controllerArray.append(controller1)
        
        
        let controller2 : FriendsViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "friends") as! FriendsViewCtrl
        
        controller2.userFriendsArray = friendsArray
        controller2.title = "Accounts"
        controllerArray.append(controller2)
        let controller3 : PlacesViewCtrl = mainStoryboard.instantiateViewController(withIdentifier: "places") as! PlacesViewCtrl
        controller3.userPlacesArray = checkInsArray
        controller3.title = "Places"
        controllerArray.append(controller3)
        
        // Customize menu (Optional)
        
        let navColor : UIColor =  UIColor(patternImage: UIImage(named: "navBg.png")!)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(navColor),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
            .menuHeight(40.0),
            .menuItemWidth(110.0),
            .centerMenuItems(false)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenuNew(viewControllers: controllerArray, frame: CGRect(x: 0.0, y:0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        self.addChildViewController(pageMenu!)
        self.bottomView.addSubview(pageMenu!.view)
        
        
        pageMenu!.didMove(toParentViewController: self)
        
    }
    
    func callProfileService()
    {
        
        Loader.sharedLoader.showLoaderOnScreen(vc: self)
        ser.delegate = self;
        
        let strUserId = String(format:"%d",user_id)
        
        self.ser.getProfileData(id: strUserId)
    }
    
    func didReceiveResponseWithResponseandAPIName(response:Response,apiName:String)
    {
        
        
       
       
        
        if response.statusCode == true
        {
            
            if(apiName == "getProfileData")
            {
                
                let profiledata = response as! UserContactInfo
                lblUserName.text = profiledata.fullName;
                let data : NSData =  profiledata.bioGraphy.data(using: String.Encoding.utf8)! as NSData
                let biographyText = String(data: data as Data, encoding: String.Encoding.nonLossyASCII)
                lblBio.text = biographyText
                lblUserName.text = profiledata.fullName;
                lblLocation.text  = profiledata.location
                UserDefaults.standard.set(lblUserName.text, forKey: "userFullName")
                UserDefaults.standard.set(profiledata.imageUrl, forKey: "userImage")
                momentsArray =   profiledata.momentsArray
                friendsArray = profiledata.friendsArray
                checkInsArray = profiledata.checkInArray
                
                DispatchQueue.main.async
                    {
                        self.layOutBottomVie()
                        Loader.sharedLoader.hideLoader()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            if let url  = NSURL(string: profiledata.imageUrl),
                                let data = NSData(contentsOf: url as URL)
                            {
                                self.imgUser.image = UIImage(data: data as Data)
                                
                            }
                        })
                        
                }
                
                
            }
            
            
            
        }
    }
    
    func didFailToReceiveResponse(response:Response)
    {
        Loader.sharedLoader.hideLoader()
        layOutBottomVie()
        MiscUtils.sharedMiscUtils.showWarningAlertWithTitleAndMessage(title: "Error", messageString: response.statusDescription!, viewcontroller: self)
    }
    
   
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
   
    @IBAction func backPressed(_ sender: Any)
    {
        if comingFrom == "popup"
        {
            let presentingViewController = self.presentingViewController
           self.dismiss(animated: true) {
                presentingViewController?.dismiss(animated: true, completion: nil)
            }
        }
        else
        {
            _ = self.navigationController?.popViewController(animated: true)
            
        }
        
    }
    

   

}
