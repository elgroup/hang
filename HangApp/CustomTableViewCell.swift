//
//  CustomMapPersonTableViewCell.swift
//  BluLint
//
//  Created by ireslab18 on 5/27/16.
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet var locationName:UILabel!
    @IBOutlet var btnCheckin:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
